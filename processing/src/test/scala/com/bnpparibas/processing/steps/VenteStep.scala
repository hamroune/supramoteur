
package com.bnpparibas.processing.steps

import com.bnpparibas.commons.models.domain.{Aggregation, Scenario}
import com.bnpparibas.commons.models.domain.enums.testaggregs
import com.bnpparibas.processing.services.AMLProcessing
import com.bnpparibas.processing.utils.CucumberUtils
import io.cucumber.datatable.DataTable
import io.cucumber.scala.{EN, ScalaDsl, Scenario => SN}
import org.apache.spark.sql.functions.{col, sum}
import org.apache.spark.sql.types.TimestampType
import org.apache.spark.sql.{DataFrame, Encoders, SparkSession}

import java.sql.Date
import java.time.LocalDate
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._

class AggStep1 extends ScalaDsl with EN {

  Before { _: SN =>
    println("Before...")
  }
  After { _: SN =>
    println("...After")
  }

  implicit val spark = SparkSession
    .builder
    .appName("TestApp")
    .master("local[*]")
    .config("spark.driver.memory", "8g")
    .config("spark.executor.memory", "8g")
    //.config("spark.driver.maxResultSize","2g")
    //.config("spark.executor.extraJavaOptions","-XX:+PrintGCDetails -XX:+PrintGCTimeStamps")
    //.config("spark.serializer","org.apache.spark.serializer.KryoSerializer")
    //.config("spark.storage.memoryFraction", "0.0")
    //.config("spark.memory.useLegacyMode", "true")
    .getOrCreate()

  var givenDF: DataFrame = _
  var expectedDF: DataFrame = _
  var result: String = _
//  var agg: String = _
  //  var scenarioBuilder  = Scenario.builder()
  var date_ref: Date = _

  Given("""^les ventes suivantes$""") { (dataTable: DataTable) =>
    givenDF = CucumberUtils.parse(dataTable)
    //givenDF = spark.read.option("header", true).csv("transactions.csv")
  }



  //  And("""^global scope$"""){
  //    (dataTable:DataTable) =>
  //      val globalScopes = CucumberUtils.parse(dataTable).as[Scope].collectAsList().asScala.toList
  //      scenarioBuilder.scopes(globalScopes)
  //  }

  //  And("""Sort Transaction By"""){
  //    (dataTable:DataTable) =>
  //
  //      val list = CucumberUtils.parse(dataTable).select("orderBy").collectAsList()
  //      if(!list.get(0).isNullAt(0)){
  //        val orderBy = list.get(0).get(0).toString
  //
  //        scenarioBuilder.orderBy(orderBy)
  //      }


  //  }

  //  And("""^aggregation Listt$""") { (dataTable: DataTable) =>
  //    val aggList = CucumberUtils.parse(dataTable).as[testaggregs](Encoders.bean(classOf[testaggregs])).collectAsList().asScala.toList.map(a => {
  //      testaggregs.builder()
  //        .aggColName(a.getAggColName)
  //        .aggFunction(a.getAggFunction)
  //        .aggMetric(a.getAggMetric)
  //        .orderBy(a.getOrderBy)
  //        //        .aggPartitionBy(a.getAggPartitionBy)
  //        //        .aggregationScopes(a.getAggregationScopes)
  //        //        .aggTimePeriod(a.getAggTimePeriod)
  //        //        .aggTimePeriodSelected(a.getAggTimePeriodSelected)
  //        //        .aggTimePeriodSize(a.getAggTimePeriodSize)
  //
  //
  //        .build()
  //    }
  //    )
  //    agg = aggList.get(0).getAggFunction
  //    //    scenarioBuilder.aggregationList(aggList)
  //
  //  }


  /*And("""^maille choisie$""") { (dataTable:DataTable) =>

    val partition = CucumberUtils.parse(dataTable).select("partition").collectAsList().map(_.get(0).toString())

    scenarioBuilder.partitionByList(partition)
  }*/


  /*And("""^niveau d'alerte$""") { (dataTable:DataTable) =>

    val alert = CucumberUtils.parse(dataTable).select("alert").collectAsList().map(_.get(0).toString()).get(0)

    scenarioBuilder.alertLevel(AlertLevel.valueOf(alert))
  }*/




  When("""^je calcul la somme$""") { () =>
    result= givenDF.agg(sum("prix")).first.get(0).toString
    //    val scenario = scenarioBuilder.build()
    //    if (agg == "sum") {
    //      println("aggfunction=", agg)
    //    }
    //    else println("agg function not yet implemented to be tested")
    //    //    val df = givenDF
    //    //    resultDF = df

  }

  Then("^le resultat de la somme est le suivant$") { (dataTable: DataTable) =>

    //    val scenario = scenarioBuilder.build()
//    println(givenDF.agg(sum("prix")).first.get(0))
    expectedDF = CucumberUtils.parse(dataTable)

    println("Result")
    //    if(agg=="sum")
//    if(expectedDF.select("prix").first().get(0) == resultDF) println("resultat bon")
//    else print("somme fausse")
    assert(expectedDF.select("prix").first().get(0)==result)
    //    else println("impossible!!!!")
    //    resultDF.show()


//        assert (resultDF.drop("week", "month", "day").except(expectedDF).collectAsList().size() == 0)


  }

}
