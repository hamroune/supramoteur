package com.bnpparibas.processing.runner

import io.cucumber.junit.{Cucumber, CucumberOptions}
import org.junit.runner.RunWith
/*

@RunWith(classOf[Cucumber])
@CucumberOptions(
  features = Array("src/test/resources/com/bnpparibas/processing/features/"),
  glue = Array("classpath:com.bnpparibas.processing.steps")
)
*/


@RunWith(classOf[Cucumber])
@CucumberOptions(
  features = Array("src/test/resources/com/bnpparibas/processing/features"),
  tags = "not @Wip",
  glue = Array("classpath:com.bnpparibas.processing.steps"),
  plugin = Array("pretty", "html:target/cucumber/html"))
class RunBDDTest
