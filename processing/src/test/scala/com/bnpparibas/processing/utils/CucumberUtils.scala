package com.bnpparibas.processing.utils

import java.util.{Locale, ArrayList => JArrayList, List => JList}
import java.text.SimpleDateFormat
import com.bnpparibas.commons.models._
import org.apache.spark.sql.types.{ArrayType, DataTypes, StringType, StructField, StructType}
import org.apache.spark.sql.{DataFrame, Dataset, Encoders, Row, SparkSession}

import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import io.cucumber.datatable.DataTable


object CucumberUtils {

  //val formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.FRANCE)

  def parseDate(sDate: String): java.sql.Date = {
    val formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.FRANCE)
    new java.sql.Date(formatter.parse(sDate).getTime)
  }

  import com.bnpparibas.commons.imp.BnpImplicits._

  // Alias variables
  val INTERACTIONS = "interactions"
  val EVENTS = "events"
  val ATTRIBUTES = "attributes"

  def parse(data: DataTable)(implicit spark: SparkSession): DataFrame = {
    val structfields = data.asLists()
      .get(0)
      .toList
      .map(elm => {


        val splitted = elm.split(":")

        var name = elm
        var dataType = "String"


        if (splitted.size >= 2) {
          name = splitted(0)
          dataType = splitted(1)
        }

        (name, dataType) match {
          case (name, "String") => new StructField(name.trim, DataTypes.StringType, true)
          case (name, "Int") => new StructField(name.trim, DataTypes.IntegerType, true)
          case (name, "Double") => new StructField(name.trim, DataTypes.DoubleType, true)
          case (name, "Boolean") => new StructField(name.trim, DataTypes.BooleanType, true)
          case (name, "Binary") => new StructField(name.trim, DataTypes.BinaryType, true)
          case (name, "Float") => new StructField(name.trim, DataTypes.FloatType, true)
          case (name, "Long") => new StructField(name.trim, DataTypes.LongType, true)
          case (name, "Date") => new StructField(name.trim, DataTypes.DateType, true)
          case (name, "List[String]") => new StructField(name.trim, DataTypes.createArrayType(DataTypes.StringType), true)
          case (name, "List[Int]") => new StructField(name.trim, DataTypes.createArrayType(DataTypes.IntegerType), true)
          case (name, "List[Boolean]") => new StructField(name.trim, DataTypes.createArrayType(DataTypes.BooleanType), true)
          case (name, "List[Double]") => new StructField(name.trim, DataTypes.createArrayType(DataTypes.DoubleType), true)
          case (name, _) => new StructField(name.trim, DataTypes.StringType, true)
        }
      })

    val sctrucType = StructType(structfields)

    val lignes = data.asLists()

    val rows: List[Row] = lignes.subList(1, lignes.size())
      .map((rowAsString: JList[String]) => {

        var index = 0
        val listOfTypedObjects: JList[Any] = new JArrayList[Any]()


        val ARDOUBLE: ArrayType = DataTypes.createArrayType(DataTypes.DoubleType, true)
        val ARSTRING: ArrayType = DataTypes.createArrayType(DataTypes.StringType, true)

        rowAsString.toList.foreach((element: String) => {

          structfields(index).dataType match {

            case DataTypes.BooleanType => {
              if (element != null && element != "null") {
                listOfTypedObjects.add(element.toBoolean)
              } else {
                listOfTypedObjects.add(null)
              }
            }
            case DataTypes.IntegerType => {

              if (element != null && element != "null") {
                listOfTypedObjects.add(element.toInt)
              } else {
                listOfTypedObjects.add(null)
              }
            }
            case DataTypes.DoubleType => {

              if (element != null && element != "null") {
                listOfTypedObjects.add(element.toDouble)
              } else {
                listOfTypedObjects.add(null)
              }
            }
            case DataTypes.FloatType => if (element != null && element != "null") {
              listOfTypedObjects.add(element.toFloat)
            } else {
              listOfTypedObjects.add(null)
            }
            case DataTypes.StringType => listOfTypedObjects.add(element)
            case DataTypes.LongType => if (element != null && element != "null") {
              listOfTypedObjects.add(element.toLong)
            } else {
              listOfTypedObjects.add(null)
            }
            case DataTypes.DateType => if (element != null && element != "null") {
              listOfTypedObjects.add(parseDate(element))
            } else {
              listOfTypedObjects.add(null)
            }
            case ARSTRING => {

              if ("null".equals(element)) {
                listOfTypedObjects.add(null)
              } else {
                listOfTypedObjects.add(element.replaceAll("\\[", "").replaceAll("\\]", "").split(",").toList)
              }
            }
            case ARDOUBLE => {
              if ("null".equals(element)) {
                listOfTypedObjects.add(null)
              } else {

                listOfTypedObjects.add(element.replaceAll("\\[", "").replaceAll("\\]", "").split(",").toList.map(_.toDouble))
              }
            }
            case _ => listOfTypedObjects.add(element.toString)
          }
          index += 1
        })

        Row.fromSeq(listOfTypedObjects.toSeq)
      }).toList

    val df = spark.createDataFrame(rows, sctrucType)
    df
  }

}




