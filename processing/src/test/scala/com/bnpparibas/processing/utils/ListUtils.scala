package com.bnpparibas.processing.utils


import java.util.{Collections, List => JList}
import scala.collection.JavaConverters._


trait CompareList[T <: Comparable[T]] {

  def compare(list1: JList[T], list2: JList[T]): Boolean = {
    Collections.sort(list1)
    Collections.sort(list2)

    (list1.size == list2.size) && list1
      .asScala
      .toSet
      .foldLeft(true) {
        case (previousOK: Boolean, elementA) =>
          previousOK && list2.asScala.toSet.contains(elementA)
      }
  }
}

//object ClientInteractionCompare extends ComparerTwoList[ClientInteraction]

object ListUtils {


  def compareTwoList[T <: Comparable[T]](list1: JList[T], list2: JList[T])(implicit comparator: CompareList[T]): Boolean = {
    comparator.compare(list1, list2)
  }
}
