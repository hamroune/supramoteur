Feature: meilleur vente par categorie

  Scenario: Sum :: categorie

    Given  les ventes suivantes
      # transform this data table to Transactions objectsomme_montant_SEPA_TO_HR_semaine_actuelle
      | VenteId | categorie | produit        | PU   | qnt | prix | paiement
      | 1       | fruit     | pomme          | 1.55 | 3   | 4.65 | esp
      | 1       | legume    | carotte        | 1    | 2   | 2    | esp
      | 2       | legume    | salade         | 0.99 | 1   | 0.99 | cb
      | 3       | legume    | pomme de terre | 3    | 2   | 6    | cb

#    And aggregation Listt
#      # transform this data table to List of Aggregation
#      # aggregationFunction = Max, Min, Sum, Count, Avg ...
#      # aggPartitionBy : should be client/account
#      # aggregation Partition By : select subset dataframe to apply (compute) the Agg
#      # aggregartion Order By used in case of aggregation is sensitive to the order (Max, Min etc...)
#        # it can be a list of dimensions
#      # aggTimePeriodSize The Lenght of Period (period of 12 Weeks, period of 3 Months etc...)
#      # aggTimePeriodSelected corresponds to Period
#          # 0-> N c-a-d LAST PERIOD
#          # 1-> N-1 ==> Previous PERIOD
#          # 2-> N-2
#     # aggPartitionDimension if we want to focus on certain dimensions (it can be a List)
#
#     # Aggragation out put name: the name of the aggregation name
#      | aggFunction | orderBy   | aggMetric | aggColName                |
#      | sum         | categorie | prix      | sum_purchase_per_category |


    When je calcul la somme

    Then le resultat de la somme est le suivant

      | prix |
      | 13.64   |
