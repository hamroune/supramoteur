export SPARK_MAJOR_VERSION=2
spark-submit --class com.bnpparibas.processing.entrypoint.ScenarioApplication  \
--master yarn  --deploy-mode client    --num-executors 3  \
--driver-memory 4g  --queue machinelearning_bddf   --executor-memory 4g  \
--executor-cores 4 --jars hdfs:///BDDF/DS/Indus/Supra/jars/supra_v2.jar     \
./processing-0.1-SNAPSHOT-jar-with-dependencies.jar \
--transaction_file /BDDF/DS/Indus/Emc2/Data/AML/ap01433_cb_transactions \
--client_file /BDDF/DS/Indus/Emc2/Data/AML/ap01433_customers \
--country_file /BDDF/DS/Indus/Emc2/Data/AML/ap01433_country \
--business_segmentation_file /BDDF/DS/Indus/Emc2/Data/AML/Codes_APE.csv \
--transaction_type_file /BDDF/DS/Indus/Emc2/Data/AML/TRANSACTION_TYPE_INSTRUMENT_NEW.csv