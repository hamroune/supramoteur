package integration

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import xom.Day

trait PersistenceManager {
  def getDay: Day

  def getData(sc: SparkContext): (RDD[xom.Client], RDD[xom.Transaction])

  def saveData(sc: SparkContext, alerts: RDD[xom.Alert])
}