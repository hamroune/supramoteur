package com.bnpparibas.processing.entrypoint

import java.sql.Date
import java.time.{LocalDate, LocalDateTime}

import org.apache.spark.launcher.SparkLauncher
import org.apache.spark.sql._
import com.bnpparibas.commons.imp.BnpImplicits._
import com.bnpparibas.commons.models.domain.{Aggregation, EnrichedTransactionWithAggregs, Scenario}
import com.bnpparibas.commons.models.types.{DoubleField, ListStringField}
import org.apache.commons.beanutils.BeanMap
import org.apache.spark.sql.catalyst.encoders.{ExpressionEncoder, RowEncoder}
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import java.util

import com.bnpparibas.commons.api.BigAppsImpl
import com.bnpparibas.commons.services.{AMLTransCodification, ReadFiles}
import com.bnpparibas.commons.utils.AppArguments
import com.bnpparibas.processing.services.AMLProcessing
import javax.sql.rowset.RowSetFactory

import scala.collection.JavaConversions._
import scala.concurrent.ExecutionContext
import org.apache.spark.sql.functions.{col, expr, lit, when}



object GenerationEnrichedRtransaction extends App {

  def getDateAlayze(inDate:String): Date ={
    val dateAnalyze = inDate match{
      case "" => {
        import java.util.Calendar
        val cal = Calendar.getInstance
        cal.add(Calendar.DATE, -1)
        new Date(cal.getTimeInMillis)
      }
      case null => {
        import java.util.Calendar
        val cal = Calendar.getInstance
        cal.add(Calendar.DATE, -1)
        new Date(cal.getTimeInMillis)
      }
      case _ => {
        java.sql.Date.valueOf(LocalDate.parse(inDate))
      }
    }

    dateAnalyze
  }

  implicit val ec = ExecutionContext.global

  val start = LocalDateTime.now()
  val appArs: AppArguments = AppArguments.parse(args)

  //GET ANALYZE DATE
  val analyzeDate: Date = getDateAlayze(appArs.dateRef)

  lazy implicit val sparkSession = SparkSession
    .builder
    .appName("Scenario B")
    //.master("local[*]")
    .enableHiveSupport()
    .config("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    .config("spark.executor.extraJavaOptions", "-XX:+UseCompressedOops  -XX:+UseG1GC")
    .config("spark.driver.extraJavaOptions", "-XX:+UseCompressedOops  -XX:+UseG1GC")
    .config("spark.memory.offHeap.enabled", "true")
    .config("spark.memory.offHeap.size", "10G")
    .getOrCreate()

  val rTransaction =  ReadFiles.readRTransaction(appArs)

  val transactionType = ReadFiles.readTransactionType(appArs)

  val customerDF = ReadFiles.readCustomers(appArs)
      .withColumnRenamed("CUSTOMER_SOURCE_UNIQUE_I","CUSTOMER")
      .drop(col("ORGANISATION"))

  println("customerDF.printSchema()")
  customerDF.printSchema()

  val businessSegmentation = ReadFiles.readBusinessSegmentation(appArs)

  val dfR = AMLTransCodification.jointRTransactionWithTransactionType(rTransaction, transactionType)

  println("dfR.printSchema()")
  dfR.printSchema()
  val rTransactionWithCustomer = dfR.join(customerDF, Seq("CUSTOMER"), "inner")


  val renrichedTransaction = rTransactionWithCustomer.join(businessSegmentation, rTransactionWithCustomer.col("SEGMENT_APE") === businessSegmentation.col("business_segment_1_value"),"left")
    .drop("business_segment_1_value")

  renrichedTransaction.write.mode("append").orc("/BDDF/DS/Indus/Emc2/Data/AML/OUTPUT/ENRICHED_3_M_R_12_TRANSACTION")

}
