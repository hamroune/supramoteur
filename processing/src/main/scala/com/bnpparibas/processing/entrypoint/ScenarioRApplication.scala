package com.bnpparibas.processing.entrypoint

import java.sql.Date
import java.time.{LocalDate, LocalDateTime}
import java.util

import com.bnpparibas.commons.api.{BigAppsImpl, RealODMImplem2}
import com.bnpparibas.commons.imp.BnpImplicits._
import com.bnpparibas.commons.models.domain.{Scenario, Scope}
import com.bnpparibas.commons.models.processed.RRawEnrichedTransaction
import com.bnpparibas.commons.services.ReadFiles
import com.bnpparibas.commons.utils.AppArguments
import com.bnpparibas.processing.services.AMLProcessing
import org.apache.spark.sql.{SaveMode, SparkSession}

import scala.concurrent.ExecutionContext

object ScenarioRApplication {
  def main(args: Array[String]): Unit = {
    def getDateAlayze(inDate:String): Date ={
      val dateAnalyze = inDate match{
        case "" => {
          import java.util.Calendar
          val cal = Calendar.getInstance
          cal.add(Calendar.DATE, -1)
          new Date(cal.getTimeInMillis)
        }
        case null => {
          import java.util.Calendar
          val cal = Calendar.getInstance
          cal.add(Calendar.DATE, -1)
          new Date(cal.getTimeInMillis)
        }
        case _ => {
          java.sql.Date.valueOf(LocalDate.parse(inDate))
        }
      }

      dateAnalyze
    }

    implicit val ec = ExecutionContext.global

    val start = LocalDateTime.now()
    val appArs: AppArguments = AppArguments.parse(args)

    //GET ANALYZE DATE
    val analyzeDate: Date = getDateAlayze(appArs.dateRef)

    lazy implicit val sparkSession = SparkSession
      .builder
      .appName("Scenario R")
      //.master("local[*]")
      .enableHiveSupport()
      .config("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
//      .config("spark.executor.extraJavaOptions", "-XX:+UseCompressedOops  -XX:+UseG1GC")
//      .config("spark.driver.extraJavaOptions", "-XX:+UseCompressedOops  -XX:+UseG1GC")
//      .config("spark.memory.offHeap.enabled", "true")
//      .config("spark.memory.offHeap.size", "10G")
      .getOrCreate()

    //FIRST INSTANCIATE ODM
//    val odm = new RealODMImplem2()
    val odm = new BigAppsImpl("")

    //GET SCENARIO FROM ODM
    val scenarios = Vector("DS_52_A_2").par

    val enrichedTransaction = ReadFiles.readRRawEnrichedTransaction(sparkSession).toDF

    scenarios.foreach(sc=>{
      val scenario: Scenario = odm.retrieveScenario(sc)

      println(s"scenario from ODM scenario=${scenario}")

      // COMPUTE AGGREGS SEQUANTIALLY
      val resultAggregs = AMLProcessing.computeAggregates(enrichedTransaction, scenario, analyzeDate)
      val resultAggregsSchema = resultAggregs.schema

      val enrichedWithAggregsDF = resultAggregs.map(r=>AMLProcessing.toRawEnrichedTransactionWithAggregs(scenario, r))
      val alertDF = AMLProcessing.toAlert(enrichedWithAggregsDF, scenario, resultAggregsSchema)

      //SAVE ALERTS
      alertDF.write.partitionBy("scenario_name").mode(SaveMode.Overwrite).orc("/BDDF/DS/Indus/Emc2/Data/AML/OUTPUT/ALERTS_3M_P_R_TRANSACTION")

    })

  }





}
