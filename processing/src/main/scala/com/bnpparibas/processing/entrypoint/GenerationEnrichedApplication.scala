package com.bnpparibas.processing.entrypoint

import java.sql.Date
import java.time.{LocalDate, LocalDateTime}
import java.util

import com.bnpparibas.commons.api.BigAppsImpl
import com.bnpparibas.commons.models.domain.enums.AlertLevel
import com.bnpparibas.commons.models.domain.{Aggregation, Scenario, Scope}
import com.bnpparibas.commons.models.raw.{BusinessSegmentation, CBTransaction, Country, TransactionType}
import com.bnpparibas.commons.services.{AMLTransCodification, ReadFiles}
import com.bnpparibas.commons.utils.AppArguments
import com.bnpparibas.processing.services.{AMLProcessing, Utils}
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.{Dataset, SaveMode, SparkSession}
import com.bnpparibas.commons.imp.BnpImplicits._
import com.bnpparibas.commons.models.processed.RawEnrichedCBTransaction

object GenerationEnrichedApplication {

  def main(args: Array[String]): Unit = {
    def getDateAlayze(inDate:String): Date ={
      val dateAnalyze = inDate match{
        case "" => {
          import java.util.Calendar
          val cal = Calendar.getInstance
          cal.add(Calendar.DATE, -1)
          new Date(cal.getTimeInMillis)
        }
        case null => {
          import java.util.Calendar
          val cal = Calendar.getInstance
          cal.add(Calendar.DATE, -1)
          new Date(cal.getTimeInMillis)
        }
        case _ => {
          java.sql.Date.valueOf(LocalDate.parse(inDate))
        }
      }

      dateAnalyze
    }

    val start = LocalDateTime.now()
    val appArs: AppArguments = AppArguments.parse(args)

    //GET ANALYZE DATE
    val analyzeDate: Date = getDateAlayze(appArs.dateRef)

    lazy implicit val sparkSession = SparkSession
      .builder
      .appName("Test FRAMEWORK")
      //.master("local[*]")
      .enableHiveSupport()
      .config("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
      .config("spark.executor.extraJavaOptions", "-XX:+UseCompressedOops  -XX:+UseG1GC")
      .config("spark.driver.extraJavaOptions", "-XX:+UseCompressedOops  -XX:+UseG1GC")
      .config("spark.memory.offHeap.enabled", "true")
      .config("spark.memory.offHeap.size", "10G")
      .getOrCreate()




    val df = sparkSession.read.csv("C:\\Users\\hamro\\dev\\supramoteur\\test1.csv")










    //FIRST INSTANCIATE ODM
    val odm = new BigAppsImpl("")

    //GET SCENARIO FROM ODM
    val scenario: Scenario = odm.retrieveScenario("")

    //JOINS TX+CUISTOMER+COUNTRY+TxType
    val customerDS = ReadFiles.readCustomers(appArs)
    val transactionDS: Dataset[CBTransaction] = ReadFiles.readTransactions(appArs)
    val transctionTypeDS: Dataset[TransactionType] = ReadFiles.readTransactionType(appArs)
    val countryDS: Dataset[Country] = ReadFiles.readCountry(appArs)
    val businessSegmentationDS: Dataset[BusinessSegmentation] = ReadFiles.readBusinessSegmentation(appArs)
    val transactionWithTransactionTypeDS = AMLTransCodification.jointTransactionWithTransactionType(transactionDS, transctionTypeDS)
    val transactionWithAddJoinColumnsDS = AMLTransCodification.enrichedTransactionWithAddJoinColumns(transactionWithTransactionTypeDS)
    val enrichedTransaction =
      AMLTransCodification.enrichedTransactions(transactionWithAddJoinColumnsDS, customerDS, countryDS, businessSegmentationDS)
    enrichedTransaction.write.mode("append").orc("/BDDF/DS/Indus/Emc2/Data/AML/OUTPUT/ENRICHED_870_P_CB_TRANSACTION")
  }
}
