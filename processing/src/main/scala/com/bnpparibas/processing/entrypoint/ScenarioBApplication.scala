package com.bnpparibas.processing.entrypoint
import com.bnpparibas.commons.api.BigAppsImpl
import com.bnpparibas.commons.imp.BnpImplicits._
import com.bnpparibas.commons.models.domain.Scenario
import com.bnpparibas.commons.services.ReadFiles
import com.bnpparibas.commons.utils.AppArguments
import com.bnpparibas.processing.services.AMLProcessing
import org.apache.spark.sql.{SaveMode, SparkSession}

import java.sql.Date
import java.time.{LocalDate, LocalDateTime}
import scala.concurrent.ExecutionContext

object ScenarioBApplication {
  def main(args: Array[String]): Unit = {
    def getDateAlayze(inDate:String): Date ={
      val dateAnalyze = inDate match{
        case "" => {
          import java.util.Calendar
          val cal = Calendar.getInstance
          cal.add(Calendar.DATE, -1)
          new Date(cal.getTimeInMillis)
        }
        case null => {
          import java.util.Calendar
          val cal = Calendar.getInstance
          cal.add(Calendar.DATE, -1)
          new Date(cal.getTimeInMillis)
        }
        case _ => {
          java.sql.Date.valueOf(LocalDate.parse(inDate))
        }
      }

      dateAnalyze
    }

    implicit val ec = ExecutionContext.global

    val start = LocalDateTime.now()
    val appArs: AppArguments = AppArguments.parse(args)

    //GET ANALYZE DATE
    val analyzeDate: Date = getDateAlayze(appArs.dateRef)

    lazy implicit val sparkSession = SparkSession
      .builder
      .appName("Scenario B")
      //.master("local[*]")
      .enableHiveSupport()
      .config("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
      .getOrCreate()

    //FIRST INSTANCIATE ODM
    val odm = new BigAppsImpl("")//new RealODMImplem()

    //GET SCENARIO FROM ODM
    val scenarios = Vector("DS_11_B_1_a").par

    val enrichedTransaction = ReadFiles.readCBRawEnrichedTransaction(sparkSession).toDF()

    scenarios.foreach(sc=>{
      val scenario: Scenario = odm.retrieveScenario(sc)
      println(s"Scenario FROM ODM = ${scenario}")
      //JOINS TX+CUISTOMER+COUNTRY+TxType
      //    val customerDS = ReadFiles.readCustomers(appArs)
      //    val transactionDS: Dataset[CBTransaction] = ReadFiles.readTransactions(appArs)
      //    val transctionTypeDS: Dataset[TransactionType] = ReadFiles.readTransactionType(appArs)
      //    val countryDS: Dataset[Country] = ReadFiles.readCountry(appArs)
      //    val businessSegmentationDS: Dataset[BusinessSegmentation] = ReadFiles.readBusinessSegmentation(appArs)
      //    val transactionWithTransactionTypeDS = AMLTransCodification.jointTransactionWithTransactionType(transactionDS, transctionTypeDS)
      //    val transactionWithAddJoinColumnsDS = AMLTransCodification.enrichedTransactionWithAddJoinColumns(transactionWithTransactionTypeDS)

      // COMPUTE AGGREGS SEQUANTIALLY
      val resultAggregs = AMLProcessing.computeAggregates(enrichedTransaction, scenario, analyzeDate)

      println("Schema")
      resultAggregs.printSchema()


      val resultAggregsSchema = resultAggregs.schema

      //EVALUATE RULE AND GET ALERTS
      val enrichedWithAggregsDF = resultAggregs.map(r=>AMLProcessing.toRawEnrichedTransactionWithAggregs(scenario, r))


      val alertDF = AMLProcessing.toAlert(enrichedWithAggregsDF, scenario, resultAggregsSchema)


      //SAVE ALERTS
      alertDF.write.partitionBy("scenario_name").mode(SaveMode.Overwrite).orc("/BDDF/DS/Indus/Emc2/Data/AML/OUTPUT/ALERTS_870_P_CB_TRANSACTION")

    })

  }





}
