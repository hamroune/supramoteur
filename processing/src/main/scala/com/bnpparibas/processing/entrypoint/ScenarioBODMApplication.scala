package com.bnpparibas.processing.entrypoint

//import com.bnpparibas.commons.api.RealODMImplem
import org.apache.spark.sql.SparkSession
//import com.bnpparibas.commons.models.domain.Scenario


object ScenarioBODMApplication{
    def main(args: Array[String]): Unit = {
        lazy implicit val sparkSession = SparkSession
          .builder
          .appName("Tests ODM")
          //.master("local[*]")
          .enableHiveSupport()
          .getOrCreate()

        //FIRST INSTANCIATE ODM

        import com.bnpparibas.commons.api.RealODMImplem2
        val odm = new RealODMImplem2()//w BigAppsImpl("")

        //GET SCENARIO FROM ODM
        val scenario = odm.retrieveScenario("DS_11_B_1_a")
        println(s"scenario ==> ${scenario}")
    }


}
