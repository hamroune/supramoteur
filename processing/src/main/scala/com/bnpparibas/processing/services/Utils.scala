package com.bnpparibas.processing.services

import java.sql
import java.sql.{Date, Timestamp}
import java.time.temporal.ChronoUnit
import java.time._

import org.apache.spark.sql.{DataFrame, SparkSession}
import java.sql.Timestamp
import java.text.SimpleDateFormat

import org.apache.spark.sql.functions._




object Utils {

  def computeDays(givenDF: DataFrame, colName:String ,toDay: Date)(implicit spark:SparkSession): DataFrame = {
    val week = spark.udf.register("day", getDaysNumberFrom2Dates)
    givenDF.withColumn("day",week(col(colName), lit(toDay)))
  }

  def getDaysNumberFrom2Dates= (colValue: Date, toDay: Date) => {
    val days = ChronoUnit.DAYS.between(colValue.toLocalDate, toDay.toLocalDate);
    Math.floor(days).toInt
  }


  def computeMonth(givenDF: DataFrame, colName:String ,toDay: Date)(implicit spark:SparkSession): DataFrame = {
    val week = spark.udf.register("month", getMonthsNumberFrom2Dates)
    givenDF.withColumn("month",week(col(colName), lit(toDay)))
  }

  def getMonthsNumberFrom2Dates= (colValue: Date, toDay: Date) => {
    val days = ChronoUnit.DAYS.between(colValue.toLocalDate, toDay.toLocalDate);
    Math.floor(days/30).toInt
  }

  def computeWeek(givenDF: DataFrame, colName:String ,toDay: Date)(implicit spark:SparkSession): DataFrame = {
    val week = spark.udf.register("week", getWeeksNumberFrom2Dates)
    givenDF.withColumn("week",week(col(colName), lit(toDay)))
  }

  def getWeeksNumberFrom2Dates= (colValue: Date, toDay: Date) => {
    val days = ChronoUnit.DAYS.between(colValue.toLocalDate, toDay.toLocalDate);
    Math.floor(days/7).toInt
  }

}
