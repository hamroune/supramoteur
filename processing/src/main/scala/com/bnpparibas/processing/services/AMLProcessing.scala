package com.bnpparibas.processing.services

import com.bnpparibas.commons.api.RealODMImplem2
import com.bnpparibas.commons.imp.BnpImplicits._
import com.bnpparibas.commons.models.domain._
import com.bnpparibas.commons.models.domain.enums.AlertLevel
import com.bnpparibas.commons.models.raw.{Customer, CustomerWithSeuil}
import com.bnpparibas.commons.models.types._
import org.apache.commons.beanutils.BeanMap
import org.apache.spark.sql._
import org.apache.spark.sql.functions.{col, _}
import org.apache.spark.sql.types._

import java.sql.Date
import java.text.SimpleDateFormat
import java.util.Objects
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._


case class MyFilter(col:String, operator:String, value:String)
object AMLProcessing {

  def isEmpty(x: String) = x == null || x.isEmpty

  def concatAndString = { (first:String, second:String) => {
    (first, second) match {
      case (f, s) if isEmpty(f) && isEmpty(s) => ""
      case (f, s) if isEmpty(f) && !isEmpty(s) =>  s
      case (f, s) if !isEmpty(f) && !isEmpty(s) =>  s"(${f.trim}) and (${s.trim})"
      case (f, s) if !isEmpty(f) && isEmpty(s) =>  f
    }
  }}

  def concatAndScopes = { (first:Scope, second:Scope) => {
    (first, second.getScope) match {
      case (f, s) if isEmpty(f.getScope) && isEmpty(s) => ""
      case (f, s) if isEmpty(f.getScope) && !isEmpty(s) =>  s
      case (f, s) if !isEmpty(f.getScope) && !isEmpty(s) =>  s"(${f.getScope.trim}) and (${s.trim})"
      case (f, s) if !isEmpty(f.getScope) && isEmpty(s) =>  f
    }
  }}

  def concatAndStringScopes = { (first:String, second:Scope) => {
    (first, second.getScope) match {
      case (f, s) if isEmpty(f) && isEmpty(s) => ""
      case (f, s) if isEmpty(f) && !isEmpty(s) =>  s
      case (f, s) if !isEmpty(f) && !isEmpty(s) =>  s"(${f.trim}) and (${s.trim})"
      case (f, s) if !isEmpty(f) && isEmpty(s) =>  f
    }
  }}

  def concatOrScopes = { (first:String, second:Scope) => {
    (first, second.getScope) match {
      case (f, s) if isEmpty(f) && isEmpty(s) => ""
      case (f, s) if isEmpty(f) && !isEmpty(s) =>  s
      case (f, s) if !isEmpty(f) && !isEmpty(s) =>  s"(${f.trim}) or (${s.trim})"
      case (f, s) if !isEmpty(f) && isEmpty(s) =>  f
    }
  }}

  def concatOrString = { (first:String, second:String) => {
    (first, second) match {
      case (f, s) if isEmpty(f) && isEmpty(s) => ""
      case (f, s) if isEmpty(f) && !isEmpty(s) =>  s
      case (f, s) if !isEmpty(f) && !isEmpty(s) =>  s"(${f.trim}) or (${s.trim})"
      case (f, s) if !isEmpty(f) && isEmpty(s) =>  f
    }
  }}

  def getParsedDate(dateAnalize:Date):String={
    val formatter = new SimpleDateFormat("yyyy-MM-dd")
    val parsedDateAnalyze = formatter.format(dateAnalize)

    parsedDateAnalyze
  }

  def getGlobalScopes(scenario:Scenario,  dateAnalize:Date)(implicit sparkSession: SparkSession):String={

    val parsedDateAnalyze = getParsedDate(dateAnalize)

    println(s"string ${dateAnalize} ==> parsedDateRefrence => ${parsedDateAnalyze}")

    val globalScopes = scenario.getScopes match {
      case s if s == null || s.size()<=0 => {
        println("pas de global Scopes")
        ""
      }
      case _ => {
        println("scenario.getScopes.asScala concatAndStringScopes")
        scenario.getScopes.asScala.foldLeft(""){concatAndStringScopes}
      }
    }
    println(s"globalScopes => ${globalScopes}")

    globalScopes
  }

  def getScopesOfAggregation(agg:Aggregation)(implicit sparkSession: SparkSession): String ={
    agg.getAggregationScopes match {
      case null => ""
      case _ =>  agg.getAggregationScopes.asScala.foldLeft(""){concatAndStringScopes}
    }
  }

  def getAggregationScopes(scenario:Scenario)(implicit sparkSession: SparkSession):String={
    val aggregationScopes = scenario.getAggregationList.asScala.map(agg=>{
      val aggScopes = getScopesOfAggregation(agg)
      aggScopes
    })

    val agScopes = aggregationScopes.foldLeft(""){concatOrString}
    agScopes
  }

  def filterByScopes(enrichedDF:DataFrame, allScopes:String)(implicit sparkSession: SparkSession):DataFrame={

    println(s"enrichedDF : ${enrichedDF.count}")
    allScopes match {
      case s if isEmpty(s) => {
        println("pas de scopes, pas de filtrage")
        enrichedDF
      }
      case _ => {

        println(s" enrichedTransaction.where(expr(allScopes)) = ${allScopes}")
        val filtered = enrichedDF.where(expr(allScopes))
        println("After Where show : "+filtered.count())

        filtered
        //enrichedDF.where(expr(allScopes))
      }
    }
  }

  def addMonthsWeeksDays(filteredIn:DataFrame, dateAnalize:Date)(implicit sparkSession: SparkSession):DataFrame={
    val df0 = Utils.computeWeek(filteredIn, "RUN_TIMESTAMP", dateAnalize)
    val df1 = Utils.computeMonth(df0, "RUN_TIMESTAMP", dateAnalize)
    val dfWithMonthWeekDays = Utils.computeDays(df1, "RUN_TIMESTAMP", dateAnalize)
    dfWithMonthWeekDays
  }

  def addPeriodProfondeur(df:DataFrame, agg:Aggregation)(implicit sparkSession: SparkSession):DataFrame={

    val periodDF = df
      // .withColumn("profondeur", lit(s"${agg.getAggTimePeriodSize}"))
      .withColumn("periode", expr(s"${agg.getAggTimePeriod}/profondeur").cast(IntegerType))
      .withColumn("periode_selected", lit(s"${agg.getAggTimePeriodSelected}"))
    periodDF
  }

  def computeGroupByAggregate(df:DataFrame, agg:Aggregation ,scenario: Scenario)(implicit sparkSession: SparkSession):(Seq[String], DataFrame)={
    // ============== FILTER ==============================
    val aggScopes = getScopesOfAggregation(agg)
    val selectedPeriodDF = addPeriodProfondeur(df, agg).filter(col("periode").equalTo(col("periode_selected")))

    //Filter by global + aggreg scope
    println(s"Current Aggreg => ${agg.getAggColName}")
    println(s"current ${agg.getAggColName} Scopes => ${aggScopes}")

    val filteredDF = aggScopes match{
      case scopes if !isEmpty(scopes) => {
        selectedPeriodDF.filter(expr(scopes))
      }
      case _ => selectedPeriodDF
    }

    val allPartitions =  agg.getAggPartitionBy match {
      case byAggPartition if byAggPartition == null || byAggPartition.isEmpty => scenario.getPartitionByList.toSeq
      case  _  => scenario.getPartitionByList.toSeq ++ Seq(agg.getAggPartitionBy)
    }

    val allPartitionsCols = allPartitions.map(col(_))

    val expressionAgg:Column = expr(s" ${agg.getAggFunction()}(${agg.getAggMetric}) as ${agg.getAggColName}") // "count(TxId)"

    val aggDF = filteredDF
      .groupBy(allPartitionsCols:_*).agg(expressionAgg)

    (allPartitions, aggDF)
  }

  @throws(classOf[Exception])
  def computeAggregates(enrichedTransaction:DataFrame, scenario:Scenario, dateAnalize:Date)(implicit sparkSession: SparkSession): DataFrame = {

    println("Start enrichedTransaction.count :" + enrichedTransaction.count())

    val globalScopes = getGlobalScopes(scenario, dateAnalize)
    val agScopes = getAggregationScopes(scenario)
    val allScopes = concatAndString(globalScopes, agScopes)

    println(s"globalScopes: ${globalScopes}")
    println(s"agScopes: ${agScopes}")
    println(s"allScopes: ${allScopes}")

    val filteredIn = filterByScopes(enrichedTransaction, allScopes)

    println("filteredIn filtered by all scopes ")
    filteredIn.show()


    val globalFilteredAndMonthsDF = addMonthsWeeksDays(filteredIn, dateAnalize)
    val parsedDateAnalyze = getParsedDate(dateAnalize)
    val todayExpression = s"RUN_TIMESTAMP = '${parsedDateAnalyze}'"

    println("globalFilteredAndMonthsDF")
    globalFilteredAndMonthsDF.show()
    println("todayExpression",todayExpression)


    val todayDF = globalFilteredAndMonthsDF.where(expr(todayExpression))

    println("ToDay")
    todayDF.show()

    val todayCount = todayDF.count()

    println("todayCount",todayCount)


    todayCount match{
      case x if x > 0 => {
        val historiqueDF = globalFilteredAndMonthsDF.where(expr(s"RUN_TIMESTAMP < '${parsedDateAnalyze}' "))

        val customerCol = scenario.getPartitionByList.map(col(_)) ++ Seq(col("profondeur"))
        val customerColNames = scenario.getPartitionByList.toSeq

        val customerWithSeuilCols = Seq(col("CUSTOMER"), col("ACCOUNT"), col("ORGANISATION"), col("SEGMENT_KYC"), col("SEGMENT_AML"))

        val toDayCustomerWithSeuil = todayDF.select(customerWithSeuilCols:_*)
          .mapPartitions(iter=>{
            //            val odm = new BigAppsImpl("")
            val odm = new RealODMImplem2()
            iter.map(row=>{
              val cus = new Customer()
              cus.setORGANISATION(row.getAs("ORGANISATION"))
              cus.setSEGMENT_KYC(row.getAs("SEGMENT_KYC"))
              cus.setSEGMENT_AML(row.getAs("SEGMENT_AML"))

              val profondeur = odm.computeProfondeur(scenario.getScenarioName, cus)

              val customerWithSeuil = new CustomerWithSeuil()
              customerWithSeuil.setORGANISATION(row.getAs("ORGANISATION"))
              customerWithSeuil.setSEGMENT_KYC(row.getAs("SEGMENT_KYC"))
              customerWithSeuil.setSEGMENT_AML(row.getAs("SEGMENT_AML"))
              customerWithSeuil.setCUSTOMER(row.getAs("CUSTOMER"))
              customerWithSeuil.setProfondeur(profondeur)

              customerWithSeuil
            })
          })

        val todayUniqueCustomerDF = toDayCustomerWithSeuil.select(customerCol:_*).dropDuplicates()

        val df = historiqueDF.join(todayUniqueCustomerDF, customerColNames, "inner")


        val aggregsDFList = scenario.getAggregationList.toList.map(agg=>{
          computeGroupByAggregate(df, agg, scenario)
        })

        val finalResultDF = scenario.getAlertLevel match{
          case AlertLevel.Transaction => {
            val joinedResultDF =  aggregsDFList.foldLeft(todayDF)(joinWithAggreg) //Uniquement to day
            joinedResultDF
          }
          case _ => {
            val joinedResultDF =  aggregsDFList.foldLeft(todayDF.dropDuplicates(scenario.getAlertLevel.getValue))(joinWithAggreg).dropDuplicates(scenario.getAlertLevel.getValue) //on prend toute la df
            joinedResultDF
          }
        }
        finalResultDF.drop("week", "month", "day")
        finalResultDF
      }

      case _ => {
        throw new Exception(s"To Day Is Empty")
      }

    }

  }

  def joinWithAggreg(todayDF:DataFrame, aggregDF:(Seq[String], DataFrame))(implicit sparkSession: SparkSession):DataFrame ={
    todayDF
      .join(aggregDF._2, aggregDF._1, "left")
  }

  def toRawEnrichedTransactionWithAggregs(scenario: Scenario, row:Row)(implicit sparkSession: SparkSession):EnrichedTransactionWithAggregs ={

    val enrichedTxWithAggregs = new EnrichedTransactionWithAggregs()

    val beanMap = new BeanMap(enrichedTxWithAggregs)
    println(s"beanMap => ${beanMap}")

    scenario.getAggregationList.foreach(agg=>{

      val foundAggregSchema =  row.schema.fields.find(f=>{
        f.name.equals(agg.getAggColName)
      })

      val ARDOUBLE: ArrayType = DataTypes.createArrayType(DataTypes.DoubleType, true)
      val ARSTRING: ArrayType = DataTypes.createArrayType(DataTypes.StringType, true)
      val decimalType: DecimalType = DataTypes.createDecimalType()

      println(s"foundAggregSchema.get.dataType=>${foundAggregSchema.get.dataType}")

      foundAggregSchema.get.dataType match {

        case ARDOUBLE => {
          val field = new ListDoubleField()
          field.setName(agg.getAggColName)
          field.setDataType(ARDOUBLE.toString)
          try{
            val aValue = row.getAs[Seq[java.lang.Double]](agg.getAggColName)
            if(!Objects.isNull(aValue)){
              field.setValue(aValue)
            }

          }catch {
            case e:Exception => {
              println("Cannot set Value List Double")
            }
          }finally {
            enrichedTxWithAggregs.getAggregs.add(field)
          }
        }

        case ARSTRING => {
          val field: ListStringField  = new ListStringField()
          field.setDataType(ARSTRING.toString)
          field.setName(agg.getAggColName)
          try{
            val aValue = row.getAs[Seq[String]](agg.getAggColName)
            if(!Objects.isNull(aValue)){
              field.setValue(aValue)
            }
          }catch {
            case e:Exception => {
              println("Cannot set Value List String")
            }
          }finally {
            enrichedTxWithAggregs.getAggregs.add(field)
          }
        }

        case DataTypes.DoubleType =>{
          val field = new DoubleField()
          field.setName(agg.getAggColName)
          field.setDataType(DoubleType.toString)
          try{
            val aValue = row.getAs[java.lang.Double](agg.getAggColName)
            if(!Objects.isNull(aValue)){
              field.setValue(aValue)
            }
          }catch {
            case e:Exception => {
              println("Cannot set Value Double")
            }
          }finally {
            enrichedTxWithAggregs.getAggregs.add(field)
          }
        }

        case DataTypes.IntegerType =>{
          val field = new IntegerField()
          field.setName(agg.getAggColName)
          field.setDataType(IntegerType.toString)
          try{
            val aValue = row.getAs[Integer](agg.getAggColName)
            if(!Objects.isNull(aValue)){
              field.setValue(aValue)
            }
          }catch {
            case e:Exception => println("Cannot set Value Integer")
          }finally {
            enrichedTxWithAggregs.getAggregs.add(field)
          }
        }

        case DataTypes.LongType  =>{
          val field = new LongField()
          field.setName(agg.getAggColName)
          field.setDataType(LongType.toString)

          try{
            val aValue = row.getAs[java.lang.Long](agg.getAggColName)
            if(!Objects.isNull(aValue)){
              field.setValue(aValue)
            }
          }catch {
            case e:Exception => println("Cannot set Value Long")
          }finally {
            enrichedTxWithAggregs.getAggregs.add(field)
          }
        }

      }
    })

    row.schema.fields.foreach(f=>{
      if(scenario.getAggregationList.filter(ag=>ag.getAggColName.equals(f.name)).length==0){
        setValuedField(enrichedTxWithAggregs, f.name, row.getAs(f.name), beanMap)
      }
    })

    enrichedTxWithAggregs
  }

  def setValuedField(enrichedTxWithAggregs:Object, colName:String, value:Object, beanMap:BeanMap)={
    try{
      //BeanUtils.setProperty(enrichedTxWithAggregs, "INCORPORATION_DATE", row.getAs("INCORPORATION_DATE") )
      val writeMethod = beanMap.getWriteMethod(colName)
      if(!Objects.isNull(writeMethod)){
        writeMethod.invoke(enrichedTxWithAggregs, value)
      }
    }catch {
      case e:Exception => {
        println(s"Cannot Set value for field : ${colName} ${value} ${e.getMessage}")
      }
    }
  }

  def getValuedField(enrichedTxWithAggregs:Object, colName:String, beanMap:BeanMap):Any={
    try{
      //BeanUtils.setProperty(enrichedTxWithAggregs, "INCORPORATION_DATE", row.getAs("INCORPORATION_DATE") )
      val readMethod = beanMap.getReadMethod(colName)
      if(!Objects.isNull(readMethod)){
        val aValue = readMethod.invoke(enrichedTxWithAggregs)
        aValue
      }else{
        null
      }
    }catch {
      case _:Throwable => {
        println(s"Cannot get value for field : ${colName} ")
        ""
      }
    }
  }

  def toAlert(enriched:Dataset[EnrichedTransactionWithAggregs], scenario: Scenario, resultAggregsSchema:StructType)(implicit sparkSession: SparkSession):DataFrame={

    println("To Alert resultAggregsSchema =>")
    println(resultAggregsSchema)

    val fieldsNormal =  Encoders.bean(classOf[EnrichedTransactionWithAggregs]).schema.fields.filter(field=>field.name != "aggregs")
    val keysAggregs = scenario.getAggregationList.map(a=>a.getAggColName)
    val fieldsAggregs = scenario.getAggregationList.map(g=>{
      val aggStructField = resultAggregsSchema.fields.find(f=>f.name.equals(g.getAggColName)).get
      println(s"aggStructField => ${aggStructField}")
      aggStructField
    })

    val keysAlerts = Seq("hit", "violation_message")
    val fieldsAlerts = Seq( StructField("hit",  BooleanType), StructField("violation_message",  StringType))
    val keysScenario = Seq("scenario_name", "global_scope") ++ scenario.getAggregationList.map(e=> s"${e.getAggColName}_scope")
    val fieldsScenario = keysScenario.map(k=>StructField(k,  StringType))
    val fileds:Seq[StructField] = fieldsNormal ++ fieldsAggregs ++ fieldsAlerts ++ fieldsScenario
    val schema =  StructType(fileds)
    val encoder = org.apache.spark.sql.catalyst.encoders.RowEncoder(schema)
    val globalScopes = scenario.getScopes match {
      case s if s == null || s.size()<=0 => {
        println("pas de global Scopes")
        ""
      }
      case _ => {
        println("scenario.getScopes.asScala concatAndStringScopes")
        scenario.getScopes.asScala.foldLeft(""){concatAndStringScopes}
      }
    }
    enriched.mapPartitions(iter=>{
      val odm = new RealODMImplem2()
      iter.map(e=>{

        val alert = odm.evaluateRules(e, scenario)

        val hit = Objects.isNull(alert) match {
          case true => false
          case false => alert.getHit
        }

        val justification =Objects.isNull(alert) match {
          case true => false
          case false => alert.getJustificationMessage
        }
        val beanMap = new BeanMap(e)
        val list = Encoders.bean(classOf[EnrichedTransactionWithAggregs]).schema.fields.filter(field=>field.name != "aggregs")
          .map(f=>getValuedField(e, f.name, beanMap)) ++ scenario.getAggregationList.map(aScenario=>{

          val f = e.getAggregs.find(g=>{

            g.getName.equals(aScenario.getAggColName)

          }).get

          if(f.isInstanceOf[ListStringField]){

            if(Objects.isNull(f.asInstanceOf[ListStringField].getValue)){
              Seq("")
            }else{
              f.asInstanceOf[ListStringField].getValue.toList
            }

          }else if(f.isInstanceOf[DoubleField]){
            System.err.println("is Double Field ")
            if(Objects.isNull(f.asInstanceOf[DoubleField].getValue)){
              null
            }else{
              f.asInstanceOf[DoubleField].getValue
            }
          }else if (f.isInstanceOf[LongField]){
            System.err.println("is LongField ")
            if(Objects.isNull(f.asInstanceOf[LongField].getValue)){
              null
            }else{
              f.asInstanceOf[LongField].getValue
            }
          }else if(f.isInstanceOf[FloatField]){
            if(Objects.isNull(f.asInstanceOf[FloatField].getValue)){
              null
            }else{
              f.asInstanceOf[FloatField].getValue
            }
          }else if(f.isInstanceOf[ListDoubleField]){
            if(Objects.isNull(f.asInstanceOf[ListDoubleField].getValue)){
              Seq.empty[Double]
            }else{
              f.asInstanceOf[ListDoubleField].getValue.toList
            }
          }else if(f.isInstanceOf[IntegerField]){
            if(Objects.isNull(f.asInstanceOf[IntegerField].getValue)){
              null
            }else{
              f.asInstanceOf[IntegerField].getValue
            }
          }else {
            // new String("")
            s"Unknown value for ${f.getName}"
          }
        }) ++ Seq(hit, justification) ++ Seq(

          scenario.getScenarioName,
          globalScopes
        ) ++ scenario.getAggregationList.map(agg => {
          agg.getAggregationScopes match {
            case null => ""
            case _ =>  agg.getAggregationScopes.asScala.foldLeft(""){concatAndStringScopes}
          }

        })
        Row(list:_*)
      })
    })(encoder)


  }

}
