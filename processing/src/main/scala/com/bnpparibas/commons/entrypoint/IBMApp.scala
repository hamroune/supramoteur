package com.bnpparibas.commons.entrypoint

import java.util

import com.bnpparibas.commons.models.domain.{EnrichedTransactionWithAggregs}
import com.bnpparibas.commons.models.types._
import org.apache.spark.sql.SparkSession

import scala.collection.JavaConversions._

object IBMApp extends App {

  lazy implicit val sparkSession = SparkSession
    .builder
    .appName("Test FRAMEWORK")
    .master("local[*]")
    .getOrCreate()

  import com.bnpparibas.commons.imp.BnpImplicits._




  val e= new EnrichedTransactionWithAggregs()
  val cumul = new DoubleField()
  cumul.setName("cumul")
  cumul.setValue(100d)

  e.setAggregs(util.Arrays.asList(cumul))
  e.setCUSTOMER("A")
  e.setTXN_AMOUNT_ORIG(1000d)


  val e2= new EnrichedTransactionWithAggregs()

  e2.setAggregs(util.Arrays.asList(cumul))
  e2.setCUSTOMER("A")
  e2.setTXN_AMOUNT_ORIG(1000d)


  val aggregs = Seq(
    e, e2
  )


//  val df = sparkSession.createDataset(aggregs)
//  val r = df.mapPartitions(iter=>{
//    val list =  iter.toList.map(e=>{
//      val listSteings = e.getAggregs.map(f => {
//        if(f.isInstanceOf[ListStringField]){
//          val listString = f.asInstanceOf[ListStringField].getValue.toString
//          println(s"listString = ${listString}")
//          ""
//        }else{
//          ""
//        }
//      })
//
//      println(s"list Strings = ${listSteings}")
//      listSteings
//    })
//
//
//    list.iterator
//  }).toDF
//
//  r.show



  println("START 5")

}