package com.bnpparibas.commons.api

import com.bnpparibas.commons.models.processed.RawEnrichedTransaction
import com.bnpparibas.commons.services.{ReadFiles, TransCodificationService}
import integration.PersistenceManager
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{Dataset, SparkSession}
import xom.{Alert, Day}

case class PersistenceManagerImplementation() extends PersistenceManager {
  import com.bnpparibas.commons.imp.BnpImplicits._

  def getCBEnrichedTransactionDataset(spark: SparkSession): Dataset[RawEnrichedTransaction] = {
    ReadFiles.readCBRawEnrichedTransaction(spark)
  }

  def getREnrichedTransactionDataset(spark: SparkSession): Dataset[RawEnrichedTransaction] = {
    ReadFiles.readRRawEnrichedTransaction(spark)
  }

  def getClientRDD(enrichedTransactionDS: Dataset[RawEnrichedTransaction]): RDD[xom.Client] = {

    enrichedTransactionDS.mapPartitions(it => {
      it.map(TransCodificationService.toXomClient(_))
    }).dropDuplicates().rdd
  }

  def getTransactionRDD(enrichedTransactionDS: Dataset[RawEnrichedTransaction]): RDD[xom.Transaction] = {
    enrichedTransactionDS.dropDuplicates("CUSTOMER").mapPartitions(it => {
      it.map(TransCodificationService.toXomTransaction(_))
    }).rdd

  }

  override def getData(sc: SparkContext): (RDD[xom.Client], RDD[xom.Transaction]) = {

      implicit var spark = SparkSession
        .builder
        //.master("local[*]")
        .enableHiveSupport()
        .config("spark.driver.allowMultipleContexts", true)
        .getOrCreate()

    val enrichedTransactionDS = getCBEnrichedTransactionDataset(spark)
//    val enrichedTransactionDS = getREnrichedTransactionDataset(spark)
    val clientRDD = getClientRDD(enrichedTransactionDS)
    val transactionRDD = getTransactionRDD(enrichedTransactionDS)
    (clientRDD, transactionRDD)
  }

  override def saveData(sc: SparkContext, alerts: RDD[Alert]): Unit = {
    alerts.saveAsTextFile("/BDDF/DS/Indus/Emc2/Data/AML/OUTPUT/ALERTS_I_870_P_CB_TRANSACTION")
  }

  override def getDay: Day = {
    new Day("2020/12/23")
  }
}
