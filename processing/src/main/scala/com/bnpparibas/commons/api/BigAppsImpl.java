package com.bnpparibas.commons.api;

import com.bnpparibas.commons.models.domain.*;
import com.bnpparibas.commons.models.domain.enums.AlertLevel;
import com.bnpparibas.commons.models.raw.Customer;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.Serializable;
import java.util.*;

public class BigAppsImpl implements ODMInterface, Serializable {
    private Scenario scenario;
    private String yaml;
    private Map<String, Scenario> map = new HashMap();


    static ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
    static{
        objectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
        objectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT);

    }
    public BigAppsImpl(String yaml){
        this.yaml = yaml;

//Scenario DS_11_B_1_A
        Scope scopeCPListC = Scope.builder().scope(" CREDIT_DEBIT_CODE='C' ").build();
        Scope scopeCPListD = Scope.builder().scope(" CREDIT_DEBIT_CODE='D' ").build();

        Aggregation aggregation1 = Aggregation.builder()
                .aggColName("counter_party_list_credit")
                .aggFunction("collect_set")
                .aggMetric("COUNTER_PARTY")
                .aggTimePeriodSize("12")
                .aggTimePeriod("month")
                .aggTimePeriodSelected("0")
                .aggregationScopes(Arrays.asList(scopeCPListC))
                .build();

        Aggregation aggregation2 = Aggregation.builder()
                .aggColName("counter_party_list_debit")
                .aggFunction("collect_set")
                .aggMetric("COUNTER_PARTY")
                .aggTimePeriodSize("12")
                .aggTimePeriod("month")
                .aggTimePeriodSelected("0")
                .aggregationScopes(Arrays.asList(scopeCPListD))
                .build();

        Aggregation aggregation3 = Aggregation.builder()
                .aggColName("cumul_debit")
                .aggFunction("sum")
                .aggMetric("TXN_AMOUNT_ORIG")
                .aggTimePeriodSize("12")
                .aggTimePeriod("month")
                .aggTimePeriodSelected("0")
                .aggregationScopes(Arrays.asList(scopeCPListD))
                .build();

        Aggregation aggregation4 = Aggregation.builder()
                .aggColName("cumul_credit")
                .aggFunction("sum")
                .aggMetric("TXN_AMOUNT_ORIG")
                .aggTimePeriodSize("12")
                .aggTimePeriod("month")
                .aggTimePeriodSelected("0")
                .aggregationScopes(Arrays.asList(scopeCPListC))
                .build();




        List<Aggregation> aggregationList = new ArrayList<>();
        aggregationList.add(aggregation1);
//        aggregationList.add(aggregation2);
//        aggregationList.add(aggregation3);
//        aggregationList.add(aggregation4);

        Scope scope1 = Scope.builder().scope(" INSTRUMENT_CROSSBORDER in ('SEPA_CROSSBORDER', 'SWIFT_CROSSBORDER') ").build();
        Scope scope2 = Scope.builder().scope("  HIGHRISK_FLAG in ('2', '3') ").build();
        Scope scope3 = Scope.builder().scope("  TXN_ELIGIBLE_FOR_MARKET='Y' ").build();

        List<Scope> scopes=new ArrayList<>();
        scopes.add(scope1);
        scopes.add(scope2);
        scopes.add(scope3);

        Scenario scenarioDS11B1A = Scenario.builder()
                .scenarioName("DS_11_B_1_a")
                .scopes(scopes)
                .partitionByList(Arrays.asList("CUSTOMER"))
                .aggregationList(aggregationList)
                .alertLevel(AlertLevel.Client)
                .build();


        Scenario scenarioDS11B1B = Scenario.builder()
                .scenarioName("DS_11_B_1_b")
                .scopes(scopes)
                .partitionByList(Arrays.asList("CUSTOMER"))
                .aggregationList(aggregationList)
                .alertLevel(AlertLevel.Client)
                .build();


        //////////////////////////DS 52 C+CASH///////////////////

        Scope scopeDS522 = Scope.builder().scope(" CUSTOMER_CASH_INTENSIVE='Y' OR INSTRUMENT='CASH'" +
                "OR (" +
                "   ORGANISATION!='CORP' AND " +
                "   INSTRUMENT='MISCELLAN' AND " +
                "   TXN_USR_DTLS in" +
                "       ('TIRGUI', 'TIRDAB', 'REPRGUI', 'REPRDAB', 'IMPEGUI', 'IMPEDAB')" +
                ")")
        .build();

        List<Scope> scopesDS52Espece=new ArrayList<>();
        scopesDS52Espece.add(scopeDS522);

        Aggregation cumul_flux_crediteur_xx_mois = Aggregation.builder()
                .aggColName("cumul_flux_crediteur_xx_mois")
                .aggFunction("sum")
                .aggMetric("TXN_AMOUNT_ORIG")
                .aggTimePeriodSize("12")
                .aggTimePeriod("month")
                .aggTimePeriodSelected("0")
                .aggregationScopes(new ArrayList<>())
                .build();

        Aggregation cumul_flux_crediteur_especes_xx_mois = Aggregation.builder()
                .aggColName("cumul_flux_crediteur_especes_xx_mois")
                .aggFunction("sum")
                .aggMetric("TXN_AMOUNT_ORIG")
                .aggTimePeriodSize("12")
                .aggTimePeriod("month")
                .aggTimePeriodSelected("0")
                .aggregationScopes(scopesDS52Espece)
                .build();

        // D
        //Scope scopeDS521D = Scope.builder().scope(" CREDIT_DEBIT_CODE = 'D' ").build();

        List<Scope> scopesDS52EspecD=new ArrayList<>();
        scopesDS52EspecD.add(scopeDS522); //COMMUN

        Aggregation nombre_flux_crediteur_xx_mois = Aggregation.builder()
                .aggColName("nombre_flux_crediteur_xx_mois")
                .aggFunction("count")
                .aggTimePeriodSize("12")
                .aggTimePeriod("month")
                .aggTimePeriodSelected("0")
                .aggregationScopes(new ArrayList<>())
                .build();

        Aggregation nombre_flux_crediteur_especes_xx_mois = Aggregation.builder()
                .aggColName("nombre_flux_crediteur_especes_xx_mois")
                .aggFunction("count")
                .aggTimePeriodSize("12")
                .aggTimePeriod("month")
                .aggTimePeriodSelected("0")
                .aggregationScopes(scopesDS52EspecD)
                .build();

        List<Aggregation> aggregationListDS52 = new ArrayList<>();

//        aggregationListDS52.add(cumul_flux_crediteur_xx_mois);
//        aggregationListDS52.add(cumul_flux_crediteur_especes_xx_mois);
//
//        aggregationListDS52.add(nombre_flux_crediteur_xx_mois);
        aggregationListDS52.add(nombre_flux_crediteur_especes_xx_mois);

        Scope scopeDS52C = Scope.builder().scope("CREDIT_DEBIT_CODE='C'").build();
        List<Scope> scopesDS52=new ArrayList<>();
        scopesDS52.add(scopeDS52C);

        Scenario scenarioDS52A2 =  Scenario.builder()
                .scenarioName("DS_52_A_2")
                .scopes(scopesDS52)
                .partitionByList(Arrays.asList("CUSTOMER"))
                .aggregationList(aggregationListDS52)
                .alertLevel(AlertLevel.Client)
                .build();

        map.put("DS_11_B_1_a", scenarioDS11B1A);
        map.put("DS_11_B_1_b", scenarioDS11B1B);
        map.put("DS_52_A_2", scenarioDS52A2);
    }

    @Override
    public Integer computeProfondeur(String s, Customer customer) {
        return 12;
    }

    @Override
    public Scenario retrieveScenario(String scenarioName) {
        return map.get(scenarioName);
    }

    @Override
    public Alert evaluateRules(EnrichedTransactionWithAggregs enrichedTransactionWithAggregs, Scenario scenario) {
        Alert alert = new Alert();
        alert.setHit(true);
        alert.setJustificationMessage("Error from Big Apps");
        return alert;
    }

}
