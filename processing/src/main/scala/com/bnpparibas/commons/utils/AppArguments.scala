package com.bnpparibas.commons.utils

import org.apache.commons.cli.{GnuParser, Option, Options}

object AppArguments{
  val options = new Options()
  def parse(args: Array[String]):AppArguments = {
    val transactionPath = new Option("t", "transaction_file", true, "Spécifier le path du fichier CSV transaction")
    transactionPath.setRequired(false)
    options.addOption(transactionPath)


    val clientCSV = new Option("c", "client_file", true, "Spécifier le path du fichier CSV client")
    clientCSV.setRequired(false)
    options.addOption(clientCSV)

    val transactionTypepath = new Option("tt", "transaction_type_file", true, "Spécifier le path du fichier CSV de transaction_type")
    transactionTypepath.setRequired(false)
    options.addOption(transactionTypepath)

    val countryPath = new Option("cn", "country_file", true, "Spécifier le path du fichier CSV de country")
    countryPath.setRequired(false)
    options.addOption(countryPath)

    val busSegPath = new Option("cn", "business_segmentation_file", true, "Spécifier le path du fichier CSV de business segmentation ")
    busSegPath.setRequired(false)
    options.addOption(busSegPath)


    val dateRef = new Option("dr", "date_reference", true, "Spécifier la date référence ")
    dateRef.setRequired(false)
    options.addOption(dateRef)



    val parser = new GnuParser().parse( options, args)
    AppArguments(
      parser.getOptionValue(transactionPath.getLongOpt),
      parser.getOptionValue(clientCSV.getLongOpt),
      parser.getOptionValue(transactionTypepath.getLongOpt),
      parser.getOptionValue(countryPath.getLongOpt),
      parser.getOptionValue(busSegPath.getLongOpt),
      parser.getOptionValue(dateRef.getLongOpt)

    )
  }
}

case class  AppArguments(transactionPath:String, clientFile:String, transactionTypepath:String,countryPath:String, busSegPath:String, dateRef:String)
