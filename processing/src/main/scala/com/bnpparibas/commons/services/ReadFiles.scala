package com.bnpparibas.commons.services

import com.bnpparibas.commons.models.processed.{RRawEnrichedTransaction, RawEnrichedTransaction}
import com.bnpparibas.commons.models.raw._
import com.bnpparibas.commons.utils.AppArguments
import com.bnpparibas.processing.entrypoint.GenerationEnrichedRtransaction.sparkSession
import com.bnpparibas.processing.services.AMLProcessing
import org.apache.commons.beanutils.BeanMap
import org.apache.spark.sql.catalyst.encoders.{ExpressionEncoder, RowEncoder}
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types._
import org.apache.spark.sql.{Dataset, Encoders, SparkSession}
import org.apache.spark.sql.functions.{col, expr, lit, when}

object ReadFiles {

  import com.bnpparibas.commons.imp.BnpImplicits._



  val RSelect = Seq("TXN_USR_DTLS",
    "ACCOUNT_SOURCE_REF_ID",
    "BRANCH_ID",
    "TXN_SOURCE_TYPE_CODE",
    "ORIGINATOR_NAME",
    "BENEFICIARY_NAME",
    "ORIGINATOR_BANK_NAME",
    "BENEFICIARY_BANK_NAME"
  )
  val CBSelect = Seq(
    "ORIG_BANK_COUNTRY_CODE",
    "BENE_BANK_COUNTRY_CODE",
    "TXN_SRC_TYPE_CODE",
    "ISO_CODE",
    "TAX_HAVEN_FLAG",
    "HIGHRISK_FLAG",
    "FATF_FLAG",
    "NARCOTIC_FLAG",
    "OFAC_FLAG",
    "SUSPICIOUS_FLAG",
    "EUROPE_FLAG",
    "TERRORISTHAVEN_FLAG",
    "CONTINENT_NAME",
    "REGION_CODE",
    "NO_EXTERNAL_ACCESS",
    "FRDDC_RISK",
    "PF_RISK",
    "AMLCB_RISK_FLAG",
    "PFCD_RISK",
    //"CODE_APE_ID",
   // "data_label_name",
    //"CUSTOMER_CASH_INTENSIVE",
    "CROSSBORDER_FLAG",
    "INSTRUMENT_CROSSBORDER",
    "COUNTER_PARTY_BANK_COUNTRY_CODE",
    "TXN_ELIGIBLE_FOR_MARKET",
    "CASH_INTENSIVE_FLAG",
    "COUNTRY_CODE",
    "COUNTER_PARTY",
    "ACCOUNT",
    "COUNTRY_COUNTER_PARTY",
    "VIR_INTERNATIONAL_FLAG",
    "ACOUNT_COUNTER_PARTY",
    "COUNTER_PARTY",
    "INSTRUMENT_CROSSBORDER",
    "HIGHRISK_FLAG",
    "TXN_ELIGIBLE_FOR_MARKET")

  implicit var sparkSession = SparkSession
    .builder
    .appName("Test")
    .master("local[*]")
    //.enableHiveSupport()
    .getOrCreate()



  def readCustomers(appArs: AppArguments): Dataset[Customer] = {
    //Customer
    val customerDF = sparkSession.read.orc(appArs.clientFile)
    val customerDS: Dataset[Customer] = AMLTransCodification.transformCustomer(customerDF)
    customerDS//.repartition(8000,col("CUSTOMER_SOURCE_UNIQUE_I"))
  }

  def readTransactions(appArs: AppArguments): Dataset[CBTransaction] = {
    //CBTransaction
    //where("day_part < '20200101'").count =  246 millions
    //where("day_part < '20200601' AND day_part >= '20200101'").count = 233 millions
    //where("day_part < '20201001' AND day_part >= '20200601'").count = 194 millions
    //df.where("day_part >= '20201001'").count = 195 millions
    val df = sparkSession.read.format("orc")
      .load(appArs.transactionPath).where(expr("day_part < '20200101'"))
    df.as[CBTransaction]
  }

  def readBusinessSegmentation(appArs: AppArguments): Dataset[BusinessSegmentation] = {

    sparkSession.read.format("csv")
      .option("header", "true")
      .option("enforceSchema", "false")
      .option("delimiter", ";")
      .load(appArs.busSegPath)
      .dropDuplicates(Seq("business_segment_1_value"))
      .withColumnRenamed("flag", "CUSTOMER_CASH_INTENSIVE")
      .withColumnRenamed("id", "CODE_APE_ID")
      .as[BusinessSegmentation](Encoders.bean(classOf[BusinessSegmentation]))

  }

  def readCountry(appArs: AppArguments): Dataset[Country] = {
    sparkSession.read.format("orc")
      .load(appArs.countryPath)
      .drop("day_part")
      .as[Country]
  }

  def readTransactionType(appArs: AppArguments): Dataset[TransactionType] = {

    sparkSession.read.format("orc")

      .load(appArs.transactionTypepath)
      .drop("day_part")
      .dropDuplicates(Seq("TXN_TYPE_CODE"))

      .as[TransactionType]
  }




  def readCBRawEnrichedTransaction(spark: SparkSession): Dataset[RawEnrichedTransaction]={
    val path = "/BDDF/DS/Indus/Emc2/Data/AML/OUTPUT/ENRICHED_870_P_CB_TRANSACTION"
    println(s"Path=${path}")
    val allRawEnrichedTransactionSelect = encoderRawEnrichedTransaction.schema.fields.map(_.name).filter( a => {
      (RSelect.indexOf(a) < 0 && CBSelect.indexOf(a) < 0)
    }).map(col(_))

    val cols = allRawEnrichedTransactionSelect  ++ CBSelect.map(col(_))

    val cbDF = spark.read.orc(path)
      .select(cols:_*)

    val rawEnrichedTxDF = RSelect
      .foldLeft(cbDF) {
        (df, colName) => {
          df.withColumn(colName, lit(null))
        }
      }

    rawEnrichedTxDF.as[RawEnrichedTransaction]

  }

  def readRRawEnrichedTransaction(spark: SparkSession): Dataset[RawEnrichedTransaction]={
    val path = "/BDDF/DS/Indus/Emc2/Data/AML/OUTPUT/ENRICHED_3_M_R_12_TRANSACTION"
    println(s"Path=${path}")

    val commonCols = encoderRawEnrichedTransaction.schema.fields.map(_.name).filter( a => {
      (RSelect.indexOf(a) < 0 && CBSelect.indexOf(a) < 0)
    }).map(col(_))

    val cols = commonCols  ++ RSelect.map(col(_))

    println(s"cols à selectionner => ${cols}")

    val cbDF = spark.read.orc(path)
//      .withColumnRenamed("CUSTOMER_SOURCE_UNIQUE_ID", "CUSTOMER")
//      .withColumnRenamed("SOURCE_TXN_UNIQUE_ID", "SRC_TXN_UNIQUE_ID")
//      .withColumnRenamed("ACCOUNT_SOURCE_UNIQUE_ID", "ACCOUNT")
//      .withColumn("CUSTOMER_CASH_INTENSIVE", lit(true))
//      .drop(col("ORG_UNIT_CODE"))
      .select(cols:_*)

    cbDF.printSchema()

    val rawEnrichedTxDF = CBSelect
      .foldLeft(cbDF) {
        (df, colName) => {
          df.withColumn(colName, lit(null))
        }
      }

    println("rawEnrichedTxDF.show")
    rawEnrichedTxDF.show

    println("rawEnrichedTxDF.printSchema")
    rawEnrichedTxDF.printSchema()

    rawEnrichedTxDF.as[RawEnrichedTransaction]

  }

  def readRTransaction(appArs: AppArguments): Dataset[RTransaction]={
    ///////2////
    //.where(" day_part < '20200501' ")
    //.where(" day_part >= '20200501' ")
    sparkSession.read.orc("/BDDF/DS/Indus/Emc2/Data/AML/ap01433_p_transactions_anonymized")
      .where(" day_part < '20200501' ")
      .withColumnRenamed("CUSTOMER_SOURCE_UNIQUE_ID", "CUSTOMER")
      .withColumnRenamed("SOURCE_TXN_UNIQUE_ID", "SRC_TXN_UNIQUE_ID")
      .withColumnRenamed("ACCOUNT_SOURCE_UNIQUE_ID", "ACCOUNT")
      .withColumnRenamed("ORG_UNIT_CODE", "ORGANISATION")
      .as[RTransaction]
  }

}