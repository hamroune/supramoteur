package com.bnpparibas.commons.services

import com.bnpparibas.commons.models.processed._
import com.bnpparibas.commons.models.raw._
import org.apache.spark.sql.functions.{col, expr, lit, when}
import org.apache.spark.sql.{DataFrame, Dataset, Encoders}

object AMLTransCodification {
  import com.bnpparibas.commons.imp.BnpImplicits._

  def transformCustomer(df: DataFrame): Dataset[Customer] = {

    val cus = df.drop("RUN_TIMESTAMP")
      .drop("day_part")
      .withColumnRenamed("BROKER_CODE", "SEGMENT_AML")
      .withColumnRenamed("CUSTOMER_SEGMENT_1", "SEGMENT_KYC")
      .withColumnRenamed("BUSINESS_SEGMENT_1", "SEGMENT_APE")
      .withColumnRenamed("PRIME_BRANCH_ID", "BRANCH")
      .withColumnRenamed("ACQUISITION_DATE", "CUSTOMER_AGE")
      .withColumnRenamed("PEP_FLAG_INGESTED", "SEGMENT_PPE")
      .withColumnRenamed("BLACK_LISTED_FLAG", "SEGMENT_SRA")
      .withColumnRenamed("BANKRUPT_FLAG", "SEGMENT_BLA")
      .withColumnRenamed("ORGUNIT_CODE", "ORGANISATION")

    cus.as[Customer]
  }

  def jointTransactionWithTransactionType(dsTransaction: Dataset[CBTransaction], dsTransactionType: Dataset[TransactionType]): Dataset[RawTransactionWithCBTransactionType] = {

    val df = dsTransaction.join(dsTransactionType, dsTransaction("TXN_SRC_TYPE_CODE") === dsTransactionType("TXN_TYPE_CODE"), "left")
      .drop("TXN_TYPE_CODE")

    df.as[RawTransactionWithCBTransactionType](Encoders.bean(classOf[RawTransactionWithCBTransactionType]))

  }

  def enrichedTransactionWithAddJoinColumns(transactionWithTransactionTypeDS: Dataset[RawTransactionWithCBTransactionType]): Dataset[RawCBTransactionWithAddedColumns] = transactionWithTransactionTypeDS
    .withColumn("CUSTOMER", when(expr(""" CREDIT_DEBIT_CODE= 'C' """),
      col("BENE_SUBCUST_SRC_UNIQUE_ID"))
      .otherwise(col("ORIG_SUBCUST_SRC_UNIQUE_ID")))

    .withColumn("COUNTRY_CODE", when(expr(""" CREDIT_DEBIT_CODE= 'C' """),
      col("BENE_BANK_COUNTRY_CODE"))
      .otherwise(col("ORIG_BANK_COUNTRY_CODE")))

    .withColumn("COUNTER_PARTY", when(expr(""" CREDIT_DEBIT_CODE= 'C' """),
      col("ORIG_ACCOUNT_NUMBER"))
      .otherwise(col("BENE_ACCOUNT_NUMBER")))

    .withColumn("ACCOUNT", when(expr(""" CREDIT_DEBIT_CODE= 'C' """),
      col("BENE_ACCOUNT_NUMBER"))
      .otherwise(col("ORIG_ACCOUNT_NUMBER")))

    .withColumn("COUNTRY_COUNTER_PARTY", when(expr(""" CREDIT_DEBIT_CODE= 'C' """),
      col("ORIG_BANK_COUNTRY_CODE"))
      .otherwise(col("BENE_BANK_COUNTRY_CODE"))) //IL FAUT TESTER AUSSI BENE_BANK_COUNTRY_CODE

    .withColumn("ACOUNT_COUNTER_PARTY", when(expr(""" CREDIT_DEBIT_CODE= 'C' """),
      col("ORIG_ACCOUNT_NUMBER"))
      .otherwise(col("BENE_ACCOUNT_NUMBER")))

    .withColumn("VIR_INTERNATIONAL_FLAG",
      when(expr(
        """ (CREDIT_DEBIT_CODE= 'C' AND
          | ORIG_BANK_COUNTRY_CODE  != 'FR') OR
          | (CREDIT_DEBIT_CODE= 'D' AND
          | BENE_BANK_COUNTRY_CODE  != 'FR')  """.stripMargin),
        lit("Y"))
        .otherwise(lit("N")))

    .as[RawCBTransactionWithAddedColumns](Encoders.bean(classOf[RawCBTransactionWithAddedColumns]))

  def enrichedTransactions(transactionWithAddJoinColumnsDS: Dataset[RawCBTransactionWithAddedColumns], customerDS: Dataset[Customer], countryDS: Dataset[Country], businessSegmentationDS: Dataset[BusinessSegmentation]): Dataset[RawEnrichedCBTransaction]={

    val enrichedTransactionDF = transactionWithAddJoinColumnsDS
      .join(customerDS, transactionWithAddJoinColumnsDS("CUSTOMER") === customerDS("CUSTOMER_SOURCE_UNIQUE_I"), "inner")
      .join(countryDS, transactionWithAddJoinColumnsDS("COUNTRY_COUNTER_PARTY") === countryDS("COUNTRY_CODE"), "left")
      .drop("CUSTOMER_SOURCE_UNIQUE_I")
      .drop(countryDS("COUNTRY_CODE"))

    val df = enrichedTransactionDF
      .join(businessSegmentationDS, enrichedTransactionDF("SEGMENT_APE") === businessSegmentationDS("business_segment_1_value"), "left")
      .drop("business_segment_1_value")

      .withColumn("CROSSBORDER_FLAG",
        when(expr(
          """ (CREDIT_DEBIT_CODE= 'C' AND
            |ORIG_BANK_COUNTRY_CODE  != 'FR' AND EUROPE_FLAG='Y') OR
            | (CREDIT_DEBIT_CODE= 'D' AND BENE_BANK_COUNTRY_CODE  != 'FR' AND
            | EUROPE_FLAG='Y')  """.stripMargin),
          lit("Y"))
          .otherwise(lit("N")))

      .withColumn("INSTRUMENT_CROSSBORDER",
        when(expr(""" CROSSBORDER_FLAG='Y' AND INSTRUMENT = 'WIRE' AND CURRENCY_CODE_ORIG='EUR'  """),
          lit("SEPA_CROSSBORDER"))
          .otherwise(when(expr(""" CROSSBORDER_FLAG='Y' AND INSTRUMENT = 'SWIFT'  """),
            lit("SWIFT_CROSSBORDER"))
            .otherwise(lit("UNKNOWN"))
          ))

      .withColumn("COUNTER_PARTY_BANK_COUNTRY_CODE", when(expr(""" CREDIT_DEBIT_CODE= 'C' """),
        col("ORIG_BANK_COUNTRY_CODE"))
        .otherwise(col("BENE_BANK_COUNTRY_CODE")))

      .withColumn("TXN_ELIGIBLE_FOR_MARKET",
        when(expr(
          """ (ORGANISATION = 'CORP' AND
            | TXN_SRC_TYPE_CODE not in ('0553','4160', '4110', '0506')) OR
            |  (ORGANISATION != 'CORP')  """.stripMargin),
          lit("Y"))
          .otherwise(lit("N")))

      .withColumn("CASH_INTENSIVE_FLAG",
        when(expr(
          """ CUSTOMER_CASH_INTENSIVE OR INSTRUMENT = 'CASH' OR (
            | ORGANISATION != 'CORP' AND
            | INSTRUMENT == "MISCELLAN"
            | )  """.stripMargin),
          lit("Y"))
          .otherwise(lit("N")))

      .as[RawEnrichedCBTransaction](Encoders.bean(classOf[RawEnrichedCBTransaction]))

    df
  }

  def jointRTransactionWithTransactionType(dsTransaction: Dataset[RTransaction], dsTransactionType: Dataset[TransactionType]): DataFrame = {

    val df = dsTransaction.join(dsTransactionType.drop("CREDIT_DEBIT_CODE"), dsTransaction("TXN_SOURCE_TYPE_CODE") === dsTransactionType("TXN_TYPE_CODE"), "left")
      .drop("TXN_TYPE_CODE")

    df
      //.as[RawTransactionWithCBTransactionType](Encoders.bean(classOf[RawTransactionWithCBTransactionType]))

  }

}
