package com.bnpparibas.commons.services

import java.util
import java.util.Date

import com.bnpparibas.commons.models.processed.{RawEnrichedTransaction}
import xom._

object TransCodificationService {

  def toXomClient(enrichedTransaction: RawEnrichedTransaction): Client = {
    val client = new Client
    try {
      val marche = Marche.lookup(enrichedTransaction.getORGANISATION)
      System.out.println("toXomClient marche =>" + marche.getValue)
      client.setMarche(marche)
    } catch {
      case e: Exception =>
        client.setMarche(Marche.Default)
    }
    client.setId(enrichedTransaction.getCUSTOMER)
    client.setCashIntensive("Y".equals(enrichedTransaction.getCUSTOMER_CASH_INTENSIVE))
    client.setCodeIBAN(enrichedTransaction.getACCOUNT)
    //client.setCotationRSF();
    client.setSegmentAML(enrichedTransaction.getSEGMENT_AML)
    client.setSegmentKYC(enrichedTransaction.getSEGMENT_KYC)
    client.setSegmentAPE(enrichedTransaction.getSEGMENT_APE)
    client.setSegmentPPE(enrichedTransaction.getSEGMENT_PPE)
    client.setSegmentBLA(enrichedTransaction.getSEGMENT_BLA)
    client.setSegmentSRA(enrichedTransaction.getSEGMENT_SRA)
    client.setCountryRiskScore(enrichedTransaction.getHIGHRISK_FLAG)
    //client.setCustomerAge(enrichedTransaction.getCUSTOMER_AGE());
    client.setBranch(enrichedTransaction.getBRANCH)
    client.setOrganization(enrichedTransaction.getORGANISATION)
    client
  }

  def toXomTransaction(enrichedTransaction: RawEnrichedTransaction): Transaction = {
    val transaction: Transaction = new Transaction
    transaction.setId(enrichedTransaction.getSRC_TXN_UNIQUE_ID)
    transaction.setClientId(enrichedTransaction.getCUSTOMER)
    transaction.setAmount(enrichedTransaction.getTXN_AMOUNT_ORIG)
    transaction.setDay(Day.fromDate(new Date(enrichedTransaction.getRUN_TIMESTAMP.getTime)))
    transaction.setCounterpartyAccountNumber(enrichedTransaction.getACOUNT_COUNTER_PARTY)
    transaction.setClientCountry(enrichedTransaction.getCOUNTRY_CODE)
    transaction.setCounterpartyCountry(enrichedTransaction.getCOUNTRY_COUNTER_PARTY)
    transaction.setClientCountryScore(enrichedTransaction.getHIGHRISK_FLAG)
    transaction.setCounterpartyCountryScore(enrichedTransaction.getCOUNTRY_COUNTER_PARTY)
    val flags: util.Set[TransactionFlag] = new util.TreeSet[TransactionFlag]
    if (enrichedTransaction.getCREDIT_DEBIT_CODE == "C") flags.add(TransactionFlag.CREDIT)
    else flags.add(TransactionFlag.DEBIT)
    if (enrichedTransaction.getHIGHRISK_FLAG eq "3") flags.add(TransactionFlag.COUNTERPARTY_VERY_HIGH_RISK_COUNTRY)
    else if (enrichedTransaction.getHIGHRISK_FLAG eq "2") flags.add(TransactionFlag.COUNTERPARTY_HIGH_RISK_COUNTRY)
    if (enrichedTransaction.getTAX_HAVEN_FLAG == "Y") flags.add(TransactionFlag.COUNTERPARTY_PRIVILEGED_TASK_COUNTRY)
    if (enrichedTransaction.getVIR_INTERNATIONAL_FLAG == "Y") flags.add(TransactionFlag.TR_INTERNATIONAL_TRANSFER)
    if (enrichedTransaction.getINSTRUMENT_CROSSBORDER == "SEPA_CROSSBORDER") flags.add(TransactionFlag.TR_SEPA)
    else if (enrichedTransaction.getINSTRUMENT_CROSSBORDER == "SWIFT_CROSSBORDER") flags.add(TransactionFlag.TR_SWIFT)
    if (enrichedTransaction.getCROSSBORDER_FLAG == "Y") flags.add(TransactionFlag.TR_CROSSBORDER)
    transaction.setFlags(flags)
    transaction
  }

}
