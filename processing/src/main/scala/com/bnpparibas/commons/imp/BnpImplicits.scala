package com.bnpparibas.commons.imp

import com.bnpparibas.commons.models.domain._
import com.bnpparibas.commons.models.processed._
import com.bnpparibas.commons.models.raw._
import com.bnpparibas.commons.models.types.{DoubleField, IntegerField, ListDoubleField, ListStringField}
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema
import org.apache.spark.sql.{Encoders, Row}

object BnpImplicits {
  //implicit val encoderForTestAggregation = Encoders.bean(classOf[ForTestAggregation])
  implicit val encoderAggregationOdm = Encoders.bean(classOf[Aggregation])
  implicit val encoderScopeOdm = Encoders.bean(classOf[Scope])
  //  implicit val encoderScenarioOdm = Encoders.bean(classOf[Scenario])
  implicit val encoderCustomer = Encoders.bean(classOf[Customer])
  implicit val encoderCustomerWithSeuil = Encoders.bean(classOf[CustomerWithSeuil])

  implicit val encoderTransaction = Encoders.bean(classOf[CBTransaction])
  //implicit val encoderTestTransaction = Encoders.bean(classOf[TestTransaction])
  implicit val encoderTransactionType = Encoders.bean(classOf[TransactionType])
  implicit val encoderCountry = Encoders.bean(classOf[Country])
  implicit val encoderEnrichedTransaction = Encoders.bean(classOf[RawEnrichedCBTransaction])
  implicit val encoderRawEnrichedTransaction = Encoders.bean(classOf[RawEnrichedTransaction])

  implicit val encoderBusinessSegmentation = Encoders.bean(classOf[BusinessSegmentation])
  implicit val encoderGenericRowWithSchema = Encoders.kryo[GenericRowWithSchema]


  implicit val encoderXomProprieteBoolean = Encoders.kryo[xom.dynamic.ProprieteBoolean]
  implicit val encoderXomProprieteDouble = Encoders.kryo[xom.dynamic.ProprieteDouble]
  implicit val encoderXomProprieteString = Encoders.kryo[xom.dynamic.ProprieteString]
  implicit val encoderXomProprieteLong = Encoders.kryo[xom.dynamic.ProprieteLong]
  implicit val encoderXomProprieteDate = Encoders.kryo[xom.dynamic.ProprieteDate]
  implicit val encoderXomProprieteDynamique = Encoders.kryo[xom.dynamic.ProprieteDynamique]
  implicit val encodedomClasseDynamique = Encoders.kryo[xom.dynamic.ClasseDynamique]
  implicit val encoderXomAMLEntity = Encoders.kryo[xom.AMLEntity]
  implicit val encoderXomClient = Encoders.kryo[xom.Client]
  implicit val encoderXomTransaction = Encoders.kryo[xom.Transaction]
  implicit val encoderXomAlert = Encoders.kryo[xom.Alert]
  implicit val encoderXomMarche = Encoders.kryo[xom.Marche]
  implicit val encoderXomClientFlag = Encoders.kryo[xom.ClientFlag]
  implicit val encoderXomDay = Encoders.bean(classOf[xom.Day])
  implicit val encoderXomFeature = Encoders.bean(classOf[xom.Feature])
  implicit val encoderXomReport = Encoders.kryo[xom.Report]
  implicit val encoderXomTransactionFlag = Encoders.kryo[xom.TransactionFlag]


  implicit val encoderTransactionWithTransactionType = Encoders.bean(classOf[RawTransactionWithCBTransactionType])
  implicit val encoderRTransaction = Encoders.bean(classOf[RTransaction])
  implicit val encoderTransactionWithAddJoinColumns = Encoders.bean(classOf[RawCBTransactionWithAddedColumns])

  implicit val encoderTestFieldInteger = Encoders.bean(classOf[IntegerField])
  implicit val encoderTestFieldDouble = Encoders.bean(classOf[DoubleField])
  implicit val encoderTestFieldListString = Encoders.bean(classOf[ListStringField])
  implicit val encoderTestFieldListDouble = Encoders.bean(classOf[ListDoubleField])
  implicit val encoderAlert = Encoders.bean(classOf[Alert])

  implicit val encoderRow = Encoders.kryo[Row]


  //implicit val encoderEnrichedTransaction = Encoders.bean(classOf[EnrichedTransaction])
  implicit val encoderEnrichedTransactionWithAggregs = Encoders.kryo(classOf[EnrichedTransactionWithAggregs])

}
