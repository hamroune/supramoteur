Feature:  JOINTURE CB_TRANSACTION ET CUSTOMER, COUNTRY, TX_CODE

  Scenario: JOINTURE ENTRE TX ET CUSTOMER : CAS CREDIT
  #
  # Si Tx de type Credit => CUSTOME_ID = Prendre BENE_SUBCUST_SRC_UNIQUE_ID comme clé de jointure
  #

    Given  la table CB_TRANSACTIONS

      #|DATE          |ID                |TYPE DE TXN          |CLIENT ORIG                |CLIENT BENEF                |                     |                  |COMPTE ORIG         | COMPTE BENEF       |
      |SRC_TXN_UNIQUE_ID |RUN_TIME_STAMP|TXN_SOURCE_TYPE_CODE |ORIG_SUBCUST_SRC_UNIQUE_ID |BENE_SUBCUST_SRC_UNIQUE_ID  |ORIG_COUNTRY_BANQUE  |BENE_COUNTRY_CODE |ORIG_ACCOUNT_NUMBER |BENE_ACCOUNT_NUMBER |
      |1                 |123               |0056             |200                        |100                         |        FR           |      FR          |                    |                    |

    And la table TRANSACTION_TYPE

      #| CODE UNIQUE |            |                         |       |INSTRUMENT|C= Crédit, D=Débit|
      |TXN_TYPE_CODE|TXN_TYPE_DESC|TXN_TYPE_NAME            |SCOPE  |INSTRUMENT|CREDIT_DEBIT_CODE|
      |0056         |ECHEANCE PRET|AMORTISSEMENT DE PRET    |       |REPAY     |C                |
      |0083         |FACTURE CARTE|REGLEMENT FACTURES CARTES|       |CARD      |D                |

    And la table CUSTOMER
      #|ID UNIQUE              |FIRST_NAME|Segment KyC       |Segment AML|
      |CUSTOMER_SOURCE_UNIQUE_I|FIRST_NAME|CUSTOMER_SEGMENT_1|CUSTOMER_SEGMENT_2|
      |0001038000060203        |DUPONT    |PB                |6                 |
      |0021023000020107        |JEAN      |MM                |7                 |

    And la table Country
      |COUNTRY_CODE|COUNTRY_NAME                |TAX_HAVEN_FLAG|HIGHRISK_FLAG|EUROPE_FLAG|
      |FR          |REPUBLIQUE FRANCAISE        |N             |0            |Y          |
      |IR          |REPUBLIQUE ISLAMIQUE IRAN   |N             |3            |N          |
      |CH          |CONFEDERATION SUISSE        |Y             |0            |Y          |
      |BM          |CONFEDERATION SUISSE        |Y             |2            |N          |

    When on enrichit la table CB_TRANSACTIONS

    Then la table enrichie est la suivante
    # comme la Tx est de Type Crédit (R) donc le client c'est le BENEF_SUBCUST_SRC_UNIQUE_ID qu'il faut prendre
      |SRC_TXN_UNIQUE_ID |RUN_TIME_STAMP|TXN_SOURCE_TYPE_CODE |ORIG_SUBCUST_SRC_UNIQUE_ID |BENE_SUBCUST_SRC_UNIQUE_ID  |ORIG_COUNTRY_BANQUE  |BENE_COUNTRY_CODE |ORIG_ACCOUNT_NUMBER |BENE_ACCOUNT_NUMBER |TXN_TYPE_DESC|TXN_TYPE_NAME            |SCOPE  |INSTRUMENT|CREDIT_DEBIT_CODE|CUSTOMER_ID      |FIRST_NAME|CUSTOMER_SEGMENT_1|CUSTOMER_SEGMENT_2|COUNTRY_NAME                |TAX_HAVEN_FLAG|HIGHRISK_FLAG|EUROPE_FLAG|
      |1                 |123           |0056                 |0021023000020107           |0001038000060203            |        FR           |      FR          |                    |                    |ECHEANCE PRET|AMORTISSEMENT DE PRET    |       |REPAY     |C                |0001038000060203 |DUPONT    |PB                |6                 |REPUBLIQUE FRANCAISE        |N             |0            |Y          |


  Scenario: JOINTURE ENTRE TX ET CUSTOMER : CAS DEBIT
  #
  # Si Tx de type Debit => CUSTOME_ID = Prendre ORIG_SUBCUST_SRC_UNIQUE_ID comme clé de jointure
  #

    Given  la table CB_TRANSACTIONS

      #|DATE          |ID                |TYPE DE TXN          |CLIENT ORIG                |CLIENT BENEF                |                     |                  |COMPTE ORIG         | COMPTE BENEF       |
      |RUN_TIME_STAMP |SRC_TXN_UNIQUE_ID |TXN_SOURCE_TYPE_CODE |ORIG_SUBCUST_SRC_UNIQUE_ID |BENE_SUBCUST_SRC_UNIQUE_ID  |ORIG_COUNTRY_BANQUE  |BENE_COUNTRY_CODE |ORIG_ACCOUNT_NUMBER |BENE_ACCOUNT_NUMBER |
      |1              |123               |0083                 |200                        |100                         |        FR           |      FR          |                    |                    |

    And la table TRANSACTION_TYPE

      #| CODE UNIQUE |            |                         |       |INSTRUMENT|C= Crédit, D=Débit|
      |TXN_TYPE_CODE|TXN_TYPE_DESC|TXN_TYPE_NAME            |SCOPE  |INSTRUMENT|CREDIT_DEBIT_CODE|
      |0056         |ECHEANCE PRET|AMORTISSEMENT DE PRET    |       |REPAY     |C                |
      |0083         |FACTURE CARTE|REGLEMENT FACTURES CARTES|       |CARD      |D                |

    And la table CUSTOMER
      #|ID UNIQUE              |FIRST_NAME|Segment KyC       |Segment AML|
      |CUSTOMER_SOURCE_UNIQUE_I|FIRST_NAME|CUSTOMER_SEGMENT_1|CUSTOMER_SEGMENT_2|
      |100                     |DUPONT    |PB                |6                 |
      |200                     |JEAN      |MM                |7                 |

    And la table Country
      |COUNTRY_CODE|COUNTRY_NAME                |TAX_HAVEN_FLAG|HIGHRISK_FLAG|EUROPE_FLAG|
      |FR          |REPUBLIQUE FRANCAISE        |N             |0            |Y          |
      |IR          |REPUBLIQUE ISLAMIQUE IRAN   |N             |3            |N          |
      |CH          |CONFEDERATION SUISSE        |Y             |0            |Y          |
      |BM          |CONFEDERATION SUISSE        |Y             |2            |N          |

    When on enrichit la table CB_TRANSACTIONS

    Then la table enrichie est la suivante
    # comme la Tx est de Type Crédit (R) donc le client c'est le BENEF_SUBCUST_SRC_UNIQUE_ID qu'il faut prendre
|RUN_TIME_STAMP |SRC_TXN_UNIQUE_ID |TXN_SOURCE_TYPE_CODE |ORIG_SUBCUST_SRC_UNIQUE_ID |BENE_SUBCUST_SRC_UNIQUE_ID  |ORIG_COUNTRY_BANQUE  |BENE_COUNTRY_CODE |ORIG_ACCOUNT_NUMBER |BENE_ACCOUNT_NUMBER |TXN_TYPE_DESC|TXN_TYPE_NAME            |SCOPE  |INSTRUMENT|CREDIT_DEBIT_CODE|CUSTOMER_ID       |FIRST_NAME|CUSTOMER_SEGMENT_1|CUSTOMER_SEGMENT_2|COUNTRY_NAME                |TAX_HAVEN_FLAG|HIGHRISK_FLAG|EUROPE_FLAG|
|1              |123               |0083                 |0021023000020107           |0001038000060203            |        FR           |      FR          |                    |                    |FACTURE CARTE|REGLEMENT FACTURES CARTES|       |CARD      |D                |200               |JEAN      |MM                |7                 |REPUBLIQUE FRANCAISE        |N             |0            |Y          |
