Feature: AML Scenarii
  
  Scenario: Scenario DS_11_A_1 :: accumulated_amount_current_1_weeks_to_HR_VHR, accumulated_amount_previous_1_weeks_to_HR_VHR, number_tx_current_1_weeks_to_HR_VHR

    Given  les transactions suivantes
      # transform this data table to Transactions objectsomme_montant_SEPA_TO_HR_semaine_actuelle
      |client   | transactionId             | transactionDate:Date   | amount          | week  | month | transactionType | Direction | Contre_partie |  country_risk  |
      |1        | A                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP1           | HR            |
      |1        | B                         | 2020-11-20             | 150              | 0     | 0     | SEPA            | TO        | CP1           | HR            |
      |1        | C                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | FROM      | CP1           | HR            |
      |1        | D                         | 2020-11-20             | 100              | 0     | 0     | DOMESTIC        | FROM      | CP2           | HR            |
      |1        | E                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP2           | HR            |
      |1        | F                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP3           | VHR           |
      |1        | G                         | 2020-11-20             | 10               | 0     | 0     | CROSSBOARDER    | TO        | CP4           | VHR           |
      |1        | H                         | 2020-11-20             | 20               | 0     | 0     | SEPA            | TO        | CP4           | VHR           |
      |1        | I                         | 2020-11-20             | 50               | 0     | 0     | CROSSBOARDER    | TO        | CP4           | VHR           |
      |1        | J                         | 2020-11-20             | 50               | 0     | 0     | SEPA            | TO        | CP4           | NORMAL        |
      |1        | K                         | 2020-11-20             | 120              | 1     | 0     | CROSSBOARDER    | TO        | CP1           | HR            |
      |1        | L                         | 2020-11-20             | 130              | 1     | 0     | SEPA            | TO        | CP1           | HR            |
      |1        | M                         | 2020-11-20             | 200              | 1     | 0     | SEPA            | FROM      | CP1           | HR            |
      |1        | N                         | 2020-11-20             | 210              | 1     | 0     | CROSSBOARDER    | FROM      | CP2           | HR            |
      |1        | O                         | 2020-11-20             | 120              | 1     | 0     | SEPA            | TO        | CP2           | HR            |
      |1        | P                         | 2020-11-20             | 100              | 1     | 0     | SEPA            | TO        | CP3           | VHR           |
      |1        | Q                         | 2020-11-20             | 100              | 1     | 0     | CROSSBOARDER    | TO        | CP4           | VHR           |
      |1        | R                         | 2020-11-20             | 200              | 1     | 0     | CROSSBOARDER    | TO        | CP4           | VHR           |
      |1        | S                         | 2020-11-20             | 250              | 1     | 0     | SEPA            | TO        | CP4           | VHR           |
      |1        | T                         | 2020-11-20             | 150              | 1     | 0     | DOMESTIC        | TO        | CP4           | NORMAL        |
      |1        | U                         | 2020-11-20             | 150              | 1     | 0     | DOMESTIC        | TO        | CP4           | NORMAL        |
      |1        | V                         | 2020-11-20             | 150              | 1     | 0     | DOMESTIC        | TO        | CP4           | NORMAL        |
      |2        | W                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP1           | HR            |
      |2        | X                         | 2020-11-20             | 150              | 0     | 0     | SEPA            | TO        | CP1           | HR            |
      |2        | Y                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | FROM      | CP1           | HR            |
      |2        | Z                         | 2020-11-20             | 100              | 0     | 0     | DOMESTIC        | FROM      | CP2           | HR            |
      |2        | Z1                        | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP2           | HR            |
      |2        | Z2                        | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP3           | VHR           |
      |2        | Z3                        | 2020-11-20             | 100              | 0     | 0     | CROSSBOARDER    | TO        | CP4           | VHR           |
      |2        | Z4                        | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP4           | VHR           |
      |2        | Z5                        | 2020-11-20             | 100              | 0     | 0     | CROSSBOARDER    | TO        | CP4           | VHR           |
      |2        | Z6                        | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP4           | NORMAL        |

    And maille choisie
      | partition |
      | client    |

    And Sort Transaction By
      | orderBy |
      | transactionDate    |

    And global scope
      | scope |
      |  transactionType in ('SEPA', 'CROSSBOARDER') and Direction = 'TO' and  country_risk in ('HR', 'VHR')   |

    And aggregation List
     |aggFunction |aggPartitionBy |orderBy         |aggTimePeriod |aggTimePeriodSize  |aggTimePeriodSelected |aggMetric              |aggColName                                    |aggregationScopes                                    |
     |sum         |               |                |week          |1                  |0                     |amount                 |accumulated_amount_current_1_weeks_to_HR_VHR  |  |
     |sum         |               |                |week          |1                  |1                     |amount                 |accumulated_amount_previous_1_weeks_to_HR_VHR |  |
     #|count       |client         |                |week          |1                  |0                     |transactionId          |number_tx_current_1_weeks_to_HR_VHR           |  |
    # |avg         |client         |transactionDate |week          |1                  |0                     |amount                 |avg_amount_current_1_weeks_to_HR_VHR  |  |
    # |min         |client      |transactionDate |week          |1                  |1                     |amount                 |min_amount_previous_1_weeks_to_HR_VHR |   |
    # |max         |client      |transactionDate |week          |1                  |1                     |amount                 |max_amount_previous_1_weeks_to_HR_VHR |   |

    When j'applique le calcul d'aggregation

    Then le resultat de l'aggregation est le suivant

      |client|transactionId|transactionDate:Date|amount:Double|week|month|transactionType|Direction|Contre_partie|country_risk|accumulated_amount_current_1_weeks_to_HR_VHR:Double|accumulated_amount_previous_1_weeks_to_HR_VHR:Double|
  |     1|            A|     2020-11-20|   100|   0|    0|           SEPA|       TO|          CP1|          HR|                                       530.0|                                       1020.0|
  |     1|            B|     2020-11-20|   150|   0|    0|           SEPA|       TO|          CP1|          HR|                                       530.0|                                       1020.0|
  |     1|            E|     2020-11-20|   100|   0|    0|           SEPA|       TO|          CP2|          HR|                                       530.0|                                       1020.0|
  |     1|            F|     2020-11-20|   100|   0|    0|           SEPA|       TO|          CP3|         VHR|                                       530.0|                                       1020.0|
  |     1|            G|     2020-11-20|    10|   0|    0|   CROSSBOARDER|       TO|          CP4|         VHR|                                       530.0|                                       1020.0|
  |     1|            H|     2020-11-20|    20|   0|    0|           SEPA|       TO|          CP4|         VHR|                                       530.0|                                       1020.0|
  |     1|            I|     2020-11-20|    50|   0|    0|   CROSSBOARDER|       TO|          CP4|         VHR|                                       530.0|                                       1020.0|
  |     1|            K|     2020-11-20|   120|   1|    0|   CROSSBOARDER|       TO|          CP1|          HR|                                       530.0|                                       1020.0|
  |     1|            L|     2020-11-20|   130|   1|    0|           SEPA|       TO|          CP1|          HR|                                       530.0|                                       1020.0|
  |     1|            O|     2020-11-20|   120|   1|    0|           SEPA|       TO|          CP2|          HR|                                       530.0|                                       1020.0|
  |     1|            P|     2020-11-20|   100|   1|    0|           SEPA|       TO|          CP3|         VHR|                                       530.0|                                       1020.0|
  |     1|            Q|     2020-11-20|   100|   1|    0|   CROSSBOARDER|       TO|          CP4|         VHR|                                       530.0|                                       1020.0|
  |     1|            R|     2020-11-20|   200|   1|    0|   CROSSBOARDER|       TO|          CP4|         VHR|                                       530.0|                                       1020.0|
  |     1|            S|     2020-11-20|   250|   1|    0|           SEPA|       TO|          CP4|         VHR|                                       530.0|                                       1020.0|
  |     2|            W|     2020-11-20|   100|   0|    0|           SEPA|       TO|          CP1|          HR|                                       750.0|                                         null|
  |     2|            X|     2020-11-20|   150|   0|    0|           SEPA|       TO|          CP1|          HR|                                       750.0|                                         null|
  |     2|           Z1|     2020-11-20|   100|   0|    0|           SEPA|       TO|          CP2|          HR|                                       750.0|                                         null|
  |     2|           Z2|     2020-11-20|   100|   0|    0|           SEPA|       TO|          CP3|         VHR|                                       750.0|                                         null|
  |     2|           Z3|     2020-11-20|   100|   0|    0|   CROSSBOARDER|       TO|          CP4|         VHR|                                       750.0|                                         null|
  |     2|           Z4|     2020-11-20|   100|   0|    0|           SEPA|       TO|          CP4|         VHR|                                       750.0|                                         null|
  |     2|           Z5| 2020-11-20    | 100  | 0  | 0   | CROSSBOARDER  | TO      | CP4         |         VHR|                                       750.0|                                         null|
  Scenario: Scenario DS_26_B_1 :: Detection of an excessive activity from the same counterparty :: Number_VI_SEPA_DOMESTIC_sortant_par_CP_VHR_HR, CUMULE_montant_VI_SEPA_DOMESTIC_sortant_par_CP_VHR_HR,


    Given  les transactions suivantes
      # transform this data table to Transactions objectsomme_montant_SEPA_TO_HR_semaine_actuelle
      |client   | transactionId             | transactionDate:Date   | amount          | week  | month | transactionType | Direction | Contre_partie |  country_risk  |
      |1        | A                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP1           | HR            |
      |1        | B                         | 2020-11-20             | 150              | 0     | 0     | SEPA            | TO        | CP1           | HR            |
      |1        | C                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | FROM      | CP1           | HR            |
      |1        | D                         | 2020-11-20             | 100              | 0     | 0     | DOMESTIC        | FROM      | CP2           | HR            |
      |1        | E                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP2           | HR            |
      |1        | F                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP3           | VHR           |
      |1        | G                         | 2020-11-20             | 10               | 0     | 0     | CROSSBOARDER    | TO        | CP4           | VHR           |
      |1        | H                         | 2020-11-20             | 20               | 0     | 0     | SEPA            | TO        | CP4           | VHR           |
      |1        | I                         | 2020-11-20             | 50               | 0     | 0     | CROSSBOARDER    | TO        | CP4           | VHR           |
      |1        | J                         | 2020-11-20             | 50               | 0     | 0     | SEPA            | TO        | CP4           | NORMAL        |
      |1        | K                         | 2020-11-20             | 120              | 1     | 0     | CROSSBOARDER    | TO        | CP1           | HR            |
      |1        | L                         | 2020-11-20             | 130              | 1     | 0     | SEPA            | TO        | CP1           | HR            |
      |1        | M                         | 2020-11-20             | 200              | 1     | 0     | SEPA            | FROM      | CP1           | HR            |
      |1        | N                         | 2020-11-20             | 210              | 1     | 0     | CROSSBOARDER    | FROM      | CP2           | HR            |
      |1        | O                         | 2020-11-20             | 120              | 1     | 0     | SEPA            | TO        | CP2           | HR            |
      |1        | P                         | 2020-11-20             | 100              | 1     | 0     | SEPA            | TO        | CP3           | VHR           |
      |1        | Q                         | 2020-11-20             | 100              | 1     | 0     | CROSSBOARDER    | TO        | CP4           | VHR           |
      |1        | R                         | 2020-11-20             | 200              | 1     | 0     | CROSSBOARDER    | TO        | CP4           | VHR           |
      |1        | S                         | 2020-11-20             | 250              | 1     | 0     | SEPA            | TO        | CP4           | VHR           |
      |1        | T                         | 2020-11-20             | 150              | 1     | 0     | DOMESTIC        | TO        | CP4           | NORMAL        |
      |1        | U                         | 2020-11-20             | 150              | 1     | 0     | DOMESTIC        | TO        | CP4           | NORMAL        |
      |1        | V                         | 2020-11-20             | 150              | 1     | 0     | DOMESTIC        | TO        | CP4           | NORMAL        |
      |2        | W                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP1           | HR            |
      |2        | X                         | 2020-11-20             | 150              | 0     | 0     | SEPA            | TO        | CP1           | HR            |
      |2        | Y                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | FROM      | CP1           | HR            |
      |2        | Z                         | 2020-11-20             | 100              | 0     | 0     | DOMESTIC        | FROM      | CP2           | HR            |
      |2        | Z1                        | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP2           | HR            |
      |2        | Z2                        | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP3           | VHR           |
      |2        | Z3                        | 2020-11-20             | 100              | 0     | 0     | CROSSBOARDER    | TO        | CP4           | VHR           |
      |2        | Z4                        | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP4           | VHR           |
      |2        | Z5                        | 2020-11-20             | 100              | 0     | 0     | CROSSBOARDER    | TO        | CP4           | VHR           |
      |2        | Z6                        | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP4           | NORMAL        |

    And maille choisie
      | partition |
      | client    |

    And Sort Transaction By
      | orderBy |
      | transactionDate    |

    And global scope
      | scope |
      |  transactionType in ('SEPA', 'CROSSBOARDER') and Direction = 'TO' and  country_risk in ('HR', 'VHR')   |

    And aggregation List
      |aggFunction |aggPartitionBy |orderBy         |aggTimePeriod |aggTimePeriodSize  |aggTimePeriodSelected |aggMetric              |aggColName                                    |aggregationScopes                                    |
      |sum         |Contre_partie  |                |week          |1                  |0                     |amount                 |accumulated_amount_current_1_weeks_to_HR_VHR  |  |
      |sum         |Contre_partie  |                |week          |1                  |1                     |amount                 |accumulated_amount_previous_1_weeks_to_HR_VHR |  |
      |count       |Contre_partie  |                |week          |1                  |0                     |transactionId          |number_tx_current_1_weeks_to_HR_VHR           |  |


        # |avg         |client      |transactionDate |week          |1                  |0                     |amount                 |avg_amount_current_1_weeks_to_HR_VHR  |  |
    # |min         |client      |transactionDate |week          |1                  |1                     |amount                 |min_amount_previous_1_weeks_to_HR_VHR |   |
    # |max         |client      |transactionDate |week          |1                  |1                     |amount                 |max_amount_previous_1_weeks_to_HR_VHR |   |

    When j'applique le calcul d'aggregation

    Then le resultat de l'aggregation est le suivant

      |client|transactionId|transactionDate|amount|week|month|transactionType|Direction|Contre_partie|country_risk|accumulated_amount_current_1_weeks_to_HR_VHR:Double|accumulated_amount_previous_1_weeks_to_HR_VHR:Double|number_tx_current_1_weeks_to_HR_VHR:Double|
      |     1|            A|     2020-11-20|   100|   0|    0|           SEPA|       TO|          CP1|          HR|                                              250.0|                                              250.0|                                   2.0|
      |     1|            B|     2020-11-20|   150|   0|    0|           SEPA|       TO|          CP1|          HR|                                              250.0|                                              250.0|                                   2.0|
  #|     1|            C|     2020-11-20|   100|   0|    0|           SEPA|     FROM|          CP1|          HR|                                                  530.0|                                             1020.0|                                   7.0|
      |     1|            E|     2020-11-20|   100|   0|    0|           SEPA|       TO|          CP2|          HR|                                              100.0|                                              120.0|                                   1.0|
      |     1|            F|     2020-11-20|   100|   0|    0|           SEPA|       TO|          CP3|         VHR|                                              100.0|                                              100.0|                                   1.0|
      |     1|            G|     2020-11-20|    10|   0|    0|   CROSSBOARDER|       TO|          CP4|         VHR|                                               80.0|                                              550.0|                                   3.0|
      |     1|            H|     2020-11-20|    20|   0|    0|           SEPA|       TO|          CP4|         VHR|                                               80.0|                                              550.0|                                   3.0|
      |     1|            I|     2020-11-20|    50|   0|    0|   CROSSBOARDER|       TO|          CP4|         VHR|                                               80.0|                                              550.0|                                   3.0|
  #|     1|            J|     2020-11-20|    50|   0|    0|           SEPA|       TO|          CP4|      NORMAL|                                                 530.0|                                        1020.0|                                        7|
      |     1|            K|     2020-11-20|   120|   1|    0|   CROSSBOARDER|       TO|          CP1|          HR|                                              250.0|                                              250.0|                                   2.0|
      |     1|            L|     2020-11-20|   130|   1|    0|           SEPA|       TO|          CP1|          HR|                                              250.0|                                              250.0|                                   2.0|
  #|     1|            M|     2020-11-20|   200|   1|    0|           SEPA|     FROM|          CP1|          HR|                                                 530.0|                                            1020.0|                                      7|
  #|     1|            N|     2020-11-20|   210|   1|    0|   CROSSBOARDER|     FROM|          CP2|          HR|                                                 530.0|                                            1020.0|                                      7|
      |     1|            O|     2020-11-20|   120|   1|    0|           SEPA|       TO|          CP2|          HR|                                              100.0|                                              120.0|                                   1.0|
      |     1|            P|     2020-11-20|   100|   1|    0|           SEPA|       TO|          CP3|         VHR|                                              100.0|                                              100.0|                                   1.0|
      |     1|            Q|     2020-11-20|   100|   1|    0|   CROSSBOARDER|       TO|          CP4|         VHR|                                               80.0|                                              550.0|                                   3.0|
      |     1|            R|     2020-11-20|   200|   1|    0|   CROSSBOARDER|       TO|          CP4|         VHR|                                               80.0|                                              550.0|                                   3.0|
      |     1|            S|     2020-11-20|   250|   1|    0|           SEPA|       TO|          CP4|         VHR|                                               80.0|                                              550.0|                                   3.0|
      |     2|            W|     2020-11-20|   100|   0|    0|           SEPA|       TO|          CP1|          HR|                                              250.0|                                               null|                                   2.0|
      |     2|            X|     2020-11-20|   150|   0|    0|           SEPA|       TO|          CP1|          HR|                                              250.0|                                               null|                                   2.0|
  #|     2|            Y|     2020-11-20|   100|   0|    0|           SEPA|     FROM|          CP1|          HR|                                                 750.0|                                               0.0|                                      7|
      |     2|           Z1|     2020-11-20|   100|   0|    0|           SEPA|       TO|          CP2|          HR|                                              100.0|                                               null|                                   1.0|
      |     2|           Z2|     2020-11-20|   100|   0|    0|           SEPA|       TO|          CP3|         VHR|                                              100.0|                                               null|                                   1.0|
      |     2|           Z3|     2020-11-20|   100|   0|    0|   CROSSBOARDER|       TO|          CP4|         VHR|                                              300.0|                                               null|                                   3.0|
      |     2|           Z4|     2020-11-20|   100|   0|    0|           SEPA|       TO|          CP4|         VHR|                                              300.0|                                               null|                                   3.0|
      |     2|           Z5|     2020-11-20|   100|   0|    0|   CROSSBOARDER|       TO|          CP4|         VHR|                                              300.0|                                               null|                                   3.0|


  Scenario: Scenario DS_11_B_1_a :: counter_party_list_last_6_months

    Given  les transactions suivantes
      # transform this data table to Transactions objectsomme_montant_SEPA_TO_HR_semaine_actuelle
      |client   | transactionId             | transactionDate:Date   | amount          | week  | month | transactionType | Direction | Contre_partie |  country_risk  |
      |1        | A                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP1           | HR            |
      |1        | B                         | 2020-11-20             | 150              | 0     | 0     | SEPA            | TO        | CP1           | HR            |
      |1        | C                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | FROM      | CP1           | HR            |
      |1        | D                         | 2020-11-20             | 100              | 0     | 0     | DOMESTIC        | FROM      | CP2           | HR            |
      |1        | E                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP2           | HR            |
      |1        | F                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP3           | VHR           |
      |1        | G                         | 2020-11-20             | 10               | 0     | 0     | CROSSBOARDER    | TO        | CP4           | VHR           |
      |1        | H                         | 2020-11-20             | 20               | 0     | 0     | SEPA            | TO        | CP4           | VHR           |
      |1        | I                         | 2020-11-20             | 50               | 0     | 0     | CROSSBOARDER    | TO        | CP4           | VHR           |
      |1        | J                         | 2020-11-20             | 50               | 0     | 0     | SEPA            | TO        | CP4           | NORMAL        |
      |1        | K                         | 2020-11-20             | 120              | 1     | 0     | CROSSBOARDER    | TO        | CP1           | HR            |
      |1        | L                         | 2020-11-20             | 130              | 1     | 0     | SEPA            | TO        | CP1           | HR            |
      |1        | M                         | 2020-11-20             | 200              | 1     | 0     | SEPA            | FROM      | CP1           | HR            |
      |1        | N                         | 2020-11-20             | 210              | 1     | 0     | CROSSBOARDER    | FROM      | CP2           | HR            |
      |1        | O                         | 2020-11-20             | 120              | 1     | 0     | SEPA            | TO        | CP2           | HR            |
      |1        | P                         | 2020-11-20             | 100              | 1     | 0     | SEPA            | TO        | CP3           | VHR           |
      |1        | Q                         | 2020-11-20             | 100              | 1     | 0     | CROSSBOARDER    | TO        | CP4           | VHR           |
      |1        | R                         | 2020-11-20             | 200              | 1     | 0     | CROSSBOARDER    | TO        | CP4           | VHR           |
      |1        | S                         | 2020-11-20             | 250              | 1     | 0     | SEPA            | TO        | CP4           | VHR           |
      |1        | T                         | 2020-11-20             | 150              | 1     | 0     | DOMESTIC        | TO        | CP4           | NORMAL        |
      |1        | U                         | 2020-11-20             | 150              | 1     | 0     | DOMESTIC        | TO        | CP4           | NORMAL        |
      |1        | V                         | 2020-11-20             | 150              | 1     | 0     | DOMESTIC        | TO        | CP4           | NORMAL        |
      |2        | W                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP1           | HR            |
      |2        | X                         | 2020-11-20             | 150              | 0     | 0     | SEPA            | TO        | CP1           | HR            |
      |2        | Y                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | FROM      | CP1           | HR            |
      |2        | Z                         | 2020-11-20             | 100              | 0     | 0     | DOMESTIC        | FROM      | CP2           | HR            |
      |2        | Z1                        | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP2           | HR            |
      |2        | Z2                        | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP3           | VHR           |
      |2        | Z3                        | 2020-11-20             | 100              | 0     | 0     | CROSSBOARDER    | TO        | CP4           | VHR           |
      |2        | Z4                        | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP4           | VHR           |
      |2        | Z5                        | 2020-11-20             | 100              | 0     | 0     | CROSSBOARDER    | TO        | CP4           | VHR           |
      |2        | Z6                        | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP4           | NORMAL        |

    And maille choisie
      | partition |
      | client    |

    And Sort Transaction By
      | orderBy |
      | transactionDate    |

    And global scope
      | scope |
      |  transactionType in ('SEPA', 'CROSSBOARDER') and Direction = 'TO' and  country_risk in ('HR', 'VHR')   |

    And aggregation List
      |aggFunction |aggPartitionBy |orderBy         |aggTimePeriod |aggTimePeriodSize  |aggTimePeriodSelected |aggMetric              |aggColName                                    |aggregationScopes                                    |
      |collect_set |               |                |week          |1                  |0                     |amount                 |counter_party_list_last_6_months              |  |
     #|count       |client         |                |week          |1                  |0                     |transactionId          |number_tx_current_1_weeks_to_HR_VHR           |  |
    # |avg         |client         |transactionDate |week          |1                  |0                     |amount                 |avg_amount_current_1_weeks_to_HR_VHR  |  |
    # |min         |client      |transactionDate |week          |1                  |1                     |amount                 |min_amount_previous_1_weeks_to_HR_VHR |   |
    # |max         |client      |transactionDate |week          |1                  |1                     |amount                 |max_amount_previous_1_weeks_to_HR_VHR |   |

    When j'applique le calcul d'aggregation

    Then le resultat de l'aggregation est le suivant

      |client|transactionId|transactionDate:Date|amount:Double|week|month|transactionType|Direction|Contre_partie|country_risk|counter_party_list_last_6_months:List[Double]|
      |     1|            A|     2020-11-20|   100|   0|    0|           SEPA|       TO|          CP1|          HR|                  [100,150,100,100,10,20,50]|
      |     1|            B|     2020-11-20|   150|   0|    0|           SEPA|       TO|          CP1|          HR|                  [100,150,100,100,10,20,50]|
      |     1|            E|     2020-11-20|   100|   0|    0|           SEPA|       TO|          CP2|          HR|                  [100,150,100,100,10,20,50]|
      |     1|            F|     2020-11-20|   100|   0|    0|           SEPA|       TO|          CP3|         VHR|                  [100,150,100,100,10,20,50]|
      |     1|            G|     2020-11-20|    10|   0|    0|   CROSSBOARDER|       TO|          CP4|         VHR|                  [100,150,100,100,10,20,50]|
      |     1|            H|     2020-11-20|    20|   0|    0|           SEPA|       TO|          CP4|         VHR|                  [100,150,100,100,10,20,50]|
      |     1|            I|     2020-11-20|    50|   0|    0|   CROSSBOARDER|       TO|          CP4|         VHR|                  [100,150,100,100,10,20,50]|
      |     1|            K|     2020-11-20|   120|   1|    0|   CROSSBOARDER|       TO|          CP1|          HR|                  [100,150,100,100,10,20,50]|
      |     1|            L|     2020-11-20|   130|   1|    0|           SEPA|       TO|          CP1|          HR|                  [100,150,100,100,10,20,50]|
      |     1|            O|     2020-11-20|   120|   1|    0|           SEPA|       TO|          CP2|          HR|                  [100,150,100,100,10,20,50]|
      |     1|            P|     2020-11-20|   100|   1|    0|           SEPA|       TO|          CP3|         VHR|                  [100,150,100,100,10,20,50]|
      |     1|            Q|     2020-11-20|   100|   1|    0|   CROSSBOARDER|       TO|          CP4|         VHR|                  [100,150,100,100,10,20,50]|
      |     1|            R|     2020-11-20|   200|   1|    0|   CROSSBOARDER|       TO|          CP4|         VHR|                  [100,150,100,100,10,20,50]|
      |     1|            S|     2020-11-20|   250|   1|    0|           SEPA|       TO|          CP4|         VHR|                  [100,150,100,100,10,20,50]|
      |     2|            W|     2020-11-20|   100|   0|    0|           SEPA|       TO|          CP1|          HR|                   [100,150,100,100,100,100]|
      |     2|            X|     2020-11-20|   150|   0|    0|           SEPA|       TO|          CP1|          HR|                   [100,150,100,100,100,100]|
      |     2|           Z1|     2020-11-20|   100|   0|    0|           SEPA|       TO|          CP2|          HR|                   [100,150,100,100,100,100]|
      |     2|           Z2|     2020-11-20|   100|   0|    0|           SEPA|       TO|          CP3|         VHR|                   [100,150,100,100,100,100]|
      |     2|           Z3|     2020-11-20|   100|   0|    0|   CROSSBOARDER|       TO|          CP4|         VHR|                   [100,150,100,100,100,100]|
      |     2|           Z4|     2020-11-20|   100|   0|    0|           SEPA|       TO|          CP4|         VHR|                   [100,150,100,100,100,100]|
      |     2|           Z5| 2020-11-20    | 100  | 0  | 0   | CROSSBOARDER  | TO      | CP4         |         VHR|                   [100,150,100,100,100,100]|
