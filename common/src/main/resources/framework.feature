Feature: AML Aggregation Framework
  
  Scenario: Sum :: Current Week  :: ALL SCOPE

    Given  les transactions suivantes
      # transform this data table to Transactions objectsomme_montant_SEPA_TO_HR_semaine_actuelle
      |compte   | transactionId             | transactionDate:Date   | montant         | week  | month | transactionType |
      |1        | A                         | 2020-11-20             | 100             | 0     | 0     | cheque          |
      |1        | B                         | 2020-11-20             | 150             | 0     | 0     | VI              |
      |1        | C                         | 2020-11-20             | 100             | 1     | 0     | cheque          |
      |1        | D                         | 2020-11-20             | 100             | 2     | 0     | VI              |
      |2        | E                         | 2020-11-20             | 100             | 0     | 0     | VI              |
      |2        | F                         | 2020-11-20             | 100             | 0     | 0     | VI              |
      |2        | G                         | 2020-11-20             | 100             | 0     | 0     | cheque          |
      |2        | H                         | 2020-11-20             | 100             | 4     | 0     | cheque          |

    And maille choisie
      | partition |
      | compte    |


    And global scope
      | scope |
      |       |

    And aggregation List
      # transform this data table to List of Aggregation
      # aggregationFunction = Max, Min, Sum, Count, Avg ...
      # aggPartitionBy : should be client/account
      # aggregation Partition By : select subset dataframe to apply (compute) the Agg
      # aggregartion Order By used in case of aggregation is sensitive to the order (Max, Min etc...)
        # it can be a list of dimensions
      # aggTimePeriodSize The Lenght of Period (period of 12 Weeks, period of 3 Months etc...)
      # aggTimePeriodSelected corresponds to Period
          # 0-> N c-a-d LAST PERIOD
          # 1-> N-1 ==> Previous PERIOD
          # 2-> N-2
     # aggPartitionDimension if we want to focus on certain dimensions (it can be a List)

     # Aggragation out put name: the name of the aggregation name
      |aggFunction |aggPartitionBy |orderBy         |aggTimePeriod |aggTimePeriodSize  |aggTimePeriodSelected |aggMetric |aggColName                          |aggregationScopes|
      |sum         |            |transactionDate |week          |1                  |0                     |montant   |sum_transactionId_over_current_week |                 |


    When j'applique le calcul d'aggregation

    Then le resultat de l'aggregation est le suivant

      |compte|transactionId|transactionDate|montant|week|month|transactionType|sum_transactionId_over_current_week:Double    |
      |     1|            A|     2020-11-20|    100|   0|    0|         cheque|                                    250|
      |     1|            B|     2020-11-20|    150|   0|    0|             VI|                                    250|
      |     1|            C|     2020-11-20|    100|   1|    0|         cheque|                                    250|
      |     1|            D|     2020-11-20|    100|   2|    0|             VI|                                    250|
      |     2|            E|     2020-11-20|    100|   0|    0|             VI|                                    300|
      |     2|            F|     2020-11-20|    100|   0|    0|             VI|                                    300|
      |     2|            G|     2020-11-20|    100|   0|    0|         cheque|                                    300|
      |     2|            H|     2020-11-20|    100|   4|    0|         cheque|                                    300|

  Scenario: Sum :: Current Week  :: GLOBAL SCOPE

    Given  les transactions suivantes
      # transform this data table to Transactions object
      |compte   | transactionId             | transactionDate:Date   | montant  | week  | month | transactionType |
      |1        | A                         | 2020-11-20             | 100      | 0     | 0     | cheque          |
      |1        | B                         | 2020-11-20             | 150      | 0     | 0     | VI              |
      |1        | C                         | 2020-11-20             | 100      | 1     | 0     | cheque          |
      |1        | D                         | 2020-11-20             | 100      | 2     | 0     | VI              |
      |2        | E                         | 2020-11-20             | 100      | 0     | 0     | VI              |
      |2        | F                         | 2020-11-20             | 100      | 0     | 0     | VI              |
      |2        | G                         | 2020-11-20             | 100      | 0     | 0     | cheque          |
      |2        | H                         | 2020-11-20             | 100      | 4     | 0     | cheque          |

    And maille choisie
      | partition |
      | compte    |

    And global scope
    | scope                                                 |
    | transactionType = "cheque" or transactionType = "VI"  |

    And aggregation List
      # transform this data table to List of Aggregation
      # aggregationFunction = Max, Min, Sum, Count, Avg ...
      # aggPartitionBy : should be client/account
      # aggregation Partition By : select subset dataframe to apply (compute) the Agg
      # aggregartion Order By used in case of aggregation is sensitive to the order (Max, Min etc...)
        # it can be a list of dimensions
      # aggTimePeriodSize The Lenght of Period (period of 12 Weeks, period of 3 Months etc...)
      # aggTimePeriodSelected corresponds to Period
          # 0-> N c-a-d LAST PERIOD
          # 1-> N-1 ==> Previous PERIOD
          # 2-> N-2
     # aggPartitionDimension if we want to focus on certain dimensions (it can be a List)

     # Aggragation out put name: the name of the aggregation name
|aggFunction |aggPartitionBy |orderBy         |aggTimePeriod |aggTimePeriodSize  |aggTimePeriodSelected |aggMetric |aggColName                          |aggregationScopes|
|sum         |      |transactionDate |week          |1                  |0                     |montant   |sum_transactionId_over_current_week |                 |


    When j'applique le calcul d'aggregation

    Then le resultat de l'aggregation est le suivant

      |compte|transactionId|transactionDate|montant|week|month|transactionType|sum_transactionId_over_current_week:Double    |
      |     1|            A|     2020-11-20|    100|   0|    0|         cheque|                                    250|
      |     1|            B|     2020-11-20|    150|   0|    0|             VI|                                    250|
      |     1|            C|     2020-11-20|    100|   1|    0|         cheque|                                    250|
      |     1|            D|     2020-11-20|    100|   2|    0|             VI|                                    250|
      |     2|            E|     2020-11-20|    100|   0|    0|             VI|                                    300|
      |     2|            F|     2020-11-20|    100|   0|    0|             VI|                                    300|
      |     2|            G|     2020-11-20|    100|   0|    0|         cheque|                                    300|
      |     2|            H|     2020-11-20|    100|   4|    0|         cheque|                                    300|

  Scenario: Sum :: Current Week  :: GLOBAL SCOPE :: AGGREG SCOPE

    Given  les transactions suivantes
      # transform this data table to Transactions object
      |compte   | transactionId             | transactionDate:Date   | montant  | week  | month | transactionType |
      |1        | A                         | 2020-11-20             | 100      | 0     | 0     | cheque          |
      |1        | B                         | 2020-11-20             | 150      | 0     | 0     | VI              |
      |1        | C                         | 2020-11-20             | 100      | 1     | 0     | cheque          |
      |1        | D                         | 2020-11-20             | 100      | 2     | 0     | VI              |
      |2        | E                         | 2020-11-20             | 100      | 0     | 0     | VI              |
      |2        | F                         | 2020-11-20             | 100      | 0     | 0     | VI              |
      |2        | G                         | 2020-11-20             | 10       | 0     | 0     | cheque          |
      |2        | H                         | 2020-11-20             | 20       | 0     | 0     | cheque          |
      |2        | I                         | 2020-11-20             | 50       | 4     | 0     | cheque          |

    And maille choisie
      | partition |
      | compte    |

    And global scope
      | scope                                                 |
      | transactionType = "cheque" or transactionType = "VI"  |

    And aggregation List
      # transform this data table to List of Aggregation
      # aggregationFunction = Max, Min, Sum, Count, Avg ...
      # aggPartitionBy : should be client/account
      # aggregation Partition By : select subset dataframe to apply (compute) the Agg
      # aggregartion Order By used in case of aggregation is sensitive to the order (Max, Min etc...)
        # it can be a list of dimensions
      # aggTimePeriodSize The Lenght of Period (period of 12 Weeks, period of 3 Months etc...)
      # aggTimePeriodSelected corresponds to Period
          # 0-> N c-a-d LAST PERIOD
          # 1-> N-1 ==> Previous PERIOD
          # 2-> N-2
     # aggPartitionDimension if we want to focus on certain dimensions (it can be a List)

     # Aggragation out put name: the name of the aggregation name
|aggFunction |aggPartitionBy |orderBy         |aggTimePeriod |aggTimePeriodSize  |aggTimePeriodSelected |aggMetric |aggColName                            |aggregationScopes                   |
|sum         |            |transactionDate |week          |1                  |0                     |montant   |somme_montant_cheque_semaine_actuelle |transactionType = "cheque"          |


    When j'applique le calcul d'aggregation

    Then le resultat de l'aggregation est le suivant

      |compte|transactionId|transactionDate|montant|week|month|transactionType|sum_transactionId_over_current_week:Double |
      |     1|            A|     2020-11-20|    100|   0|    0|         cheque|                                    100|
      #|     1|            B|     2020-11-20|    150|   0|    0|             VI|                                    100|
      |     1|            C|     2020-11-20|    100|   1|    0|         cheque|                                    100|
      #|     1|            D|     2020-11-20|    100|   2|    0|             VI|                                    100|
      #|     2|            E|     2020-11-20|    100|   0|    0|             VI|                                     30|
      #|     2|            F|     2020-11-20|    100|   0|    0|             VI|                                     30|
      |     2|            G|     2020-11-20|     10|   0|    0|         cheque|                                     30|
      |     2|            H|     2020-11-20|     20|   0|    0|         cheque|                                     30|
      |     2|            I|     2020-11-20|     50|   4|    0|         cheque|                                     30|

  Scenario: Sum :: Current Week  :: GLOBAL SCOPE :: AGGREG SCOPE :: DISJUNCTION

    Given  les transactions suivantes
      # transform this data table to Transactions object
      |compte   | transactionId             | transactionDate:Date   | montant  | week  | month | transactionType |
      |1        | A                         | 2020-11-20             | 100      | 0     | 0     | cheque          |
      |1        | B                         | 2020-11-20             | 150      | 0     | 0     | VI              |
      |1        | C                         | 2020-11-20             | 100      | 1     | 0     | cheque          |
      |1        | D                         | 2020-11-20             | 100      | 2     | 0     | VI              |
      |2        | E                         | 2020-11-20             | 100      | 0     | 0     | VI              |
      |2        | F                         | 2020-11-20             | 100      | 0     | 0     | VI              |
      |2        | G                         | 2020-11-20             | 10       | 0     | 0     | cheque          |
      |2        | H                         | 2020-11-20             | 20       | 0     | 0     | cheque          |
      |2        | I                         | 2020-11-20             | 50       | 4     | 0     | cheque          |

    And maille choisie
      | partition |
      | compte    |

    And global scope
      | scope                                                 |
      | transactionType = "VI"  |

    And aggregation List
      # transform this data table to List of Aggregation
      # aggregationFunction = Max, Min, Sum, Count, Avg ...
      # aggPartitionBy : should be client/account
      # aggregation Partition By : select subset dataframe to apply (compute) the Agg
      # aggregartion Order By used in case of aggregation is sensitive to the order (Max, Min etc...)
        # it can be a list of dimensions
      # aggTimePeriodSize The Lenght of Period (period of 12 Weeks, period of 3 Months etc...)
      # aggTimePeriodSelected corresponds to Period
          # 0-> N c-a-d LAST PERIOD
          # 1-> N-1 ==> Previous PERIOD
          # 2-> N-2
     # aggPartitionDimension if we want to focus on certain dimensions (it can be a List)

     # Aggragation out put name: the name of the aggregation name
      |aggFunction |aggPartitionBy |orderBy         |aggTimePeriod |aggTimePeriodSize  |aggTimePeriodSelected |aggMetric |aggColName                            |aggregationScopes                   |
      |sum         |      |transactionDate |week          |1                  |0                     |montant   |somme_montant_cheque_semaine_actuelle |transactionType = "cheque"  |


    When j'applique le calcul d'aggregation

    Then le resultat de l'aggregation est le suivant

      |compte|transactionId|transactionDate|montant|week|month|transactionType|sum_transactionId_over_current_week:Double  |
      #|     1|            B|     2020-11-20|    150|   0|    0|             VI|                                    0|
      #|     1|            D|     2020-11-20|    100|   2|    0|             VI|                                    0|
      #|     2|            E|     2020-11-20|    100|   0|    0|             VI|                                    0|
      #|     2|            F|     2020-11-20|    100|   0|    0|             VI|                                    0|

  Scenario: Sum :: Current Week  :: GLOBAL SCOPE :: AGGREG SCOPE :: COUNTER-PARTS :: HR/VHR

    Given  les transactions suivantes
      # transform this data table to Transactions object
      |compte   | transactionId             | transactionDate:Date   | montant          | week  | month | transactionType | Direction | Contre_partie |  Country_Risk |
      |1        | A                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP1           | HR        |
      |1        | B                         | 2020-11-20             | 150              | 0     | 0     | VI              | TO        | CP1           | HR        |
      |1        | C                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP1           | HR        |
      |1        | D                         | 2020-11-20             | 100              | 0     | 0     | VI              | TO        | CP2           | HR        |
      |1        | E                         | 2020-11-20             | 100              | 0     | 0     | VI              | TO        | CP2           | HR        |
      |1        | F                         | 2020-11-20             | 100              | 0     | 0     | VI              | TO        | CP3           | HR        |
      |1        | G                         | 2020-11-20             | 10               | 0     | 0     | SEPA            | TO        | CP4           | LR        |
      |1        | H                         | 2020-11-20             | 20               | 0     | 0     | SEPA            | TO        | CP4           | LR        |
      |1        | I                         | 2020-11-20             | 50               | 0     | 0     | SEPA            | TO        | CP4           | LR        |

    And maille choisie
      | partition |
      | compte    |

    And global scope
      | scope                                                 |
      | transactionType = "SEPA" or transactionType = "VI"  |

    And aggregation List

     # Aggragation out put name: the name of the aggregation name
|aggFunction |aggPartitionBy |orderBy         |aggTimePeriod |aggTimePeriodSize  |aggTimePeriodSelected |aggMetric |aggColName                                 |aggregationScopes                                                     |
|sum         |      |transactionDate |week          |1                  |0                     |montant   |somme_montant_SEPA_TO_HR_semaine_actuelle  |transactionType = "SEPA" and  Direction = "TO"  and Country_Risk="HR" |


    When j'applique le calcul d'aggregation

    Then le resultat de l'aggregation est le suivant

      |compte   | transactionId             | transactionDate:Date   | montant          | week  | month | transactionType | Direction | Contre_partie |  Country_Risk | somme_montant_SEPA_TO_HR_semaine_actuelle:Double|
      |1        | A                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP1           | HR        |200                                           |
      #|1        | B                         | 2020-11-20             | 150              | 0     | 0     | VI              | TO        | CP1           | HR        |200                                           |
      |1        | C                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP1           | HR        |200                                           |
      #|1        | D                         | 2020-11-20             | 100              | 0     | 0     | VI              | TO        | CP2           | HR        |200                                           |
      #|1        | E                         | 2020-11-20             | 100              | 0     | 0     | VI              | TO        | CP2           | HR        |200                                           |
      #|1        | F                         | 2020-11-20             | 100              | 0     | 0     | VI              | TO        | CP3           | HR        |200                                           |
      |1        | G                         | 2020-11-20             | 10               | 0     | 0     | SEPA            | TO        | CP4           | LR        |200                                           |
      |1        | H                         | 2020-11-20             | 20               | 0     | 0     | SEPA            | TO        | CP4           | LR        |200                                           |
      |1        | I                         | 2020-11-20             | 50               | 0     | 0     | SEPA            | TO        | CP4           | LR        |200                                           |

  Scenario: Framework  Testing :: Sum + Current Week +Scope Scope + Contre-Parties+ TO+Mix TO/FROM

    Given  les transactions suivantes
      # transform this data table to Transactions object
      |compte   | transactionId             | transactionDate:Date   | montant          | week  | month | transactionType | Direction | Contre_partie |  Country_Risk |
      |1        | A                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP1           | HR        |
      |1        | B                         | 2020-11-20             | 150              | 0     | 0     | VI              | TO        | CP1           | HR        |
      |1        | C                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | FROM      | CP1           | HR        |
      |1        | D                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | FROM      | CP2           | HR        |
      |1        | E                         | 2020-11-20             | 100              | 0     | 0     | VI              | TO        | CP2           | HR        |
      |1        | F                         | 2020-11-20             | 100              | 0     | 0     | VI              | TO        | CP3           | HR        |
      |1        | G                         | 2020-11-20             | 10               | 0     | 0     | SEPA            | TO        | CP4           | LR        |
      |1        | H                         | 2020-11-20             | 20               | 0     | 0     | SEPA            | TO        | CP4           | LR        |
      |1        | I                         | 2020-11-20             | 50               | 0     | 0     | SEPA            | TO        | CP4           | LR        |

    And maille choisie
      | partition |
      | compte    |

    And global scope
      | scope                                               |
      | transactionType = "SEPA" or transactionType = "VI"  |

    And aggregation List

     # Aggragation out put name: the name of the aggregation name
      |aggFunction |aggPartitionBy |orderBy         |aggTimePeriod |aggTimePeriodSize  |aggTimePeriodSelected |aggMetric |aggColName                                 |aggregationScopes                                                     |
      |sum         |      |transactionDate |week          |1                  |0                     |montant   |somme_montant_MIX_SEPA_TO_HR_semaine_actuelle  |transactionType = "SEPA" and  Direction = "TO"  and Country_Risk="HR" |


    When j'applique le calcul d'aggregation

    Then le resultat de l'aggregation est le suivant

      |compte   | transactionId             | transactionDate:Date   | montant          | week  | month | transactionType | Direction | Contre_partie |  Country_Risk | somme_montant_MIX_SEPA_TO_HR_semaine_actuelle:Double |
      |1        | A                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP1           | HR            | 100                                                  |
      #|1        | B                         | 2020-11-20             | 150              | 0     | 0     | VI              | TO        | CP1           | HR            | 100                                                  |
      |1        | C                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | FROM      | CP1           | HR            | 100                                                  |
      |1        | D                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | FROM      | CP2           | HR            | 100                                                  |
      #|1        | E                         | 2020-11-20             | 100              | 0     | 0     | VI              | TO        | CP2           | HR            | 100                                                  |
      #|1        | F                         | 2020-11-20             | 100              | 0     | 0     | VI              | TO        | CP3           | HR            | 100                                                  |
      |1        | G                         | 2020-11-20             | 10               | 0     | 0     | SEPA            | TO        | CP4           | LR            | 100                                                  |
      |1        | H                         | 2020-11-20             | 20               | 0     | 0     | SEPA            | TO        | CP4           | LR            | 100                                                  |
      |1        | I                         | 2020-11-20             | 50               | 0     | 0     | SEPA            | TO        | CP4           | LR            | 100                                                  |

  Scenario: Framework  Testing :: Sum + Current Week +Scope Scope + Contre-Parties Outconming transaction to [VHR, HR]

    Given  les transactions suivantes
      # transform this data table to Transactions object
      |compte   | transactionId             | transactionDate:Date   | montant          | week  | month | transactionType | Direction | Contre_partie |  country_risk |
      |1        | A                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP1           | HR            |
      |1        | B                         | 2020-11-20             | 150              | 0     | 0     | SEPA            | TO        | CP1           | HR            |
      |1        | C                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | FROM      | CP1           | HR            |
      |1        | D                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | FROM      | CP2           | HR            |
      |1        | E                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP2           | HR            |
      |1        | F                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP3           | VHR           |
      |1        | G                         | 2020-11-20             | 10               | 0     | 0     | SEPA            | TO        | CP4           | VHR           |
      |1        | H                         | 2020-11-20             | 20               | 0     | 0     | SEPA            | TO        | CP4           | VHR           |
      |1        | I                         | 2020-11-20             | 50               | 0     | 0     | SEPA            | TO        | CP4           | VHR           |
      |1        | I                         | 2020-11-20             | 50               | 0     | 0     | SEPA            | TO        | CP4           | NORMAL        |

    And maille choisie
      | partition |
      | compte    |

    And global scope
      | scope                    |
      | transactionType = "SEPA" |


    And aggregation List

     # Aggragation out put name: the name of the aggregation name
|aggFunction |aggPartitionBy |orderBy         |aggTimePeriod |aggTimePeriodSize  |aggTimePeriodSelected |aggMetric |aggColName                         |aggregationScopes                                   |
|sum         |      |transactionDate |week          |1                  |0                     |montant   |somme_montant_SEPA_sortant_VHR_HR  | Direction = 'TO' and country_risk in ('HR', 'VHR') |


    When j'applique le calcul d'aggregation

    Then le resultat de l'aggregation est le suivant

      |compte   | transactionId             | transactionDate:Date   | montant          | week  | month | transactionType | Direction | Contre_partie |  country_risk |somme_montant_SEPA_sortant_VHR_HR:Double |
      |1        | A                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP1           | HR            |530                                      |
      |1        | B                         | 2020-11-20             | 150              | 0     | 0     | SEPA            | TO        | CP1           | HR            |530                                      |
      #|1        | C                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | FROM      | CP1           | HR            |530                                      |
      #|1        | D                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | FROM      | CP2           | HR            |530                                      |
      |1        | E                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP2           | HR            |530                                      |
      |1        | F                         | 2020-11-20             | 100              | 0     | 0     | SEPA            | TO        | CP3           | VHR           |530                                      |
      |1        | G                         | 2020-11-20             | 10               | 0     | 0     | SEPA            | TO        | CP4           | VHR           |530                                      |
      |1        | H                         | 2020-11-20             | 20               | 0     | 0     | SEPA            | TO        | CP4           | VHR           |530                                      |
      |1        | I                         | 2020-11-20             | 50               | 0     | 0     | SEPA            | TO        | CP4           | VHR           |530                                      |
      #|1        | I                         | 2020-11-20             | 50               | 0     | 0     | SEPA            | TO        | CP4           | NORMAL        |530                                      |


  Scenario: Framework  Testing :: MAX + Current Week +Scope Scope + Contre-Parties Outconming transaction to [VHR, HR]

    Given  les transactions suivantes
      # transform this data table to Transactions object
      |compte   | transactionId             | transactionDate:Date   | montant:Double          | week  | month | transactionType | Direction | Contre_partie |  country_risk |
      |1        | A                         | 2020-11-01             | 100              | 0     | 0     | SEPA            | TO        | CP1           | HR            |
      |1        | B                         | 2020-11-02             | 120              | 0     | 0     | SEPA            | TO        | CP1           | HR            |
      |1        | C                         | 2020-11-03             | 130              | 0     | 0     | SEPA            | FROM      | CP1           | HR            |
      |1        | D                         | 2020-11-04             | 140              | 0     | 0     | SEPA            | FROM      | CP2           | HR            |
      |1        | E                         | 2020-11-05             | 150              | 0     | 0     | SEPA            | TO        | CP2           | HR            |
      |1        | F                         | 2020-11-06             | 160              | 0     | 0     | SEPA            | TO        | CP3           | VHR           |
      |1        | G                         | 2020-11-07             | 170               | 0     | 0     | SEPA            | TO        | CP4           | VHR           |
      |1        | H                         | 2020-11-08             | 190               | 0     | 0     | SEPA            | TO        | CP4           | VHR           |
      |1        | I                         | 2020-11-09             | 200               | 0     | 0     | SEPA            | TO        | CP4           | VHR           |
      |1        | I                         | 2020-11-10             | 210               | 0     | 0     | NORMAL            | TO        | CP4           | NORMAL        |

    And maille choisie
      | partition |
      | compte    |

    And global scope
      | scope                    |
      | transactionType = "SEPA" |


    And aggregation List

     # Aggragation out put name: the name of the aggregation name
      |aggFunction |aggPartitionBy |orderBy         |aggTimePeriod |aggTimePeriodSize  |aggTimePeriodSelected |aggMetric |aggColName                         |aggregationScopes                                   |
      |max         |      |transactionDate |week          |1                  |0                     |montant   |MAX_montant_SEPA_sortant_VHR_HR    | Direction = 'TO' and country_risk in ('HR', 'VHR') |


    When j'applique le calcul d'aggregation

    Then le resultat de l'aggregation est le suivant

      |compte   | transactionId             | transactionDate:Date   | montant:Double         | week  | month | transactionType | Direction | Contre_partie |  country_risk |MAX_montant_SEPA_sortant_VHR_HR:Double |
      |1        | A                         | 2020-11-01             | 100              | 0     | 0     | SEPA            | TO        | CP1           | HR            |200                                      |
      |1        | B                         | 2020-11-02             | 120              | 0     | 0     | SEPA            | TO        | CP1           | HR            |200                                      |
      #|1        | C                         | 2020-11-03             | 130              | 0     | 0     | SEPA            | FROM      | CP1           | HR            |200                                      |
      #|1        | D                         | 2020-11-04             | 140              | 0     | 0     | SEPA            | FROM      | CP2           | HR            |200                                      |
      |1        | E                         | 2020-11-05             | 150              | 0     | 0     | SEPA            | TO        | CP2           | HR            |200                                      |
      |1        | F                         | 2020-11-06             | 160              | 0     | 0     | SEPA            | TO        | CP3           | VHR           |200                                      |
      |1        | G                         | 2020-11-07             | 170               | 0     | 0     | SEPA            | TO        | CP4           | VHR           |200                                      |
      |1        | H                         | 2020-11-08             | 190               | 0     | 0     | SEPA            | TO        | CP4           | VHR           |200                                      |
      |1        | I                         | 2020-11-09             | 200               | 0     | 0     | SEPA            | TO        | CP4           | VHR           |200                                      |
      #|1        | I                         | 2020-11-10             | 210               | 0     | 0     | NORMAL            | TO        | CP4           | NORMAL        |200                                      |


  Scenario: Framework  Testing :: LAST TRANSACTION + Current Week +Scope Scope +  Outconming transaction to [VHR, HR]

    Given  les transactions suivantes
      # transform this data table to Transactions object
      |compte   | transactionId             | transactionDate:Date   | montant:Double          | week  | month | transactionType | Direction | Contre_partie |  country_risk |
      |1        | A                         | 2020-08-01             | 111              | 0     | 0     | SEPA            | TO        | CP1           | HR            |
      |1        | B                         | 2020-11-05             | 120              | 0     | 0     | SEPA            | TO        | CP1           | HR            |
      |1        | C                         | 2020-11-03             | 130              | 0     | 0     | SEPA            | FROM      | CP1           | HR            |
      |1        | D                         | 2020-11-10             | 140              | 0     | 0     | SEPA            | FROM      | CP2           | HR            |
      |1        | E                         | 2020-11-01             | 150              | 0     | 0     | SEPA            | TO        | CP2           | HR            |
      |1        | F                         | 2020-11-03             | 160              | 0     | 0     | SEPA            | TO        | CP3           | VHR           |
      |1        | G                         | 2020-11-04             | 170               | 0     | 0     | SEPA            | TO        | CP4           | VHR           |
      |1        | H                         | 2020-11-07             | 222               | 0     | 0     | SEPA            | TO        | CP4           | VHR           |
      |1        | I                         | 2020-10-09             | 999               | 0     | 0     | SEPA            | TO        | CP4           | VHR           |
      |1        | I                         | 2020-11-06             | 210               | 0     | 0     | NORMAL            | TO        | CP4           | NORMAL        |

    And maille choisie
      | partition |
      | compte    |

    And Sort Transaction By
      | orderBy |
      | transactionDate    |


    And global scope
      | scope                    |
      |  |


    And aggregation List

     # Aggragation out put name: the name of the aggregation name
      |aggFunction |aggPartitionBy |orderBy         |aggTimePeriod |aggTimePeriodSize  |aggTimePeriodSelected |aggMetric |aggColName                         |aggregationScopes                                   |
      |first       |compte      |transactionDate |week          |1                  |0                     |montant   |RECENT_montant_SEPA_sortant_VHR_HR | Direction = 'TO' and country_risk in ('HR', 'VHR') |
      |last        |compte      |transactionDate |week          |1                  |0                     |montant   |OLDEST_montant_SEPA_sortant_VHR_HR  | Direction = 'TO' and country_risk in ('HR', 'VHR') |


    When j'applique le calcul d'aggregation

    Then le resultat de l'aggregation est le suivant

      |compte   | transactionId             | transactionDate:Date   | montant:Double          | week  | month | transactionType | Direction | Contre_partie |  country_risk | RECENT_montant_SEPA_sortant_VHR_HR:Double|OLDEST_montant_SEPA_sortant_VHR_HR:Double|
      |1        | A                         | 2020-08-01             | 111                     | 0     | 0     | SEPA            | TO        | CP1           | HR            | 222                                    |111                                            |
      |1        | B                         | 2020-11-05             | 120                     | 0     | 0     | SEPA            | TO        | CP1           | HR            | 222                                    |111                                            |
      #|1        | C                         | 2020-11-03             | 130                    | 0     | 0     | SEPA            | FROM      | CP1           | HR            | 222                                    |111                                            |
      #|1        | D                         | 2020-11-10             | 140                    | 0     | 0     | SEPA            | FROM      | CP2           | HR            | 222                                    |111                                            |
      |1        | E                         | 2020-11-01             | 150                     | 0     | 0     | SEPA            | TO        | CP2           | HR            | 222                                    |111                                            |
      |1        | F                         | 2020-11-03             | 160                     | 0     | 0     | SEPA            | TO        | CP3           | VHR           | 222                                    |111                                            |
      |1        | G                         | 2020-11-04             | 170                     | 0     | 0     | SEPA            | TO        | CP4           | VHR           | 222                                    |111                                            |
      |1        | H                         | 2020-11-07             | 222                     | 0     | 0     | SEPA            | TO        | CP4           | VHR           | 222                                    |111                                            |
      |1        | I                         | 2020-10-09             | 999                     | 0     | 0     | SEPA            | TO        | CP4           | VHR           | 222                                    |111                                            |
      #|1        | I                         | 2020-11-10             | 210                    | 0     | 0     | NORMAL            | TO        | CP4           | NORMAL        | 222                                    |111                                            |



  Scenario: Framework  Testing :: INNER CASE

    Given  les transactions suivantes
      # transform this data table to Transactions object
      |compte   | transactionId             | transactionDate:Date   | montant:Double          | week  | month | transactionType | Direction | Contre_partie |  country_risk |
      |1        | A                         | 2020-08-01             | 111              | 0     | 0     | SEPA            | TO        | CP1           | HR            |
      |1        | B                         | 2020-11-05             | 120              | 0     | 0     | SEPA            | TO        | CP1           | HR            |
      |1        | C                         | 2020-11-03             | 130              | 0     | 0     | SEPA            | FROM      | CP1           | HR            |
      |1        | D                         | 2020-11-10             | 140              | 0     | 0     | SEPA            | FROM      | CP2           | HR            |
      |1        | E                         | 2020-11-01             | 150              | 0     | 0     | SEPA            | TO        | CP2           | HR            |
      |1        | F                         | 2020-11-03             | 160              | 0     | 0     | SEPA            | TO        | CP3           | VHR           |
      |1        | G                         | 2020-11-04             | 170               | 0     | 0     | NORMAL            | TO        | CP4           | VHR           |
      |1        | H                         | 2020-11-07             | 222               | 0     | 0     | NORMAL            | TO        | CP4           | VHR           |
      |1        | I                         | 2020-10-09             | 999               | 0     | 0     | NORMAL            | TO        | CP4           | VHR           |
      |1        | I                         | 2020-11-06             | 210               | 0     | 0     | NORMAL            | TO        | CP4           | NORMAL        |
      |1        | J                         | 2020-11-06             | 210               | 0     | 0     | OTHER            | TO        | CP4           | NORMAL        |



    And maille choisie
      | partition |
      | compte    |

    And Sort Transaction By
      | orderBy |
      | transactionDate    |


    And global scope
      | scope                    |
      |  |


    And aggregation List

     # Aggragation out put name: the name of the aggregation name
      |aggFunction |aggPartitionBy |orderBy         |aggTimePeriod |aggTimePeriodSize  |aggTimePeriodSelected |aggMetric |aggColName                         |aggregationScopes                                   |
      |count        |      |transactionDate |week         |1                  |0                     |montant   |Count_Sepa_TO_HR_VHR               | transactionType = 'SEPA' and Direction = 'TO' and country_risk in ('HR', 'VHR') |
      |count       |      |transactionDate |week          |1                  |0                     |montant   |Count_Sepa_Normal_TO_HR_VHR        | transactionType in ('SEPA','NORMAL') and Direction = 'TO' and country_risk in ('HR', 'VHR') |


    When j'applique le calcul d'aggregation

    Then le resultat de l'aggregation est le suivant

      |compte|transactionId|transactionDate|montant|week|month|transactionType|Direction|Contre_partie|country_risk|Count_Sepa_TO_HR_VHR|Count_Sepa_Normal_TO_HR_VHR|
      |     1|            A|     2020-08-01|  111.0|   0|    0|           SEPA|       TO|          CP1|          HR|                   4|                          7|
      |     1|            B|     2020-11-05|  120.0|   0|    0|           SEPA|       TO|          CP1|          HR|                   4|                          7|
      |     1|            E|     2020-11-01|  150.0|   0|    0|           SEPA|       TO|          CP2|          HR|                   4|                          7|
      |     1|            F|     2020-11-03|  160.0|   0|    0|           SEPA|       TO|          CP3|         VHR|                   4|                          7|
      |     1|            G|     2020-11-04|  170.0|   0|    0|         NORMAL|       TO|          CP4|         VHR|                   4|                          7|
      |     1|            H|     2020-11-07|  222.0|   0|    0|         NORMAL|       TO|          CP4|         VHR|                   4|                          7|
      |     1|            I|     2020-10-09|  999.0|   0|    0|         NORMAL|       TO|          CP4|         VHR|                   4|                          7|
     # |1        | J                         | 2020-11-06             | 210               | 0     | 0     | OTHER            | TO        | CP4           | NORMAL        |



  Scenario: Framework  Testing :: Sum amount TRANSACTION + Current Week +Scope Scope + Contre-partie  Outconming transaction to [VHR, HR]

    Given  les transactions suivantes
      # transform this data table to Transactions object
      |compte   | transactionId             | transactionDate:Date   | montant:Double          | week  | month | transactionType | Direction | Contre_partie |  country_risk |
      |1        | A                         | 2020-08-01             | 111              | 0     | 0     | SEPA            | TO        | CP1           | HR            |
      |1        | B                         | 2020-11-05             | 120              | 0     | 0     | SEPA            | TO        | CP1           | HR            |
      |1        | C                         | 2020-11-03             | 130              | 0     | 0     | SEPA            | FROM      | CP1           | HR            |
      |1        | D                         | 2020-11-10             | 140              | 0     | 0     | SEPA            | FROM      | CP2           | HR            |
      |1        | E                         | 2020-11-01             | 150              | 0     | 0     | SEPA            | TO        | CP2           | HR            |
      |1        | F                         | 2020-11-03             | 160              | 0     | 0     | SEPA            | TO        | CP3           | VHR           |
      |1        | G                         | 2020-11-04             | 170               | 0     | 0     | SEPA            | TO        | CP4           | VHR           |
      |1        | H                         | 2020-11-07             | 222               | 0     | 0     | SEPA            | TO        | CP4           | VHR           |
      |1        | I                         | 2020-10-09             | 999               | 0     | 0     | SEPA            | TO        | CP4           | VHR           |
      |1        | I                         | 2020-11-06             | 210               | 0     | 0     | NORMAL            | TO        | CP4           | VHR        |
      |2        | J                         | 2020-11-06             | 10               | 0     | 0     | SEPA            | TO        | CP5           | VHR        |
      |2        | J                         | 2020-11-06             | 10               | 0     | 0     | SEPA            | TO        | CP5           | VHR        |
      |2        | J                         | 2020-11-06             | 10               | 0     | 0     | SEPA            | TO        | CP6           | VHR        |
      |2        | J                         | 2020-11-06             | 10               | 0     | 0     | SEPA            | TO        | CP7           | HR        |

    And maille choisie
      | partition |
      | compte    |

    And Sort Transaction By
      | orderBy |
      | transactionDate    |


    And global scope
      | scope                    |
      | transactionType = 'SEPA' |


    And aggregation List

     # Aggragation out put name: the name of the aggregation name
      |aggFunction |aggPartitionBy        |orderBy         |aggTimePeriod |aggTimePeriodSize  |aggTimePeriodSelected |aggMetric |aggColName                         |aggregationScopes                                   |
      |sum         |Contre_partie      |transactionDate |week          |1                  |0                     |montant   |CUMULE_montant_SEPA_sortant_par_CP_VHR_HR | Direction = 'TO' and country_risk in ('HR', 'VHR') |


    When j'applique le calcul d'aggregation

    Then le resultat de l'aggregation est le suivant

      |compte   | transactionId             | transactionDate:Date   | montant:Double          | week  | month | transactionType | Direction | Contre_partie |  country_risk | CUMULE_montant_SEPA_sortant_par_CP_VHR_HR:Double|
      |1        | A                         | 2020-08-01             | 111                     | 0     | 0     | SEPA            | TO        | CP1           | HR            | 231                                             |
      |1        | B                         | 2020-11-05             | 120                     | 0     | 0     | SEPA            | TO        | CP1           | HR            | 231                                             |
      #|1        | C                         | 2020-11-03             | 130                    | 0     | 0     | SEPA            | FROM      | CP1           | HR            | 222                                    ||
      #|1        | D                         | 2020-11-10             | 140                    | 0     | 0     | SEPA            | FROM      | CP2           | HR            | 222                                    ||111
      |1        | E                         | 2020-11-01             | 150                     | 0     | 0     | SEPA            | TO        | CP2           | HR            | 150                                             |
      |1        | F                         | 2020-11-03             | 160                     | 0     | 0     | SEPA            | TO        | CP3           | VHR           | 160                                             |
      |1        | G                         | 2020-11-04             | 170                     | 0     | 0     | SEPA            | TO        | CP4           | VHR           | 1391                                            |
      |1        | H                         | 2020-11-07             | 222                     | 0     | 0     | SEPA            | TO        | CP4           | VHR           | 1391                                            |
      |1        | I                         | 2020-10-09             | 999                     | 0     | 0     | SEPA            | TO        | CP4           | VHR           | 1391                                            |
      |2        | J                         | 2020-11-06             | 10                      | 0     | 0     | SEPA            | TO        | CP5           | VHR           |20                                             |
      |2        | J                         | 2020-11-06             | 10                      | 0     | 0     | SEPA            | TO        | CP5           | VHR           |20                                             |
      |2        | J                         | 2020-11-06             | 10                      | 0     | 0     | SEPA            | TO        | CP6           | VHR           |10                                             |
      |2        | J                         | 2020-11-06             | 10                      | 0     | 0     | SEPA            | TO        | CP7           | HR            |10                                             |



  Scenario: Framework  Testing :: Multi Level Partitions

    Given  les transactions suivantes
      # transform this data table to Transactions object
      |agence   |client   | compte  | transactionId             | transactionDate:Date   | montant:Double   | week  | month | transactionType | Direction | Contre_partie |  country_risk |
      |Agence 1 |client 1 |1        | A                         | 2020-08-01             | 111              | 0     | 0     | SEPA            | TO        | CP1           | HR            |
      |Agence 1 |client 1 |1        | B                         | 2020-11-05             | 120              | 0     | 0     | SEPA            | TO        | CP1           | HR            |
      |Agence 1 |client 1 |1        | C                         | 2020-11-03             | 130              | 0     | 0     | SEPA            | FROM      | CP1           | HR            |
      |Agence 1 |client 1 |1        | D                         | 2020-11-10             | 140              | 0     | 0     | SEPA            | FROM      | CP2           | HR            |
      |Agence 1 |client 1 |1        | E                         | 2020-11-01             | 150              | 0     | 0     | SEPA            | TO        | CP2           | HR            |
      |Agence 1 |client 1 |1        | F                         | 2020-11-03             | 160              | 0     | 0     | SEPA            | TO        | CP3           | VHR           |
      |Agence 1 |client 1 |1        | G                         | 2020-11-04             | 170              | 0     | 0     | SEPA            | TO        | CP4           | VHR           |
      |Agence 1 |client 2 |1        | H                         | 2020-11-07             | 222              | 0     | 0     | SEPA            | TO        | CP4           | VHR           |
      |Agence 1 |client 2 |1        | I                         | 2020-10-09             | 999              | 0     | 0     | SEPA            | TO        | CP4           | VHR           |
      |Agence 1 |client 2 |1        | I                         | 2020-11-06             | 210              | 0     | 0     | NORMAL          | TO        | CP4           | VHR           |
      |Agence 1 |client 2 |2        | J                         | 2020-11-06             | 10               | 0     | 0     | SEPA            | TO        | CP5           | VHR           |
      |Agence 1 |client 2 |2        | J                         | 2020-11-06             | 10               | 0     | 0     | SEPA            | TO        | CP5           | VHR           |
      |Agence 1 |client 2 |2        | J                         | 2020-11-06             | 10               | 0     | 0     | SEPA            | TO        | CP6           | VHR           |
      |Agence 1 |client 2 |2        | J                         | 2020-11-06             | 10               | 0     | 0     | SEPA            | TO        | CP7           | HR            |
      |Agence 2 |client 3 |1        | K                         | 2020-08-01             | 111              | 0     | 0     | SEPA            | TO        | CP1           | HR            |
      |Agence 2 |client 3 |1        | L                         | 2020-11-05             | 120              | 0     | 0     | SEPA            | TO        | CP1           | HR            |
      |Agence 2 |client 3 |1        | M                         | 2020-11-03             | 130              | 0     | 0     | SEPA            | FROM      | CP1           | HR            |
      |Agence 2 |client 3 |1        | N                         | 2020-11-10             | 140              | 0     | 0     | SEPA            | FROM      | CP2           | HR            |
      |Agence 2 |client 3 |1        | O                         | 2020-11-01             | 150              | 0     | 0     | SEPA            | TO        | CP2           | HR            |
      |Agence 2 |client 3 |1        | Q                         | 2020-11-03             | 160              | 0     | 0     | SEPA            | TO        | CP3           | VHR           |
      |Agence 2 |client 3 |1        | R                         | 2020-11-04             | 170              | 0     | 0     | SEPA            | TO        | CP4           | VHR           |
      |Agence 2 |client 4 |1        | S                         | 2020-11-07             | 222              | 0     | 0     | SEPA            | TO        | CP4           | VHR           |
      |Agence 2 |client 4 |1        | T                         | 2020-10-09             | 999              | 0     | 0     | SEPA            | TO        | CP4           | VHR           |
      |Agence 2 |client 4 |1        | U                         | 2020-11-06             | 210              | 0     | 0     | NORMAL          | TO        | CP4           | VHR           |
      |Agence 2 |client 4 |2        | V                         | 2020-11-06             | 10               | 0     | 0     | SEPA            | TO        | CP5           | VHR           |
      |Agence 2 |client 4 |2        | W                         | 2020-11-06             | 10               | 0     | 0     | SEPA            | TO        | CP5           | VHR           |
      |Agence 2 |client 4 |2        | X                         | 2020-11-06             | 10               | 0     | 0     | SEPA            | TO        | CP6           | VHR           |
      |Agence 2 |client 4 |2        | Y                         | 2020-11-06             | 10               | 0     | 0     | SEPA            | TO        | CP7           | HR            |

    And maille choisie
      | partition |
      | agence    |
      | client    |
      | compte    |

    And Sort Transaction By
      | orderBy |
      | transactionDate    |


    And global scope
      | scope                    |
      | transactionType = 'SEPA' |


    And aggregation List

     # Aggragation out put name: the name of the aggregation name
      |aggFunction |aggPartitionBy        |orderBy         |aggTimePeriod |aggTimePeriodSize  |aggTimePeriodSelected |aggMetric |aggColName                         |aggregationScopes                                   |
      |count         |Contre_partie      |transactionDate |week          |1                  |0                     |montant   |count_SEPA_sortant_par_CP_VHR_HR | Direction = 'TO' and country_risk in ('HR', 'VHR') |


    When j'applique le calcul d'aggregation

    Then le resultat de l'aggregation est le suivant

    |  agence|  client|compte|transactionId|transactionDate:Date   |montant:Double|week|month|transactionType|Direction|Contre_partie|country_risk|count_SEPA_sortant_par_CP_VHR_HR:Long|
    |Agence 1|client 1|     1|            B|     2020-11-05|  120.0|   0|    0|           SEPA|       TO|          CP1|          HR|                               2|
    |Agence 1|client 1|     1|            A|     2020-08-01|  111.0|   0|    0|           SEPA|       TO|          CP1|          HR|                               2|
    |Agence 2|client 3|     1|            Q|     2020-11-03|  160.0|   0|    0|           SEPA|       TO|          CP3|         VHR|                               1|
    |Agence 1|client 1|     1|            F|     2020-11-03|  160.0|   0|    0|           SEPA|       TO|          CP3|         VHR|                               1|
    |Agence 1|client 1|     1|            G|     2020-11-04|  170.0|   0|    0|           SEPA|       TO|          CP4|         VHR|                               1|
    |Agence 1|client 2|     2|            J|     2020-11-06|   10.0|   0|    0|           SEPA|       TO|          CP6|         VHR|                               1|
    |Agence 2|client 4|     2|            V|     2020-11-06|   10.0|   0|    0|           SEPA|       TO|          CP5|         VHR|                               2|
    |Agence 2|client 4|     2|            W|     2020-11-06|   10.0|   0|    0|           SEPA|       TO|          CP5|         VHR|                               2|
    |Agence 1|client 2|     2|            J|     2020-11-06|   10.0|   0|    0|           SEPA|       TO|          CP5|         VHR|                               2|
    |Agence 1|client 2|     2|            J|     2020-11-06|   10.0|   0|    0|           SEPA|       TO|          CP5|         VHR|                               2|
    |Agence 2|client 4|     1|            S|     2020-11-07|  222.0|   0|    0|           SEPA|       TO|          CP4|         VHR|                               2|
    |Agence 2|client 4|     1|            T|     2020-10-09|  999.0|   0|    0|           SEPA|       TO|          CP4|         VHR|                               2|
    |Agence 2|client 4|     2|            Y|     2020-11-06|   10.0|   0|    0|           SEPA|       TO|          CP7|          HR|                               1|
    |Agence 2|client 3|     1|            R|     2020-11-04|  170.0|   0|    0|           SEPA|       TO|          CP4|         VHR|                               1|
    |Agence 1|client 2|     1|            H|     2020-11-07|  222.0|   0|    0|           SEPA|       TO|          CP4|         VHR|                               2|
    |Agence 1|client 2|     1|            I|     2020-10-09|  999.0|   0|    0|           SEPA|       TO|          CP4|         VHR|                               2|
    |Agence 1|client 2|     2|            J|     2020-11-06|   10.0|   0|    0|           SEPA|       TO|          CP7|          HR|                               1|
    |Agence 2|client 4|     2|            X|     2020-11-06|   10.0|   0|    0|           SEPA|       TO|          CP6|         VHR|                               1|
    |Agence 2|client 3|     1|            L|     2020-11-05|  120.0|   0|    0|           SEPA|       TO|          CP1|          HR|                               2|
    |Agence 2|client 3|     1|            K|     2020-08-01|  111.0|   0|    0|           SEPA|       TO|          CP1|          HR|                               2|
    |Agence 2|client 3|     1|            O|     2020-11-01|  150.0|   0|    0|           SEPA|       TO|          CP2|          HR|                               1|
    |Agence 1|client 1|     1|            E|     2020-11-01|  150.0|   0|    0|           SEPA|       TO|          CP2|          HR|                               1|


  Scenario: Collect List :: Current Week  :: ALL SCOPE

    Given  les transactions suivantes
      # transform this data table to Transactions objectsomme_montant_SEPA_TO_HR_semaine_actuelle
      |compte   | transactionId             | transactionDate:Date   | montant:Double         | week  | month | transactionType |
      |1        | A                         | 2020-11-20             | 100             | 0     | 0     | cheque          |
      |1        | B                         | 2020-11-20             | 150             | 0     | 0     | VI              |
      |1        | C                         | 2020-11-20             | 100             | 1     | 0     | cheque          |
      |1        | D                         | 2020-11-20             | 100             | 2     | 0     | VI              |
      |2        | E                         | 2020-11-20             | 100             | 0     | 0     | VI              |
      |2        | F                         | 2020-11-20             | 100             | 0     | 0     | VI              |
      |2        | G                         | 2020-11-20             | 100             | 0     | 0     | cheque          |
      |2        | H                         | 2020-11-20             | 100             | 4     | 0     | cheque          |

    And maille choisie
      | partition |
      | compte    |


    And global scope
      | scope |
      |       |

    And aggregation List
      # transform this data table to List of Aggregation
      # aggregationFunction = Max, Min, Sum, Count, Avg ...
      # aggPartitionBy : should be client/account
      # aggregation Partition By : select subset dataframe to apply (compute) the Agg
      # aggregartion Order By used in case of aggregation is sensitive to the order (Max, Min etc...)
        # it can be a list of dimensions
      # aggTimePeriodSize The Lenght of Period (period of 12 Weeks, period of 3 Months etc...)
      # aggTimePeriodSelected corresponds to Period
          # 0-> N c-a-d LAST PERIOD
          # 1-> N-1 ==> Previous PERIOD
          # 2-> N-2
     # aggPartitionDimension if we want to focus on certain dimensions (it can be a List)

     # Aggragation out put name: the name of the aggregation name
      |aggFunction |aggPartitionBy |orderBy         |aggTimePeriod |aggTimePeriodSize  |aggTimePeriodSelected |aggMetric |aggColName                          |aggregationScopes|
      |collect_set |               |transactionDate |week          |1                  |0                     |montant   |list_montant_over_current_week      |                 |


    When j'applique le calcul d'aggregation

    Then le resultat de l'aggregation est le suivant

      |compte|transactionId|transactionDate:Date|montant:Double|week|month|transactionType|list_montant_over_current_week:List[Double]|
  |     1|            A|     2020-11-20|  100.0|   0|    0|         cheque|                  [100.0, 150.0]|
  |     1|            B|     2020-11-20|  150.0|   0|    0|             VI|                  [100.0, 150.0]|
  |     1|            C|     2020-11-20|  100.0|   1|    0|         cheque|                  [100.0, 150.0]|
  |     1|            D|     2020-11-20|  100.0|   2|    0|             VI|                  [100.0, 150.0]|
  |     2|            E|     2020-11-20|  100.0|   0|    0|             VI|                         [100.0]|
  |     2|            F|     2020-11-20|  100.0|   0|    0|             VI|                         [100.0]|
  |     2|            G|     2020-11-20|  100.0|   0|    0|         cheque|                        [100.0]|
  |     2|            H|     2020-11-20|  100.0|   4|    0|         cheque|                         [100.0]|





  Scenario: Collect List :: EMPTY

    Given  les transactions suivantes
      # transform this data table to Transactions objectsomme_montant_SEPA_TO_HR_semaine_actuelle
      |compte   | transactionId             | transactionDate:Date   | montant:Double         | week  | month | transactionType |
      |1        | A                         | 2020-11-20             | 100             | 0     | 0     | cheque          |
      |1        | B                         | 2020-11-20             | 150             | 0     | 0     | VI              |
      |1        | C                         | 2020-11-20             | 100             | 0     | 0     | cheque          |
      |1        | D                         | 2020-11-20             | 100             | 0     | 0     | VI              |


    And maille choisie
      | partition |
      | compte    |


    And global scope
      | scope |
      |       |

    And aggregation List
      # transform this data table to List of Aggregation
      # aggregationFunction = Max, Min, Sum, Count, Avg ...
      # aggPartitionBy : should be client/account
      # aggregation Partition By : select subset dataframe to apply (compute) the Agg
      # aggregartion Order By used in case of aggregation is sensitive to the order (Max, Min etc...)
        # it can be a list of dimensions
      # aggTimePeriodSize The Lenght of Period (period of 12 Weeks, period of 3 Months etc...)
      # aggTimePeriodSelected corresponds to Period
          # 0-> N c-a-d LAST PERIOD
          # 1-> N-1 ==> Previous PERIOD
          # 2-> N-2
     # aggPartitionDimension if we want to focus on certain dimensions (it can be a List)

     # Aggragation out put name: the name of the aggregation name
      |aggFunction |aggPartitionBy |orderBy         |aggTimePeriod |aggTimePeriodSize  |aggTimePeriodSelected |aggMetric |aggColName                          |aggregationScopes|
      |collect_set |               |transactionDate |week          |1                  |1                     |montant   |list_montant_over_previous_week      |                 |


    When j'applique le calcul d'aggregation

    Then le resultat de l'aggregation est le suivant

      |compte|transactionId|transactionDate:Date|montant:Double|week|month|transactionType|list_montant_over_current_week:List[Double]|
      |     1|            A|     2020-11-20|  100.0|   0|    0|         cheque|                  null |
      |     1|            B|     2020-11-20|  150.0|   0|    0|             VI|                  null |
      |     1|            C|     2020-11-20|  100.0|   0|    0|         cheque|                  null |
      |     1|            D|     2020-11-20|  100.0|   0|    0|             VI|                  null |
