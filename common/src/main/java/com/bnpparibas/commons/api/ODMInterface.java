package com.bnpparibas.commons.api;

import com.bnpparibas.commons.models.domain.Alert;
import com.bnpparibas.commons.models.domain.EnrichedTransactionWithAggregs;
import com.bnpparibas.commons.models.domain.Scenario;
import com.bnpparibas.commons.models.raw.Customer;

import java.io.Serializable;

public interface ODMInterface extends Serializable {

    public Integer computeProfondeur(String scenarioName, Customer customer); //via table de decision

    public Scenario retrieveScenario(String scenarioName); //DS_11_1_A

    public Alert evaluateRules(EnrichedTransactionWithAggregs enrichedTransactionWithAggregs, Scenario scenario);

}

