package com.bnpparibas.commons.models.raw;

import lombok.*;


public class TransactionType {
    private String TXN_TYPE_CODE;
    private String TXN_TYPE_DESC;
    private String TXN_TYPE_NAME;
    private String SCOPE;
    private String INSTRUMENT;
    private String REGION;
    private String INTERNAL_RISK;
    private String CREDIT_DEBIT_CODE;
    private String ID_DATE_CHGT_INS;
    private String ID_DATE_CHGT_MAJ;
    private String UPDATE_DATE;

    public TransactionType(){}

    public String getTXN_TYPE_CODE() {
        return TXN_TYPE_CODE;
    }

    public void setTXN_TYPE_CODE(String TXN_TYPE_CODE) {
        this.TXN_TYPE_CODE = TXN_TYPE_CODE;
    }

    public String getTXN_TYPE_DESC() {
        return TXN_TYPE_DESC;
    }

    public void setTXN_TYPE_DESC(String TXN_TYPE_DESC) {
        this.TXN_TYPE_DESC = TXN_TYPE_DESC;
    }

    public String getTXN_TYPE_NAME() {
        return TXN_TYPE_NAME;
    }

    public void setTXN_TYPE_NAME(String TXN_TYPE_NAME) {
        this.TXN_TYPE_NAME = TXN_TYPE_NAME;
    }

    public String getSCOPE() {
        return SCOPE;
    }

    public void setSCOPE(String SCOPE) {
        this.SCOPE = SCOPE;
    }

    public String getINSTRUMENT() {
        return INSTRUMENT;
    }

    public void setINSTRUMENT(String INSTRUMENT) {
        this.INSTRUMENT = INSTRUMENT;
    }

    public String getREGION() {
        return REGION;
    }

    public void setREGION(String REGION) {
        this.REGION = REGION;
    }

    public String getINTERNAL_RISK() {
        return INTERNAL_RISK;
    }

    public void setINTERNAL_RISK(String INTERNAL_RISK) {
        this.INTERNAL_RISK = INTERNAL_RISK;
    }

    public String getCREDIT_DEBIT_CODE() {
        return CREDIT_DEBIT_CODE;
    }

    public void setCREDIT_DEBIT_CODE(String CREDIT_DEBIT_CODE) {
        this.CREDIT_DEBIT_CODE = CREDIT_DEBIT_CODE;
    }

    public String getID_DATE_CHGT_INS() {
        return ID_DATE_CHGT_INS;
    }

    public void setID_DATE_CHGT_INS(String ID_DATE_CHGT_INS) {
        this.ID_DATE_CHGT_INS = ID_DATE_CHGT_INS;
    }

    public String getID_DATE_CHGT_MAJ() {
        return ID_DATE_CHGT_MAJ;
    }

    public void setID_DATE_CHGT_MAJ(String ID_DATE_CHGT_MAJ) {
        this.ID_DATE_CHGT_MAJ = ID_DATE_CHGT_MAJ;
    }

    public String getUPDATE_DATE() {
        return UPDATE_DATE;
    }

    public void setUPDATE_DATE(String UPDATE_DATE) {
        this.UPDATE_DATE = UPDATE_DATE;
    }
}