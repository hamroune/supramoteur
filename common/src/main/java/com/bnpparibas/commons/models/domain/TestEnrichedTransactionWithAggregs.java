package com.bnpparibas.commons.models.domain;

import com.bnpparibas.commons.models.types.generic.Field;
import lombok.*;

import java.util.ArrayList;
import java.util.List;
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class TestEnrichedTransactionWithAggregs extends TestEnrichedTransaction{
    private List<Field> aggregs = new ArrayList<>();
}
