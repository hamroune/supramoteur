package com.bnpparibas.commons.models.processed;

import com.bnpparibas.commons.models.raw.CBTransaction;


public class RawTransactionWithCBTransactionType extends CBTransaction {

    //private String TXN_TYPE_CODE;
    private String TXN_TYPE_DESC;
    private String TXN_TYPE_NAME;
    private String SCOPE ;
    private String INSTRUMENT;
    private String REGION;
    private String INTERNAL_RISK;
    private String CREDIT_DEBIT_CODE;
//    private String ID_DATE_CHGT_INS;
//    private String ID_DATE_CHGT_MAJ;
//    private String UPDATE_DATE;


    public RawTransactionWithCBTransactionType() {

    }

    public String getTXN_TYPE_DESC() {
        return TXN_TYPE_DESC;
    }

    public void setTXN_TYPE_DESC(String TXN_TYPE_DESC) {
        this.TXN_TYPE_DESC = TXN_TYPE_DESC;
    }

    public String getTXN_TYPE_NAME() {
        return TXN_TYPE_NAME;
    }

    public void setTXN_TYPE_NAME(String TXN_TYPE_NAME) {
        this.TXN_TYPE_NAME = TXN_TYPE_NAME;
    }

    public String getSCOPE() {
        return SCOPE;
    }

    public void setSCOPE(String SCOPE) {
        this.SCOPE = SCOPE;
    }

    public String getINSTRUMENT() {
        return INSTRUMENT;
    }

    public void setINSTRUMENT(String INSTRUMENT) {
        this.INSTRUMENT = INSTRUMENT;
    }

    public String getREGION() {
        return REGION;
    }

    public void setREGION(String REGION) {
        this.REGION = REGION;
    }

    public String getINTERNAL_RISK() {
        return INTERNAL_RISK;
    }

    public void setINTERNAL_RISK(String INTERNAL_RISK) {
        this.INTERNAL_RISK = INTERNAL_RISK;
    }

    public String getCREDIT_DEBIT_CODE() {
        return CREDIT_DEBIT_CODE;
    }

    public void setCREDIT_DEBIT_CODE(String CREDIT_DEBIT_CODE) {
        this.CREDIT_DEBIT_CODE = CREDIT_DEBIT_CODE;
    }
}