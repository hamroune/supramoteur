package com.bnpparibas.commons.models.data;

import lombok.*;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Customer {

    private Timestamp RunTimestamp;
    private String CustomerSourceU;
    private String OrgUnitCode;
    private String CustomerSourceR;
    private String PersonTitle;
    private String FirstName;
    private String MiddleNames;
    private String LastName;
    private String Suffix;
    private String CustomerName;
    private String CompanyName;
    private String CompanyForm;
    private String RegisteredNumbe;
    private String IncorporationDat;
    private String IncorporationCou;
    private String BusinessType;
    //Problem dans le exel fourni
    private String BusinessSegment1;
    private String BusinessSegment2;
    private String Initials;
    private String DateOfBirth;
    private String NameOfBirth;
    private String Address;
    private String Zone;
    private String City;
    private String PostalCode;
    private String CountryOfResiden;
    private String CountryOfOrigin;
    private String NationalityCode;
    private String PlaceofBirth;
    private String GenderCode;
    private String PrimeBranchId;
    private String RelationshipMgrI;
    private String EmployeeFlag;
    private String EmployeeNumber;
    private String MaritalStatus;
    private String Occupation;
    private String EmploymentStatu;
    private String AcquisitionDate;
    private String CancelledDate;
    private String CustomerTypeCo;
    private String CustomerStatusC;
    //Problem dans le exel fourni
    private String CustomerSegmen1;
    private String CustomerSegmen2;
    private String CustomerSegmen3;
    private String ResidenceFlag;
    private String SpecialAttentionFl;
    private String DeceasedFlag;
    private String DormantOverride;
    private String RiskScore;
    private String BankruptFlag;
    private String CompensationReq;
    private String CustomerComplai;
    private String EndRelationshipFl;
    private String MerchantNumber;
    private String FaceToFaceFlag;
    private String Channel;
    private Integer Age;
    private String NearBorderFlag;
    private String IntendedProduct;
    private String SourceOfFunds;
    private String ComplexStructure;
    private String ExpectedAnnualT;
    private Integer TradingDuration;
    private BigDecimal BalanceSheetTota;
    private String VatNumber;
    private String BrokerCode;
    private String BlackListedFlag;
    private String DomainCode;
    private String Comments;
    private String PepFlag;
    private Integer WireInNumber;
    private Integer WireOutNumber;
    private BigDecimal WireInVolume;
    private BigDecimal WireOutVolume;
    private BigDecimal CashInVolume;
    private BigDecimal CashOutVolume;
    private BigDecimal CheckInVolume;
    private BigDecimal CheckOutVolume;
    private BigDecimal OverallScoreAdju;
    private String TaxNumber;
    private String TaxNumberIssue;
    private String CustomerCategor;
    private String OwnAffiliateFlag;
    private String MarketingService;
    private String SanctionedFlag;
    private String PepType;
    private String PepRelatedFlag;
    private Timestamp AddressValidFrom;
    private Timestamp AddressValidTo;
    private String Email;
    private Timestamp EmailValidFrom;
    private Timestamp EmailValidTo;
    private String CountryCode;
    private String AreaCode;
    private String PhoneNumber;
    private String PhoneExtension;
    private Timestamp PhoneValidFrom;
    private Timestamp PhoneValidTo;
    private String AlternateName;
    private String TaxNumberType;
    //Problem dans le exel fourni
    private String BusinessClassificaC;
    private String BusinessClassificaSY;
    private String BnppFluxTransfro;
    private String BnppBanquePrive;
    private String BnppGestFortune;
    private String BnppInterditBqGe;
    private String BnppInterditBq;
    private String BnppInterditBqJu;
    private String BnppEntreeAReg;
    private String BnppCedantForfai;
    private String BnppClientASurve;
    private String BnppCptSurveilGe;
    private String BnppOuvertCptIm;
    private String BnppTypeEngage;
    private String BnppIndiceCourri;
    private String BnppTypeCreanc;
    private String BnppReqJudBla;
    private String BnppTrancheFMC;
    private String BnppDateIncident;
    private Integer BnppNbEmployes;

}
