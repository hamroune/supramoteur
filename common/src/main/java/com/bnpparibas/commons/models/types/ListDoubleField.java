package com.bnpparibas.commons.models.types;

import com.bnpparibas.commons.models.types.generic.Field;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;


public class ListDoubleField extends Field {
    List<Double> value = new ArrayList<>();

    public ListDoubleField(){}

    public List<Double> getValue() {
        return value;
    }

    public void setValue(List<Double> value) {
        this.value = value;
    }
}