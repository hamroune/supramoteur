package com.bnpparibas.commons.models.processed;

import com.bnpparibas.commons.models.raw.RModifiedTransaction;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;
import java.sql.Timestamp;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RRawEnrichedTransaction extends RModifiedTransaction {

    //Customer

    //private Timestamp RUN_TIMESTAMP  ;
    //private String CUSTOMER_SOURCE_UNIQUE_I ;
    //private String ORGUNIT_CODE ;
    //private String CUSTOMER_SOURCE_UNIQUE_I ;
    private String ORGANISATION ;
    private String CUSTOMER_SOURCE_REF_ID ;
    private String PERSON_TITLE ;
    private String FIRST_NAME ;
    private String MIDDLE_NAMES ;
    private String LAST_NAME ;
    private String SUFFIX ;
    private String CUSTOMER_NAME ;
    private String COMPANY_NAME ;
    private String COMPANY_FORM ;
    private String REGISTERED_NUMBER ;
    private Date INCORPORATION_DATE  ;
    private String INCORPORATION_COUNTRY_C ;
    private String BUSINESS_TYPE ;
    private String SEGMENT_APE ;
    private String BUSINESS_SEGMENT_2 ;
    private String INITIALS ;
    private Date DATE_OF_BIRTH  ;
    private String NAME_OF_BIRTH ;
    private String ADDRESS ;
    private String ZONE ;
    private String CITY ;
    private String POSTAL_CODE ;
    private String COUNTRY_OF_RESIDENCE ;
    private String COUNTRY_OF_ORIGIN ;
    private String NATIONALITY_CODE ;
    private String PLACE_OF_BIRTH ;
    private String GENDER_CODE ;
    private String BRANCH;
    private String RELATIONSHIP_MGR_ID ;
    private String EMPLOYEE_FLAG ;
    private String EMPLOYEE_NUMBER ;
    private String MARITAL_STATUS ;
    private String OCCUPATION ;
    private String EMPLOYMENT_STATUS ;
    private Date CUSTOMER_AGE;
    private Date CANCELLED_DATE  ;
    private String CUSTOMER_TYPE_CODE ;
    private String CUSTOMER_STATUS_CODE ;
    private String SEGMENT_KYC ;
    private String CUSTOMER_SEGMENT_2 ;
    private String CUSTOMER_SEGMENT_3 ;
    private String RESIDENCE_FLAG ;
    private String SPECIAL_ATTENTION_FLAG ;
    private String DECEASED_FLAG ;
    private Date DORMANT_OVERRIDE_DATE  ;
    private Double RISK_SCORE  ;
    private String SEGMENT_BLA ;
    private String COMPENSATION_REQD_FLAG ;
    private String CUSTOMER_COMPLAINT_FLAG ;
    private String END_RELATIONSHIP_FLAG ;
    private String MERCHANT_NUMBER ;
    private String FACE_TO_FACE_FLAG ;
    private String CHANNEL ;
    private Double AGE ;
    private String NEAR_BORDER_FLAG ;
    private String INTENDED_PRODUCT_USE ;
    private String SOURCE_OF_FUNDS ;
    private String COMPLEX_STRUCTURE ;
    private Double EXPECTED_ANNUAL_TURNOVE ;
    private Integer TRADING_DURATION ;
    private Double BALANCE_SHEET_TOTAL  ;
    private String VAT_NUMBER ;
    private String SEGMENT_AML;
    private String SEGMENT_SRA ;
    private String DOMAIN_CODE ;
    private String COMMENTS ;
    private String SEGMENT_PPE ;
    private Double WIRE_IN_NUMBER ;
    private Double WIRE_OUT_NUMBER ;
    private Double WIRE_IN_VOLUME ;
    private Double WIRE_OUT_VOLUME ;
    private Double CASH_IN_VOLUME ;
    private Double CASH_OUT_VOLUME ;
    private Double CHECK_IN_VOLUME ;
    private Double CHECK_OUT_VOLUME ;
    private Double OVERALL_SCORE_ADJUSTMENT ;
    private String TAX_NUMBER ;
    private String TAX_NUMBER_ISSUED_BY ;
    private String CUSTOMER_CATEGORY_CODE ;
    private String OWN_AFFILIATE_FLAG ;
    private String MARKETING_SERVICE_LEVEL ;
    private String SANCTIONED_FLAGINGESTED ;
    private String PEP_TYPEINGESTED ;
    private String RCA_FLAGINGESTED ;
    private Timestamp ADDRESS_VALID_FROM ;
    private Timestamp ADDRESS_VALID_TO;
    private String EMAIL ;
    private Timestamp EMAIL_VALID_FROM;
    private Timestamp EMAIL_VALID_TO;
    private String PHONE_COUNTRY_CODE ;
    private String PHONE_AREA_CODE ;
    private String PHONE_NUMBER ;
    private String PHONE_EXTENSION ;
    private Timestamp PHONE_VALID_FROM;
    private Timestamp PHONE_VALID_TO ;
    private String ALTERNATE_NAME ;
    private String TAX_NUMBER_TYPE ;
    private String BUSINESS_CLASSIFICATION_C ;
    private String BUSINESS_CLASSIFICATION_SY ;
    private String BNPP_FLUX_TRANSFRONTALIE ;
    private String BNPP_BANQUE_PRIVEE ;
    private String BNPP_GEST_FORTUNE ;
    private String BNPP_INTERDIT_BQ_GENE ;
    private String BNPP_INTERDIT_BQ ;
    private String BNPP_INTERDIT_JUDICIAIRE ;
    private String BNPP_ENTREE_REL_A_RÉGUL ;
    private String BNPP_CEDANT_FORFAIT_TOT ;
    private String BNPP_CLIENT_A_SURVEILLER ;
    private String BNPP_CPT_SURVEIL_GENE ;
    private String BNPP_OUVERT_CPT_IMPOSEE ;
    private String BNPP_TYPE_ENGAGEMENT ;
    private String BNPP_INDICE_COURRIER ;
    private String BNPP_TYPE_CREANCE_RISQ ;
    private String BNPP_REQ_JUD_BLA ;
    private String BNPP_TRANCHE_FMC ;
    private Date BNPP_DATE_INCIDENT;
    private Double BNPP_NB_EMPLOYES;


    //Country
    //private String COUNTRY_CODE ;
    //private String COUNTRY_CODE ;
//    private String COUNTRY_NAME ;
//    private String ISO_CODE ;
//    private String CROOK_COUNTRY_FLAG ;
//    private String TAX_HAVEN_FLAG ;
//    private String HIGHRISK_FLAG ;
//    private String FATF_FLAG ;
//    private String NARCOTIC_FLAG ;
//    private String OFAC_FLAG ;
//    private String SUSPICIOUS_FLAG ;
//    private String EUROPE_FLAG ;
//    private String TERRORISTHAVEN_FLAG ;
//    private String CONTINENT_NAME ;
//    private String REGION_CODE ;
//    private String NO_EXTERNAL_ACCESS ;
//    private Double FRDDC_RISK  ;
//    private Double PF_RISK  ;
//    private String AMLCB_RISK_FLAG ;
//    private Double PFCD_RISK  ;

    //BusinessSegmentation
//    private String CODE_APE_ID;
//    // private String business_segment_1_value; jointure
//    private String data_label_name;
//    private String CUSTOMER_CASH_INTENSIVE;

    //added columns
//    private String CROSSBORDER_FLAG;
//    private String INSTRUMENT_CROSSBORDER;
//    private String COUNTER_PARTY_BANK_COUNTRY_CODE;
//    private String TXN_ELIGIBLE_FOR_MARKET;
//    private String CASH_INTENSIVE_FLAG;
}
