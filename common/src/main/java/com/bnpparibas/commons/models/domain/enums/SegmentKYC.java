package com.bnpparibas.commons.models.domain.enums;

public enum SegmentKYC {
    M10,
    M01,
    P01,
    M06,
    M12,
    MM,
    M03,
    M02,
    M00,
    M11,
    M05,
    M04,
    M08,
    M07,
    PB;
}
