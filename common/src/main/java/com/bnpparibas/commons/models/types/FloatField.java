package com.bnpparibas.commons.models.types;

import com.bnpparibas.commons.models.types.generic.Field;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


public class FloatField extends Field{
    Float value;

    public FloatField(){}

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }
}