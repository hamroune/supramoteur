package com.bnpparibas.commons.models.raw;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


public class BusinessSegmentation {
    private String CODE_APE_ID;
    private String business_segment_1_value;
    private String data_label_name;
    private String CUSTOMER_CASH_INTENSIVE;

    public BusinessSegmentation(){}

    public String getCODE_APE_ID() {
        return CODE_APE_ID;
    }

    public void setCODE_APE_ID(String CODE_APE_ID) {
        this.CODE_APE_ID = CODE_APE_ID;
    }

    public String getBusiness_segment_1_value() {
        return business_segment_1_value;
    }

    public void setBusiness_segment_1_value(String business_segment_1_value) {
        this.business_segment_1_value = business_segment_1_value;
    }

    public String getData_label_name() {
        return data_label_name;
    }

    public void setData_label_name(String data_label_name) {
        this.data_label_name = data_label_name;
    }

    public String getCUSTOMER_CASH_INTENSIVE() {
        return CUSTOMER_CASH_INTENSIVE;
    }

    public void setCUSTOMER_CASH_INTENSIVE(String CUSTOMER_CASH_INTENSIVE) {
        this.CUSTOMER_CASH_INTENSIVE = CUSTOMER_CASH_INTENSIVE;
    }
}
