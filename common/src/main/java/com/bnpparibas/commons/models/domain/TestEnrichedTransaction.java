package com.bnpparibas.commons.models.domain;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class TestEnrichedTransaction implements Cloneable {

    private String client;
    private String transactionId;
    private String transactionDate;
    private String amount;
    private String week;
    private String month;
    private String transactionType;
    private String Direction;
    private String Contre_partie;
    private String country_risk;

}
