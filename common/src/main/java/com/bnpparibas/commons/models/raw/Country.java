package com.bnpparibas.commons.models.raw;

import lombok.*;


public class Country {


    private String COUNTRY_CODE ;
    private String COUNTRY_NAME ;
    private String ISO_CODE ;
    private String CROOK_COUNTRY_FLAG ;
    private String TAX_HAVEN_FLAG ;
    private String HIGHRISK_FLAG ;
    private String FATF_FLAG ;
    private String NARCOTIC_FLAG ;
    private String OFAC_FLAG ;
    private String SUSPICIOUS_FLAG ;
    private String EUROPE_FLAG ;
    private String TERRORISTHAVEN_FLAG ;
    private String CONTINENT_NAME ;
    private String REGION_CODE ;
    private String NO_EXTERNAL_ACCESS ;
    private Double FRDDC_RISK  ;
    private Double PF_RISK  ;
    private String AMLCB_RISK_FLAG ;
    private Double PFCD_RISK  ;
   // private Integer day_part ;

    public Country(){}

    public String getCOUNTRY_CODE() {
        return COUNTRY_CODE;
    }

    public void setCOUNTRY_CODE(String COUNTRY_CODE) {
        this.COUNTRY_CODE = COUNTRY_CODE;
    }

    public String getCOUNTRY_NAME() {
        return COUNTRY_NAME;
    }

    public void setCOUNTRY_NAME(String COUNTRY_NAME) {
        this.COUNTRY_NAME = COUNTRY_NAME;
    }

    public String getISO_CODE() {
        return ISO_CODE;
    }

    public void setISO_CODE(String ISO_CODE) {
        this.ISO_CODE = ISO_CODE;
    }

    public String getCROOK_COUNTRY_FLAG() {
        return CROOK_COUNTRY_FLAG;
    }

    public void setCROOK_COUNTRY_FLAG(String CROOK_COUNTRY_FLAG) {
        this.CROOK_COUNTRY_FLAG = CROOK_COUNTRY_FLAG;
    }

    public String getTAX_HAVEN_FLAG() {
        return TAX_HAVEN_FLAG;
    }

    public void setTAX_HAVEN_FLAG(String TAX_HAVEN_FLAG) {
        this.TAX_HAVEN_FLAG = TAX_HAVEN_FLAG;
    }

    public String getHIGHRISK_FLAG() {
        return HIGHRISK_FLAG;
    }

    public void setHIGHRISK_FLAG(String HIGHRISK_FLAG) {
        this.HIGHRISK_FLAG = HIGHRISK_FLAG;
    }

    public String getFATF_FLAG() {
        return FATF_FLAG;
    }

    public void setFATF_FLAG(String FATF_FLAG) {
        this.FATF_FLAG = FATF_FLAG;
    }

    public String getNARCOTIC_FLAG() {
        return NARCOTIC_FLAG;
    }

    public void setNARCOTIC_FLAG(String NARCOTIC_FLAG) {
        this.NARCOTIC_FLAG = NARCOTIC_FLAG;
    }

    public String getOFAC_FLAG() {
        return OFAC_FLAG;
    }

    public void setOFAC_FLAG(String OFAC_FLAG) {
        this.OFAC_FLAG = OFAC_FLAG;
    }

    public String getSUSPICIOUS_FLAG() {
        return SUSPICIOUS_FLAG;
    }

    public void setSUSPICIOUS_FLAG(String SUSPICIOUS_FLAG) {
        this.SUSPICIOUS_FLAG = SUSPICIOUS_FLAG;
    }

    public String getEUROPE_FLAG() {
        return EUROPE_FLAG;
    }

    public void setEUROPE_FLAG(String EUROPE_FLAG) {
        this.EUROPE_FLAG = EUROPE_FLAG;
    }

    public String getTERRORISTHAVEN_FLAG() {
        return TERRORISTHAVEN_FLAG;
    }

    public void setTERRORISTHAVEN_FLAG(String TERRORISTHAVEN_FLAG) {
        this.TERRORISTHAVEN_FLAG = TERRORISTHAVEN_FLAG;
    }

    public String getCONTINENT_NAME() {
        return CONTINENT_NAME;
    }

    public void setCONTINENT_NAME(String CONTINENT_NAME) {
        this.CONTINENT_NAME = CONTINENT_NAME;
    }

    public String getREGION_CODE() {
        return REGION_CODE;
    }

    public void setREGION_CODE(String REGION_CODE) {
        this.REGION_CODE = REGION_CODE;
    }

    public String getNO_EXTERNAL_ACCESS() {
        return NO_EXTERNAL_ACCESS;
    }

    public void setNO_EXTERNAL_ACCESS(String NO_EXTERNAL_ACCESS) {
        this.NO_EXTERNAL_ACCESS = NO_EXTERNAL_ACCESS;
    }

    public Double getFRDDC_RISK() {
        return FRDDC_RISK;
    }

    public void setFRDDC_RISK(Double FRDDC_RISK) {
        this.FRDDC_RISK = FRDDC_RISK;
    }

    public Double getPF_RISK() {
        return PF_RISK;
    }

    public void setPF_RISK(Double PF_RISK) {
        this.PF_RISK = PF_RISK;
    }

    public String getAMLCB_RISK_FLAG() {
        return AMLCB_RISK_FLAG;
    }

    public void setAMLCB_RISK_FLAG(String AMLCB_RISK_FLAG) {
        this.AMLCB_RISK_FLAG = AMLCB_RISK_FLAG;
    }

    public Double getPFCD_RISK() {
        return PFCD_RISK;
    }

    public void setPFCD_RISK(Double PFCD_RISK) {
        this.PFCD_RISK = PFCD_RISK;
    }
}
