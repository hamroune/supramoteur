package com.bnpparibas.commons.models.types;

import com.bnpparibas.commons.models.types.generic.Field;
import lombok.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class ListStringField  extends Field{
    private List<String> value=new ArrayList<>();

    public ListStringField (){}

    public List<String> getValue() {
        return value;
    }

    public void setValue(List<String> value) {
        this.value = value;
    }
}