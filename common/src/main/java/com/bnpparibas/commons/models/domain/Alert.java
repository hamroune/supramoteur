package com.bnpparibas.commons.models.domain;

import lombok.*;

import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Alert {
    private Boolean hit;
    private String justificationMessage;
    private List<String> violations;
}
