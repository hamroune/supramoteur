package com.bnpparibas.commons.models.domain;

import com.bnpparibas.commons.models.processed.RawEnrichedCBTransaction;
import com.bnpparibas.commons.models.processed.RawEnrichedTransaction;
import com.bnpparibas.commons.models.types.generic.Field;
import lombok.*;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
public class EnrichedTransactionWithAggregs extends RawEnrichedTransaction {
    private List<Field> aggregs = new ArrayList<>();



}
