package com.bnpparibas.commons.models.data;

import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class TransactionType {

    private String TxnTypeCode;
    private String TxnTypeDesc;
    private String TxnTypeName;
    private String Scope;
    private String Instrument;
    private String Region;
    private String InternalRisk;

}
