package com.bnpparibas.commons.models.domain;

import com.bnpparibas.commons.models.domain.enums.AlertLevel;
import lombok.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class Scenario implements Serializable {

    private String description;
    private String scenarioName;
    private String orderBy ;

    @Builder.Default
    private AlertLevel alertLevel = AlertLevel.Client; //Client Par defaut

    @Builder.Default
    private List<String> partitionByList = new ArrayList<>();
    @Builder.Default
    private List<Scope> scopes = new ArrayList<>();
    @Builder.Default
    private List<Aggregation> aggregationList = new ArrayList<>();


}