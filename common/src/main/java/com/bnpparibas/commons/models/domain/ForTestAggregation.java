package com.bnpparibas.commons.models.domain;


import lombok.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ForTestAggregation implements Serializable {
    private String aggFunction;
    private String orderBy;

    private String aggTimePeriod;
    private String aggTimePeriodSize;
    private String aggTimePeriodSelected;


    private String aggMetric;
    private String aggColName;

    private String aggPartitionBy;
    private String aggregationScopes ;



    //USED FOR TESTS ONLY !
    //TODO: Find better way to get Scopes from BDD
    public List<Scope> convertToAggScopes(){
        return !Objects.isNull(this.aggregationScopes) ?
                Arrays.asList(this.aggregationScopes.split("and")).stream().map(element -> {
                    Scope s = new Scope();
                    s.setScope(element);
                    return s;
                }).collect(Collectors.toList())
                :
                new ArrayList<>()
                ;
    }

}
