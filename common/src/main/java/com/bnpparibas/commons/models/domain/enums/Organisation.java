package com.bnpparibas.commons.models.domain.enums;

public enum Organisation {
    CORP,
    PROXI, //parti, voir quand meme le KyC
    G100, //parti
    GF, //Parti
    MC;
}