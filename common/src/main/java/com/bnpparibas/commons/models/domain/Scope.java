package com.bnpparibas.commons.models.domain;


import lombok.*;

import java.io.Serializable;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class Scope implements Serializable {
    private String scope;

}
