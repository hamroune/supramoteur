package com.bnpparibas.commons.models.domain.enums;


import java.util.HashMap;
import java.util.Map;

public enum AlertLevel {
    Client("CUSTOMER"),
    Compte("ACCOUNT"),// a voir
    Transaction("SRC_TXN_UNIQUE_ID");


    private String value;
    private static final Map<String, AlertLevel> lookup = new HashMap();

    static
    {
        for(AlertLevel alert : AlertLevel.values())
        {
            lookup.put(alert.getValue(), alert);
        }
    }
    AlertLevel(String value){
        this.value=value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static AlertLevel lookup(String value){
        try{
            return lookup.get(value);
        }catch (Exception e){
            return AlertLevel.Client;
        }
    }
}
