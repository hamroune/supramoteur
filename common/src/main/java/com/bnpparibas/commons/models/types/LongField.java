package com.bnpparibas.commons.models.types;

import com.bnpparibas.commons.models.types.generic.Field;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


public class LongField extends Field{
    Long value;

    public LongField(){}

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }
}