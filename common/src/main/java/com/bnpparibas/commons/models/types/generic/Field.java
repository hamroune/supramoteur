package com.bnpparibas.commons.models.types.generic;


import java.io.Serializable;
import java.util.Map;


public abstract class Field implements Serializable {

    private String name;
    private String dataType;
    //private T value;
    private String label = "";
    private Map<String, String> metadata;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

//    public T getValue() {
//        return value;
//    }
//
//    public void setValue(T value) {
//        this.value = value;
//    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Map<String, String> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String, String> metadata) {
        this.metadata = metadata;
    }

    public static String NAME="name";
    public static String DATATYPE="dataType";
    public static String VALUE="value";
    public static String LABEL="label";
    public static String METADATA="metadata";

}