package com.bnpparibas.commons.models.types;

import com.bnpparibas.commons.models.types.generic.Field;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


public class DoubleField extends Field {
    Double value;

    public DoubleField(){}

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }
}