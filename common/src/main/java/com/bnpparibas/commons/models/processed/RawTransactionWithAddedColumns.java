package com.bnpparibas.commons.models.processed;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RawTransactionWithAddedColumns extends RawTransactionWithTransactionType {


    //
    private String CUSTOMER;
    private String COUNTRY_CODE;
    private String COUNTER_PARTY;
    private String ACCOUNT;
    private String COUNTRY_COUNTER_PARTY;
    private String VIR_INTERNATIONAL_FLAG;
    private String ACOUNT_COUNTER_PARTY;



}
