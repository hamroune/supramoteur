package com.bnpparibas.commons.models.data;

import lombok.*;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;


@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class CbTransaction {

    private Timestamp RunTimestamp;
    private String SrcTxnUniqueId;
    private String SrcTxnNum;
    private String TellerId;
    private String TxnSrcTypeCode;
    private String CurrencyCodeOrig;
    private String CurrencyCodeBase;
    private String OriginationDate;
    private Date PostingDate;
    private Date ValueDate;
    private Timestamp SystemTimestamp;
    private Timestamp LocalTimestamp;
    private String TxnChannelCode;
    private BigDecimal TxnAmountOrig;     //Decimal
    private BigDecimal TxnAmountBase;     //Decimal
    private String TransRefDesc;
    private String ClientDePassage;
    private String TxnStatusCode;
    private String SrcSystemCode;
    private String OrgUnitCode;
    private String CardSourceRefId;
    private String OriginCPartySrcUniqueI;
    private String OriginCPartySrcRefId;
    private String OriginSubCustSrcUnique;
    private String OriginSubCustSrcRefId;
    private String OriginCpartyAccountId;
    private String OriginBIC;
    private String OriginAccountNumber;
    private String OriginName;
    private String OriginBankName;
    private String OriginCountryCode;
    private String OriginAddress;
    private String OriginCity;
    private String OriginZone;
    private String OriginPostalCode;
    private String OriginBankCountryCode;
    private String OriginBankAddress;
    private String BeneCPartySrcUniqueId;
    private String BeneCPartySrcRefId;
    private String BeneSubCustSrcUniqueI;
    private String BeneSubCustSrcRefId;
    private String BeneCpartyAccountId;
    private String BeneBIC;
    private String BeneAccountNumber;
    private String BeneName;
    private String BeneBankName;
    private String BeneCountryCode;
    private String BeneAddress;
    private String BeneCity;
    private String BeneZone;
    private String BenePostalCode;
    private String BeneBankCountryCode;
    private String BeneBankAddress;
    private String DbRespCustSrcUniqueId;
    private String DbRespCustSrcRefId;
    private String DbRespAccountSrcUniq;
    private String DbRespAccountSrcRefId;
    private String DbRespBic;
    private String DbRespAccountNumber;
    private String DbRespOrgUnitCode;
    private String CrRespCustSrcUniqueId;
    private String CrRespCustSrcRefId;
    private String CrRespAccountSrcUniqu;
    private String CrRespAccountSrcRefId;
    private String CrRespBic;
    private String CrRespAccountNumber;
    private String CrRespOrgUnitCode;
    private String SendingBankName;
    private String SendingBankBic;
    private String SendingBankAccountNu;
    private String SendingBankCountryCo;
    private String SendingBankAddress;
    private String RecvBankName;
    private String RecvBankBic;
    private String RecvBankAccountNumb;
    private String RecvBankCountryCode;
    private String RecvBankAddress;
    private String IntBankName;
    private String IntBankBic;
    private String IntBankAccountNumber;
    private String IntBankCountryCode;
    private String IntBankAddress;
    private String OrderInstBankName;
    private String OrderInstBankBic;
    private String OrderInstAccountNumb;
    private String OrderInstCountryCode;
    private String OrderInstAddress;
    private String SenderCorrBankName;
    private String SenderCorrBankBic;
    private String SenderCorrAccountNum;
    private String SenderCorrCountryCode;
    private String SenderCorrAddress;
    private String RecvCorrBankName;
    private String RecvCorrBankBic;
    private String RecvCorrAccountNumbe;
    private String RecvCorrCountryCode;
    private String RecvCorrAddress;


}
