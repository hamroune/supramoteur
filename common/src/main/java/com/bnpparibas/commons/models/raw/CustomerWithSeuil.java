package com.bnpparibas.commons.models.raw;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CustomerWithSeuil extends Customer {
    private String CUSTOMER;
    private Integer profondeur;

}
