package com.bnpparibas.commons.models.data;

import lombok.*;


@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Country {

    private String CountryCode;
    private String CountryName;
    private String IsoCode;
    private String CrookCountryFlag;
    private String TaxHavenFlag;
    private String HighRiskFlag;
    private String FatfFlag;
    private String NarcoticFlag;
    private String OfacFlag;
    private String SuspiciousFlag;
    private String EuropeFlag;
    private String TerroristhavenFla;
    private String ContinentName;
    private String RegionCode;
    private String NoExternalAccess;
    private int FrddcRisk;
    private int PfRisk;
    private String AMLCBRiskFlag;
    private int PFCDRisk;


}
