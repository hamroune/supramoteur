package com.bnpparibas.commons.models.processed;

import com.bnpparibas.commons.models.raw.Transaction;
import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class RawTransactionWithTransactionType extends Transaction {

    //private String TXN_TYPE_CODE;
    private String TXN_TYPE_DESC;
    private String TXN_TYPE_NAME;
    private String SCOPE ;
    private String INSTRUMENT;
    private String REGION;
    private String INTERNAL_RISK;
    private String CREDIT_DEBIT_CODE;
//    private String ID_DATE_CHGT_INS;
//    private String ID_DATE_CHGT_MAJ;
//    private String UPDATE_DATE;


}
