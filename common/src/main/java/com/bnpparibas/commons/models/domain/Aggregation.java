package com.bnpparibas.commons.models.domain;

import lombok.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class Aggregation implements Serializable{
    private String aggFunction;
    private String orderBy;

    private String aggTimePeriod;
    private String aggTimePeriodSize;
    private String aggTimePeriodSelected;


    private String aggMetric;
    private String aggColName;

    @Builder.Default
    private List<Scope> aggregationScopes = new ArrayList<>();
    private String aggPartitionBy;

//
//    //USED FOR TESTS ONLY !
//    //TODO: Find better way to get Scopes from BDD
//    public List<Scope> getAggScopes(){
//        return !Objects.isNull(this.aggregationScopes) ?
//                Arrays.asList(this.aggregationScopes.split("and")).stream().map(element -> {
//                    Scope s = new Scope();
//                    s.setScope(element);
//                    return s;
//                }).collect(Collectors.toList())
//                :
//                new ArrayList<>()
//                ;
//    }

}
