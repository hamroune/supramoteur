package com.bnpparibas.commons.models.types;

import lombok.Getter;

public enum Datatypes {

    STRING_TYPE("String"),
    BIGDECIMAL_TYPE("BigDecimal"),
    INTERGER_TYPE("Integer"),
    DATE_TYPE("Date");

    private String dataType ;

    private Datatypes(String dataType){ this.dataType = dataType;}

    public String getDataType() {
        return dataType;
    }
}