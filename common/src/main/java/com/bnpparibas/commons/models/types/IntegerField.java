package com.bnpparibas.commons.models.types;

import com.bnpparibas.commons.models.types.generic.Field;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


public class IntegerField extends Field{
    Integer value;
    public IntegerField(){}

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}