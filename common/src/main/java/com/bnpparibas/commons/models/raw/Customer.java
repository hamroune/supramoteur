package com.bnpparibas.commons.models.raw;

import lombok.*;

import java.sql.Date;
import java.sql.Timestamp;

public class Customer {
    public Customer(){}

    //private Timestamp RUN_TIMESTAMP  ;
    private String CUSTOMER_SOURCE_UNIQUE_I ;
    private String ORGANISATION ;
    private String CUSTOMER_SOURCE_REF_ID ;
    private String PERSON_TITLE ;
    private String FIRST_NAME ;
    private String MIDDLE_NAMES ;
    private String LAST_NAME ;
    private String SUFFIX ;
    private String CUSTOMER_NAME ;
    private String COMPANY_NAME ;
    private String COMPANY_FORM ;
    private String REGISTERED_NUMBER ;
    private Date INCORPORATION_DATE  ;
    private String INCORPORATION_COUNTRY_C ;
    private String BUSINESS_TYPE ;
    private String SEGMENT_APE ;
    private String BUSINESS_SEGMENT_2 ;
    private String INITIALS ;
    private Date DATE_OF_BIRTH  ;
    private String NAME_OF_BIRTH ;
    private String ADDRESS ;
    private String ZONE ;
    private String CITY ;
    private String POSTAL_CODE ;
    private String COUNTRY_OF_RESIDENCE ;
    private String COUNTRY_OF_ORIGIN ;
    private String NATIONALITY_CODE ;
    private String PLACE_OF_BIRTH ;
    private String GENDER_CODE ;
    private String BRANCH;
    private String RELATIONSHIP_MGR_ID ;
    private String EMPLOYEE_FLAG ;
    private String EMPLOYEE_NUMBER ;
    private String MARITAL_STATUS ;
    private String OCCUPATION ;
    private String EMPLOYMENT_STATUS ;
    private Date CUSTOMER_AGE;
    private Date CANCELLED_DATE  ;
    private String CUSTOMER_TYPE_CODE ;
    private String CUSTOMER_STATUS_CODE ;
    private String SEGMENT_KYC ;
    private String CUSTOMER_SEGMENT_2 ;
    private String CUSTOMER_SEGMENT_3 ;
    private String RESIDENCE_FLAG ;
    private String SPECIAL_ATTENTION_FLAG ;
    private String DECEASED_FLAG ;
    private Date DORMANT_OVERRIDE_DATE  ;
    private Double RISK_SCORE  ;
    private String SEGMENT_BLA ;
    private String COMPENSATION_REQD_FLAG ;
    private String CUSTOMER_COMPLAINT_FLAG ;
    private String END_RELATIONSHIP_FLAG ;
    private String MERCHANT_NUMBER ;
    private String FACE_TO_FACE_FLAG ;
    private String CHANNEL ;
    private Double AGE ;
    private String NEAR_BORDER_FLAG ;
    private String INTENDED_PRODUCT_USE ;
    private String SOURCE_OF_FUNDS ;
    private String COMPLEX_STRUCTURE ;
    private Double EXPECTED_ANNUAL_TURNOVE ;
    private Integer TRADING_DURATION ;
    private Double BALANCE_SHEET_TOTAL  ;
    private String VAT_NUMBER ;
    private String SEGMENT_AML;
    private String SEGMENT_SRA ;
    private String DOMAIN_CODE ;
    private String COMMENTS ;
    private String SEGMENT_PPE ;
    private Double WIRE_IN_NUMBER ;
    private Double WIRE_OUT_NUMBER ;
    private Double WIRE_IN_VOLUME ;
    private Double WIRE_OUT_VOLUME ;
    private Double CASH_IN_VOLUME ;
    private Double CASH_OUT_VOLUME ;
    private Double CHECK_IN_VOLUME ;
    private Double CHECK_OUT_VOLUME ;
    private Double OVERALL_SCORE_ADJUSTMENT ;
    private String TAX_NUMBER ;
    private String TAX_NUMBER_ISSUED_BY ;
    private String CUSTOMER_CATEGORY_CODE ;
    private String OWN_AFFILIATE_FLAG ;
    private String MARKETING_SERVICE_LEVEL ;
    private String SANCTIONED_FLAGINGESTED ;
    private String PEP_TYPEINGESTED ;
    private String RCA_FLAGINGESTED ;
    private Timestamp ADDRESS_VALID_FROM ;
    private Timestamp ADDRESS_VALID_TO;
    private String EMAIL ;
    private Timestamp EMAIL_VALID_FROM;
    private Timestamp EMAIL_VALID_TO;
    private String PHONE_COUNTRY_CODE ;
    private String PHONE_AREA_CODE ;
    private String PHONE_NUMBER ;
    private String PHONE_EXTENSION ;
    private Timestamp PHONE_VALID_FROM;
    private Timestamp PHONE_VALID_TO ;
    private String ALTERNATE_NAME ;
    private String TAX_NUMBER_TYPE ;
    private String BUSINESS_CLASSIFICATION_C ;
    private String BUSINESS_CLASSIFICATION_SY ;
    private String BNPP_FLUX_TRANSFRONTALIE ;
    private String BNPP_BANQUE_PRIVEE ;
    private String BNPP_GEST_FORTUNE ;
    private String BNPP_INTERDIT_BQ_GENE ;
    private String BNPP_INTERDIT_BQ ;
    private String BNPP_INTERDIT_JUDICIAIRE ;
    private String BNPP_ENTREE_REL_A_RÉGUL ;
    private String BNPP_CEDANT_FORFAIT_TOT ;
    private String BNPP_CLIENT_A_SURVEILLER ;
    private String BNPP_CPT_SURVEIL_GENE ;
    private String BNPP_OUVERT_CPT_IMPOSEE ;
    private String BNPP_TYPE_ENGAGEMENT ;
    private String BNPP_INDICE_COURRIER ;
    private String BNPP_TYPE_CREANCE_RISQ ;
    private String BNPP_REQ_JUD_BLA ;
    private String BNPP_TRANCHE_FMC ;
    private Date BNPP_DATE_INCIDENT;
    private Double BNPP_NB_EMPLOYES;
   // private Integer day_part;


    public String getCUSTOMER_SOURCE_UNIQUE_I() {
        return CUSTOMER_SOURCE_UNIQUE_I;
    }

    public void setCUSTOMER_SOURCE_UNIQUE_I(String CUSTOMER_SOURCE_UNIQUE_I) {
        this.CUSTOMER_SOURCE_UNIQUE_I = CUSTOMER_SOURCE_UNIQUE_I;
    }

    public String getORGANISATION() {
        return ORGANISATION;
    }

    public void setORGANISATION(String ORGANISATION) {
        this.ORGANISATION = ORGANISATION;
    }

    public String getCUSTOMER_SOURCE_REF_ID() {
        return CUSTOMER_SOURCE_REF_ID;
    }

    public void setCUSTOMER_SOURCE_REF_ID(String CUSTOMER_SOURCE_REF_ID) {
        this.CUSTOMER_SOURCE_REF_ID = CUSTOMER_SOURCE_REF_ID;
    }

    public String getPERSON_TITLE() {
        return PERSON_TITLE;
    }

    public void setPERSON_TITLE(String PERSON_TITLE) {
        this.PERSON_TITLE = PERSON_TITLE;
    }

    public String getFIRST_NAME() {
        return FIRST_NAME;
    }

    public void setFIRST_NAME(String FIRST_NAME) {
        this.FIRST_NAME = FIRST_NAME;
    }

    public String getMIDDLE_NAMES() {
        return MIDDLE_NAMES;
    }

    public void setMIDDLE_NAMES(String MIDDLE_NAMES) {
        this.MIDDLE_NAMES = MIDDLE_NAMES;
    }

    public String getLAST_NAME() {
        return LAST_NAME;
    }

    public void setLAST_NAME(String LAST_NAME) {
        this.LAST_NAME = LAST_NAME;
    }

    public String getSUFFIX() {
        return SUFFIX;
    }

    public void setSUFFIX(String SUFFIX) {
        this.SUFFIX = SUFFIX;
    }

    public String getCUSTOMER_NAME() {
        return CUSTOMER_NAME;
    }

    public void setCUSTOMER_NAME(String CUSTOMER_NAME) {
        this.CUSTOMER_NAME = CUSTOMER_NAME;
    }

    public String getCOMPANY_NAME() {
        return COMPANY_NAME;
    }

    public void setCOMPANY_NAME(String COMPANY_NAME) {
        this.COMPANY_NAME = COMPANY_NAME;
    }

    public String getCOMPANY_FORM() {
        return COMPANY_FORM;
    }

    public void setCOMPANY_FORM(String COMPANY_FORM) {
        this.COMPANY_FORM = COMPANY_FORM;
    }

    public String getREGISTERED_NUMBER() {
        return REGISTERED_NUMBER;
    }

    public void setREGISTERED_NUMBER(String REGISTERED_NUMBER) {
        this.REGISTERED_NUMBER = REGISTERED_NUMBER;
    }

    public Date getINCORPORATION_DATE() {
        return INCORPORATION_DATE;
    }

    public void setINCORPORATION_DATE(Date INCORPORATION_DATE) {
        this.INCORPORATION_DATE = INCORPORATION_DATE;
    }

    public String getINCORPORATION_COUNTRY_C() {
        return INCORPORATION_COUNTRY_C;
    }

    public void setINCORPORATION_COUNTRY_C(String INCORPORATION_COUNTRY_C) {
        this.INCORPORATION_COUNTRY_C = INCORPORATION_COUNTRY_C;
    }

    public String getBUSINESS_TYPE() {
        return BUSINESS_TYPE;
    }

    public void setBUSINESS_TYPE(String BUSINESS_TYPE) {
        this.BUSINESS_TYPE = BUSINESS_TYPE;
    }

    public String getSEGMENT_APE() {
        return SEGMENT_APE;
    }

    public void setSEGMENT_APE(String SEGMENT_APE) {
        this.SEGMENT_APE = SEGMENT_APE;
    }

    public String getBUSINESS_SEGMENT_2() {
        return BUSINESS_SEGMENT_2;
    }

    public void setBUSINESS_SEGMENT_2(String BUSINESS_SEGMENT_2) {
        this.BUSINESS_SEGMENT_2 = BUSINESS_SEGMENT_2;
    }

    public String getINITIALS() {
        return INITIALS;
    }

    public void setINITIALS(String INITIALS) {
        this.INITIALS = INITIALS;
    }

    public Date getDATE_OF_BIRTH() {
        return DATE_OF_BIRTH;
    }

    public void setDATE_OF_BIRTH(Date DATE_OF_BIRTH) {
        this.DATE_OF_BIRTH = DATE_OF_BIRTH;
    }

    public String getNAME_OF_BIRTH() {
        return NAME_OF_BIRTH;
    }

    public void setNAME_OF_BIRTH(String NAME_OF_BIRTH) {
        this.NAME_OF_BIRTH = NAME_OF_BIRTH;
    }

    public String getADDRESS() {
        return ADDRESS;
    }

    public void setADDRESS(String ADDRESS) {
        this.ADDRESS = ADDRESS;
    }

    public String getZONE() {
        return ZONE;
    }

    public void setZONE(String ZONE) {
        this.ZONE = ZONE;
    }

    public String getCITY() {
        return CITY;
    }

    public void setCITY(String CITY) {
        this.CITY = CITY;
    }

    public String getPOSTAL_CODE() {
        return POSTAL_CODE;
    }

    public void setPOSTAL_CODE(String POSTAL_CODE) {
        this.POSTAL_CODE = POSTAL_CODE;
    }

    public String getCOUNTRY_OF_RESIDENCE() {
        return COUNTRY_OF_RESIDENCE;
    }

    public void setCOUNTRY_OF_RESIDENCE(String COUNTRY_OF_RESIDENCE) {
        this.COUNTRY_OF_RESIDENCE = COUNTRY_OF_RESIDENCE;
    }

    public String getCOUNTRY_OF_ORIGIN() {
        return COUNTRY_OF_ORIGIN;
    }

    public void setCOUNTRY_OF_ORIGIN(String COUNTRY_OF_ORIGIN) {
        this.COUNTRY_OF_ORIGIN = COUNTRY_OF_ORIGIN;
    }

    public String getNATIONALITY_CODE() {
        return NATIONALITY_CODE;
    }

    public void setNATIONALITY_CODE(String NATIONALITY_CODE) {
        this.NATIONALITY_CODE = NATIONALITY_CODE;
    }

    public String getPLACE_OF_BIRTH() {
        return PLACE_OF_BIRTH;
    }

    public void setPLACE_OF_BIRTH(String PLACE_OF_BIRTH) {
        this.PLACE_OF_BIRTH = PLACE_OF_BIRTH;
    }

    public String getGENDER_CODE() {
        return GENDER_CODE;
    }

    public void setGENDER_CODE(String GENDER_CODE) {
        this.GENDER_CODE = GENDER_CODE;
    }

    public String getBRANCH() {
        return BRANCH;
    }

    public void setBRANCH(String BRANCH) {
        this.BRANCH = BRANCH;
    }

    public String getRELATIONSHIP_MGR_ID() {
        return RELATIONSHIP_MGR_ID;
    }

    public void setRELATIONSHIP_MGR_ID(String RELATIONSHIP_MGR_ID) {
        this.RELATIONSHIP_MGR_ID = RELATIONSHIP_MGR_ID;
    }

    public String getEMPLOYEE_FLAG() {
        return EMPLOYEE_FLAG;
    }

    public void setEMPLOYEE_FLAG(String EMPLOYEE_FLAG) {
        this.EMPLOYEE_FLAG = EMPLOYEE_FLAG;
    }

    public String getEMPLOYEE_NUMBER() {
        return EMPLOYEE_NUMBER;
    }

    public void setEMPLOYEE_NUMBER(String EMPLOYEE_NUMBER) {
        this.EMPLOYEE_NUMBER = EMPLOYEE_NUMBER;
    }

    public String getMARITAL_STATUS() {
        return MARITAL_STATUS;
    }

    public void setMARITAL_STATUS(String MARITAL_STATUS) {
        this.MARITAL_STATUS = MARITAL_STATUS;
    }

    public String getOCCUPATION() {
        return OCCUPATION;
    }

    public void setOCCUPATION(String OCCUPATION) {
        this.OCCUPATION = OCCUPATION;
    }

    public String getEMPLOYMENT_STATUS() {
        return EMPLOYMENT_STATUS;
    }

    public void setEMPLOYMENT_STATUS(String EMPLOYMENT_STATUS) {
        this.EMPLOYMENT_STATUS = EMPLOYMENT_STATUS;
    }

    public Date getCUSTOMER_AGE() {
        return CUSTOMER_AGE;
    }

    public void setCUSTOMER_AGE(Date CUSTOMER_AGE) {
        this.CUSTOMER_AGE = CUSTOMER_AGE;
    }

    public Date getCANCELLED_DATE() {
        return CANCELLED_DATE;
    }

    public void setCANCELLED_DATE(Date CANCELLED_DATE) {
        this.CANCELLED_DATE = CANCELLED_DATE;
    }

    public String getCUSTOMER_TYPE_CODE() {
        return CUSTOMER_TYPE_CODE;
    }

    public void setCUSTOMER_TYPE_CODE(String CUSTOMER_TYPE_CODE) {
        this.CUSTOMER_TYPE_CODE = CUSTOMER_TYPE_CODE;
    }

    public String getCUSTOMER_STATUS_CODE() {
        return CUSTOMER_STATUS_CODE;
    }

    public void setCUSTOMER_STATUS_CODE(String CUSTOMER_STATUS_CODE) {
        this.CUSTOMER_STATUS_CODE = CUSTOMER_STATUS_CODE;
    }

    public String getSEGMENT_KYC() {
        return SEGMENT_KYC;
    }

    public void setSEGMENT_KYC(String SEGMENT_KYC) {
        this.SEGMENT_KYC = SEGMENT_KYC;
    }

    public String getCUSTOMER_SEGMENT_2() {
        return CUSTOMER_SEGMENT_2;
    }

    public void setCUSTOMER_SEGMENT_2(String CUSTOMER_SEGMENT_2) {
        this.CUSTOMER_SEGMENT_2 = CUSTOMER_SEGMENT_2;
    }

    public String getCUSTOMER_SEGMENT_3() {
        return CUSTOMER_SEGMENT_3;
    }

    public void setCUSTOMER_SEGMENT_3(String CUSTOMER_SEGMENT_3) {
        this.CUSTOMER_SEGMENT_3 = CUSTOMER_SEGMENT_3;
    }

    public String getRESIDENCE_FLAG() {
        return RESIDENCE_FLAG;
    }

    public void setRESIDENCE_FLAG(String RESIDENCE_FLAG) {
        this.RESIDENCE_FLAG = RESIDENCE_FLAG;
    }

    public String getSPECIAL_ATTENTION_FLAG() {
        return SPECIAL_ATTENTION_FLAG;
    }

    public void setSPECIAL_ATTENTION_FLAG(String SPECIAL_ATTENTION_FLAG) {
        this.SPECIAL_ATTENTION_FLAG = SPECIAL_ATTENTION_FLAG;
    }

    public String getDECEASED_FLAG() {
        return DECEASED_FLAG;
    }

    public void setDECEASED_FLAG(String DECEASED_FLAG) {
        this.DECEASED_FLAG = DECEASED_FLAG;
    }

    public Date getDORMANT_OVERRIDE_DATE() {
        return DORMANT_OVERRIDE_DATE;
    }

    public void setDORMANT_OVERRIDE_DATE(Date DORMANT_OVERRIDE_DATE) {
        this.DORMANT_OVERRIDE_DATE = DORMANT_OVERRIDE_DATE;
    }

    public Double getRISK_SCORE() {
        return RISK_SCORE;
    }

    public void setRISK_SCORE(Double RISK_SCORE) {
        this.RISK_SCORE = RISK_SCORE;
    }

    public String getSEGMENT_BLA() {
        return SEGMENT_BLA;
    }

    public void setSEGMENT_BLA(String SEGMENT_BLA) {
        this.SEGMENT_BLA = SEGMENT_BLA;
    }

    public String getCOMPENSATION_REQD_FLAG() {
        return COMPENSATION_REQD_FLAG;
    }

    public void setCOMPENSATION_REQD_FLAG(String COMPENSATION_REQD_FLAG) {
        this.COMPENSATION_REQD_FLAG = COMPENSATION_REQD_FLAG;
    }

    public String getCUSTOMER_COMPLAINT_FLAG() {
        return CUSTOMER_COMPLAINT_FLAG;
    }

    public void setCUSTOMER_COMPLAINT_FLAG(String CUSTOMER_COMPLAINT_FLAG) {
        this.CUSTOMER_COMPLAINT_FLAG = CUSTOMER_COMPLAINT_FLAG;
    }

    public String getEND_RELATIONSHIP_FLAG() {
        return END_RELATIONSHIP_FLAG;
    }

    public void setEND_RELATIONSHIP_FLAG(String END_RELATIONSHIP_FLAG) {
        this.END_RELATIONSHIP_FLAG = END_RELATIONSHIP_FLAG;
    }

    public String getMERCHANT_NUMBER() {
        return MERCHANT_NUMBER;
    }

    public void setMERCHANT_NUMBER(String MERCHANT_NUMBER) {
        this.MERCHANT_NUMBER = MERCHANT_NUMBER;
    }

    public String getFACE_TO_FACE_FLAG() {
        return FACE_TO_FACE_FLAG;
    }

    public void setFACE_TO_FACE_FLAG(String FACE_TO_FACE_FLAG) {
        this.FACE_TO_FACE_FLAG = FACE_TO_FACE_FLAG;
    }

    public String getCHANNEL() {
        return CHANNEL;
    }

    public void setCHANNEL(String CHANNEL) {
        this.CHANNEL = CHANNEL;
    }

    public Double getAGE() {
        return AGE;
    }

    public void setAGE(Double AGE) {
        this.AGE = AGE;
    }

    public String getNEAR_BORDER_FLAG() {
        return NEAR_BORDER_FLAG;
    }

    public void setNEAR_BORDER_FLAG(String NEAR_BORDER_FLAG) {
        this.NEAR_BORDER_FLAG = NEAR_BORDER_FLAG;
    }

    public String getINTENDED_PRODUCT_USE() {
        return INTENDED_PRODUCT_USE;
    }

    public void setINTENDED_PRODUCT_USE(String INTENDED_PRODUCT_USE) {
        this.INTENDED_PRODUCT_USE = INTENDED_PRODUCT_USE;
    }

    public String getSOURCE_OF_FUNDS() {
        return SOURCE_OF_FUNDS;
    }

    public void setSOURCE_OF_FUNDS(String SOURCE_OF_FUNDS) {
        this.SOURCE_OF_FUNDS = SOURCE_OF_FUNDS;
    }

    public String getCOMPLEX_STRUCTURE() {
        return COMPLEX_STRUCTURE;
    }

    public void setCOMPLEX_STRUCTURE(String COMPLEX_STRUCTURE) {
        this.COMPLEX_STRUCTURE = COMPLEX_STRUCTURE;
    }

    public Double getEXPECTED_ANNUAL_TURNOVE() {
        return EXPECTED_ANNUAL_TURNOVE;
    }

    public void setEXPECTED_ANNUAL_TURNOVE(Double EXPECTED_ANNUAL_TURNOVE) {
        this.EXPECTED_ANNUAL_TURNOVE = EXPECTED_ANNUAL_TURNOVE;
    }

    public Integer getTRADING_DURATION() {
        return TRADING_DURATION;
    }

    public void setTRADING_DURATION(Integer TRADING_DURATION) {
        this.TRADING_DURATION = TRADING_DURATION;
    }

    public Double getBALANCE_SHEET_TOTAL() {
        return BALANCE_SHEET_TOTAL;
    }

    public void setBALANCE_SHEET_TOTAL(Double BALANCE_SHEET_TOTAL) {
        this.BALANCE_SHEET_TOTAL = BALANCE_SHEET_TOTAL;
    }

    public String getVAT_NUMBER() {
        return VAT_NUMBER;
    }

    public void setVAT_NUMBER(String VAT_NUMBER) {
        this.VAT_NUMBER = VAT_NUMBER;
    }

    public String getSEGMENT_AML() {
        return SEGMENT_AML;
    }

    public void setSEGMENT_AML(String SEGMENT_AML) {
        this.SEGMENT_AML = SEGMENT_AML;
    }

    public String getSEGMENT_SRA() {
        return SEGMENT_SRA;
    }

    public void setSEGMENT_SRA(String SEGMENT_SRA) {
        this.SEGMENT_SRA = SEGMENT_SRA;
    }

    public String getDOMAIN_CODE() {
        return DOMAIN_CODE;
    }

    public void setDOMAIN_CODE(String DOMAIN_CODE) {
        this.DOMAIN_CODE = DOMAIN_CODE;
    }

    public String getCOMMENTS() {
        return COMMENTS;
    }

    public void setCOMMENTS(String COMMENTS) {
        this.COMMENTS = COMMENTS;
    }

    public String getSEGMENT_PPE() {
        return SEGMENT_PPE;
    }

    public void setSEGMENT_PPE(String SEGMENT_PPE) {
        this.SEGMENT_PPE = SEGMENT_PPE;
    }

    public Double getWIRE_IN_NUMBER() {
        return WIRE_IN_NUMBER;
    }

    public void setWIRE_IN_NUMBER(Double WIRE_IN_NUMBER) {
        this.WIRE_IN_NUMBER = WIRE_IN_NUMBER;
    }

    public Double getWIRE_OUT_NUMBER() {
        return WIRE_OUT_NUMBER;
    }

    public void setWIRE_OUT_NUMBER(Double WIRE_OUT_NUMBER) {
        this.WIRE_OUT_NUMBER = WIRE_OUT_NUMBER;
    }

    public Double getWIRE_IN_VOLUME() {
        return WIRE_IN_VOLUME;
    }

    public void setWIRE_IN_VOLUME(Double WIRE_IN_VOLUME) {
        this.WIRE_IN_VOLUME = WIRE_IN_VOLUME;
    }

    public Double getWIRE_OUT_VOLUME() {
        return WIRE_OUT_VOLUME;
    }

    public void setWIRE_OUT_VOLUME(Double WIRE_OUT_VOLUME) {
        this.WIRE_OUT_VOLUME = WIRE_OUT_VOLUME;
    }

    public Double getCASH_IN_VOLUME() {
        return CASH_IN_VOLUME;
    }

    public void setCASH_IN_VOLUME(Double CASH_IN_VOLUME) {
        this.CASH_IN_VOLUME = CASH_IN_VOLUME;
    }

    public Double getCASH_OUT_VOLUME() {
        return CASH_OUT_VOLUME;
    }

    public void setCASH_OUT_VOLUME(Double CASH_OUT_VOLUME) {
        this.CASH_OUT_VOLUME = CASH_OUT_VOLUME;
    }

    public Double getCHECK_IN_VOLUME() {
        return CHECK_IN_VOLUME;
    }

    public void setCHECK_IN_VOLUME(Double CHECK_IN_VOLUME) {
        this.CHECK_IN_VOLUME = CHECK_IN_VOLUME;
    }

    public Double getCHECK_OUT_VOLUME() {
        return CHECK_OUT_VOLUME;
    }

    public void setCHECK_OUT_VOLUME(Double CHECK_OUT_VOLUME) {
        this.CHECK_OUT_VOLUME = CHECK_OUT_VOLUME;
    }

    public Double getOVERALL_SCORE_ADJUSTMENT() {
        return OVERALL_SCORE_ADJUSTMENT;
    }

    public void setOVERALL_SCORE_ADJUSTMENT(Double OVERALL_SCORE_ADJUSTMENT) {
        this.OVERALL_SCORE_ADJUSTMENT = OVERALL_SCORE_ADJUSTMENT;
    }

    public String getTAX_NUMBER() {
        return TAX_NUMBER;
    }

    public void setTAX_NUMBER(String TAX_NUMBER) {
        this.TAX_NUMBER = TAX_NUMBER;
    }

    public String getTAX_NUMBER_ISSUED_BY() {
        return TAX_NUMBER_ISSUED_BY;
    }

    public void setTAX_NUMBER_ISSUED_BY(String TAX_NUMBER_ISSUED_BY) {
        this.TAX_NUMBER_ISSUED_BY = TAX_NUMBER_ISSUED_BY;
    }

    public String getCUSTOMER_CATEGORY_CODE() {
        return CUSTOMER_CATEGORY_CODE;
    }

    public void setCUSTOMER_CATEGORY_CODE(String CUSTOMER_CATEGORY_CODE) {
        this.CUSTOMER_CATEGORY_CODE = CUSTOMER_CATEGORY_CODE;
    }

    public String getOWN_AFFILIATE_FLAG() {
        return OWN_AFFILIATE_FLAG;
    }

    public void setOWN_AFFILIATE_FLAG(String OWN_AFFILIATE_FLAG) {
        this.OWN_AFFILIATE_FLAG = OWN_AFFILIATE_FLAG;
    }

    public String getMARKETING_SERVICE_LEVEL() {
        return MARKETING_SERVICE_LEVEL;
    }

    public void setMARKETING_SERVICE_LEVEL(String MARKETING_SERVICE_LEVEL) {
        this.MARKETING_SERVICE_LEVEL = MARKETING_SERVICE_LEVEL;
    }

    public String getSANCTIONED_FLAGINGESTED() {
        return SANCTIONED_FLAGINGESTED;
    }

    public void setSANCTIONED_FLAGINGESTED(String SANCTIONED_FLAGINGESTED) {
        this.SANCTIONED_FLAGINGESTED = SANCTIONED_FLAGINGESTED;
    }

    public String getPEP_TYPEINGESTED() {
        return PEP_TYPEINGESTED;
    }

    public void setPEP_TYPEINGESTED(String PEP_TYPEINGESTED) {
        this.PEP_TYPEINGESTED = PEP_TYPEINGESTED;
    }

    public String getRCA_FLAGINGESTED() {
        return RCA_FLAGINGESTED;
    }

    public void setRCA_FLAGINGESTED(String RCA_FLAGINGESTED) {
        this.RCA_FLAGINGESTED = RCA_FLAGINGESTED;
    }

    public Timestamp getADDRESS_VALID_FROM() {
        return ADDRESS_VALID_FROM;
    }

    public void setADDRESS_VALID_FROM(Timestamp ADDRESS_VALID_FROM) {
        this.ADDRESS_VALID_FROM = ADDRESS_VALID_FROM;
    }

    public Timestamp getADDRESS_VALID_TO() {
        return ADDRESS_VALID_TO;
    }

    public void setADDRESS_VALID_TO(Timestamp ADDRESS_VALID_TO) {
        this.ADDRESS_VALID_TO = ADDRESS_VALID_TO;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public Timestamp getEMAIL_VALID_FROM() {
        return EMAIL_VALID_FROM;
    }

    public void setEMAIL_VALID_FROM(Timestamp EMAIL_VALID_FROM) {
        this.EMAIL_VALID_FROM = EMAIL_VALID_FROM;
    }

    public Timestamp getEMAIL_VALID_TO() {
        return EMAIL_VALID_TO;
    }

    public void setEMAIL_VALID_TO(Timestamp EMAIL_VALID_TO) {
        this.EMAIL_VALID_TO = EMAIL_VALID_TO;
    }

    public String getPHONE_COUNTRY_CODE() {
        return PHONE_COUNTRY_CODE;
    }

    public void setPHONE_COUNTRY_CODE(String PHONE_COUNTRY_CODE) {
        this.PHONE_COUNTRY_CODE = PHONE_COUNTRY_CODE;
    }

    public String getPHONE_AREA_CODE() {
        return PHONE_AREA_CODE;
    }

    public void setPHONE_AREA_CODE(String PHONE_AREA_CODE) {
        this.PHONE_AREA_CODE = PHONE_AREA_CODE;
    }

    public String getPHONE_NUMBER() {
        return PHONE_NUMBER;
    }

    public void setPHONE_NUMBER(String PHONE_NUMBER) {
        this.PHONE_NUMBER = PHONE_NUMBER;
    }

    public String getPHONE_EXTENSION() {
        return PHONE_EXTENSION;
    }

    public void setPHONE_EXTENSION(String PHONE_EXTENSION) {
        this.PHONE_EXTENSION = PHONE_EXTENSION;
    }

    public Timestamp getPHONE_VALID_FROM() {
        return PHONE_VALID_FROM;
    }

    public void setPHONE_VALID_FROM(Timestamp PHONE_VALID_FROM) {
        this.PHONE_VALID_FROM = PHONE_VALID_FROM;
    }

    public Timestamp getPHONE_VALID_TO() {
        return PHONE_VALID_TO;
    }

    public void setPHONE_VALID_TO(Timestamp PHONE_VALID_TO) {
        this.PHONE_VALID_TO = PHONE_VALID_TO;
    }

    public String getALTERNATE_NAME() {
        return ALTERNATE_NAME;
    }

    public void setALTERNATE_NAME(String ALTERNATE_NAME) {
        this.ALTERNATE_NAME = ALTERNATE_NAME;
    }

    public String getTAX_NUMBER_TYPE() {
        return TAX_NUMBER_TYPE;
    }

    public void setTAX_NUMBER_TYPE(String TAX_NUMBER_TYPE) {
        this.TAX_NUMBER_TYPE = TAX_NUMBER_TYPE;
    }

    public String getBUSINESS_CLASSIFICATION_C() {
        return BUSINESS_CLASSIFICATION_C;
    }

    public void setBUSINESS_CLASSIFICATION_C(String BUSINESS_CLASSIFICATION_C) {
        this.BUSINESS_CLASSIFICATION_C = BUSINESS_CLASSIFICATION_C;
    }

    public String getBUSINESS_CLASSIFICATION_SY() {
        return BUSINESS_CLASSIFICATION_SY;
    }

    public void setBUSINESS_CLASSIFICATION_SY(String BUSINESS_CLASSIFICATION_SY) {
        this.BUSINESS_CLASSIFICATION_SY = BUSINESS_CLASSIFICATION_SY;
    }

    public String getBNPP_FLUX_TRANSFRONTALIE() {
        return BNPP_FLUX_TRANSFRONTALIE;
    }

    public void setBNPP_FLUX_TRANSFRONTALIE(String BNPP_FLUX_TRANSFRONTALIE) {
        this.BNPP_FLUX_TRANSFRONTALIE = BNPP_FLUX_TRANSFRONTALIE;
    }

    public String getBNPP_BANQUE_PRIVEE() {
        return BNPP_BANQUE_PRIVEE;
    }

    public void setBNPP_BANQUE_PRIVEE(String BNPP_BANQUE_PRIVEE) {
        this.BNPP_BANQUE_PRIVEE = BNPP_BANQUE_PRIVEE;
    }

    public String getBNPP_GEST_FORTUNE() {
        return BNPP_GEST_FORTUNE;
    }

    public void setBNPP_GEST_FORTUNE(String BNPP_GEST_FORTUNE) {
        this.BNPP_GEST_FORTUNE = BNPP_GEST_FORTUNE;
    }

    public String getBNPP_INTERDIT_BQ_GENE() {
        return BNPP_INTERDIT_BQ_GENE;
    }

    public void setBNPP_INTERDIT_BQ_GENE(String BNPP_INTERDIT_BQ_GENE) {
        this.BNPP_INTERDIT_BQ_GENE = BNPP_INTERDIT_BQ_GENE;
    }

    public String getBNPP_INTERDIT_BQ() {
        return BNPP_INTERDIT_BQ;
    }

    public void setBNPP_INTERDIT_BQ(String BNPP_INTERDIT_BQ) {
        this.BNPP_INTERDIT_BQ = BNPP_INTERDIT_BQ;
    }

    public String getBNPP_INTERDIT_JUDICIAIRE() {
        return BNPP_INTERDIT_JUDICIAIRE;
    }

    public void setBNPP_INTERDIT_JUDICIAIRE(String BNPP_INTERDIT_JUDICIAIRE) {
        this.BNPP_INTERDIT_JUDICIAIRE = BNPP_INTERDIT_JUDICIAIRE;
    }

    public String getBNPP_ENTREE_REL_A_RÉGUL() {
        return BNPP_ENTREE_REL_A_RÉGUL;
    }

    public void setBNPP_ENTREE_REL_A_RÉGUL(String BNPP_ENTREE_REL_A_RÉGUL) {
        this.BNPP_ENTREE_REL_A_RÉGUL = BNPP_ENTREE_REL_A_RÉGUL;
    }

    public String getBNPP_CEDANT_FORFAIT_TOT() {
        return BNPP_CEDANT_FORFAIT_TOT;
    }

    public void setBNPP_CEDANT_FORFAIT_TOT(String BNPP_CEDANT_FORFAIT_TOT) {
        this.BNPP_CEDANT_FORFAIT_TOT = BNPP_CEDANT_FORFAIT_TOT;
    }

    public String getBNPP_CLIENT_A_SURVEILLER() {
        return BNPP_CLIENT_A_SURVEILLER;
    }

    public void setBNPP_CLIENT_A_SURVEILLER(String BNPP_CLIENT_A_SURVEILLER) {
        this.BNPP_CLIENT_A_SURVEILLER = BNPP_CLIENT_A_SURVEILLER;
    }

    public String getBNPP_CPT_SURVEIL_GENE() {
        return BNPP_CPT_SURVEIL_GENE;
    }

    public void setBNPP_CPT_SURVEIL_GENE(String BNPP_CPT_SURVEIL_GENE) {
        this.BNPP_CPT_SURVEIL_GENE = BNPP_CPT_SURVEIL_GENE;
    }

    public String getBNPP_OUVERT_CPT_IMPOSEE() {
        return BNPP_OUVERT_CPT_IMPOSEE;
    }

    public void setBNPP_OUVERT_CPT_IMPOSEE(String BNPP_OUVERT_CPT_IMPOSEE) {
        this.BNPP_OUVERT_CPT_IMPOSEE = BNPP_OUVERT_CPT_IMPOSEE;
    }

    public String getBNPP_TYPE_ENGAGEMENT() {
        return BNPP_TYPE_ENGAGEMENT;
    }

    public void setBNPP_TYPE_ENGAGEMENT(String BNPP_TYPE_ENGAGEMENT) {
        this.BNPP_TYPE_ENGAGEMENT = BNPP_TYPE_ENGAGEMENT;
    }

    public String getBNPP_INDICE_COURRIER() {
        return BNPP_INDICE_COURRIER;
    }

    public void setBNPP_INDICE_COURRIER(String BNPP_INDICE_COURRIER) {
        this.BNPP_INDICE_COURRIER = BNPP_INDICE_COURRIER;
    }

    public String getBNPP_TYPE_CREANCE_RISQ() {
        return BNPP_TYPE_CREANCE_RISQ;
    }

    public void setBNPP_TYPE_CREANCE_RISQ(String BNPP_TYPE_CREANCE_RISQ) {
        this.BNPP_TYPE_CREANCE_RISQ = BNPP_TYPE_CREANCE_RISQ;
    }

    public String getBNPP_REQ_JUD_BLA() {
        return BNPP_REQ_JUD_BLA;
    }

    public void setBNPP_REQ_JUD_BLA(String BNPP_REQ_JUD_BLA) {
        this.BNPP_REQ_JUD_BLA = BNPP_REQ_JUD_BLA;
    }

    public String getBNPP_TRANCHE_FMC() {
        return BNPP_TRANCHE_FMC;
    }

    public void setBNPP_TRANCHE_FMC(String BNPP_TRANCHE_FMC) {
        this.BNPP_TRANCHE_FMC = BNPP_TRANCHE_FMC;
    }

    public Date getBNPP_DATE_INCIDENT() {
        return BNPP_DATE_INCIDENT;
    }

    public void setBNPP_DATE_INCIDENT(Date BNPP_DATE_INCIDENT) {
        this.BNPP_DATE_INCIDENT = BNPP_DATE_INCIDENT;
    }

    public Double getBNPP_NB_EMPLOYES() {
        return BNPP_NB_EMPLOYES;
    }

    public void setBNPP_NB_EMPLOYES(Double BNPP_NB_EMPLOYES) {
        this.BNPP_NB_EMPLOYES = BNPP_NB_EMPLOYES;
    }
}
