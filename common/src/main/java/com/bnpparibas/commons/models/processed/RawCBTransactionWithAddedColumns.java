package com.bnpparibas.commons.models.processed;


public class RawCBTransactionWithAddedColumns extends RawTransactionWithCBTransactionType {

    public RawCBTransactionWithAddedColumns(){}

    //
    private String CUSTOMER;
    private String COUNTRY_CODE;
    private String COUNTER_PARTY;
    private String ACCOUNT;
    private String COUNTRY_COUNTER_PARTY;
    private String VIR_INTERNATIONAL_FLAG;
    private String ACOUNT_COUNTER_PARTY;

    public String getCUSTOMER() {
        return CUSTOMER;
    }

    public void setCUSTOMER(String CUSTOMER) {
        this.CUSTOMER = CUSTOMER;
    }

    public String getCOUNTRY_CODE() {
        return COUNTRY_CODE;
    }

    public void setCOUNTRY_CODE(String COUNTRY_CODE) {
        this.COUNTRY_CODE = COUNTRY_CODE;
    }

    public String getCOUNTER_PARTY() {
        return COUNTER_PARTY;
    }

    public void setCOUNTER_PARTY(String COUNTER_PARTY) {
        this.COUNTER_PARTY = COUNTER_PARTY;
    }

    public String getACCOUNT() {
        return ACCOUNT;
    }

    public void setACCOUNT(String ACCOUNT) {
        this.ACCOUNT = ACCOUNT;
    }

    public String getCOUNTRY_COUNTER_PARTY() {
        return COUNTRY_COUNTER_PARTY;
    }

    public void setCOUNTRY_COUNTER_PARTY(String COUNTRY_COUNTER_PARTY) {
        this.COUNTRY_COUNTER_PARTY = COUNTRY_COUNTER_PARTY;
    }

    public String getVIR_INTERNATIONAL_FLAG() {
        return VIR_INTERNATIONAL_FLAG;
    }

    public void setVIR_INTERNATIONAL_FLAG(String VIR_INTERNATIONAL_FLAG) {
        this.VIR_INTERNATIONAL_FLAG = VIR_INTERNATIONAL_FLAG;
    }

    public String getACOUNT_COUNTER_PARTY() {
        return ACOUNT_COUNTER_PARTY;
    }

    public void setACOUNT_COUNTER_PARTY(String ACOUNT_COUNTER_PARTY) {
        this.ACOUNT_COUNTER_PARTY = ACOUNT_COUNTER_PARTY;
    }
}
