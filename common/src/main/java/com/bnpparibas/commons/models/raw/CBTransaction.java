package com.bnpparibas.commons.models.raw;

import lombok.*;

import java.sql.Date;
import java.sql.Timestamp;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class CBTransaction {
    private Timestamp RUN_TIMESTAMP;
    private String SRC_TXN_UNIQUE_ID ;
    private String SRC_TXN_NUM  ;
    private String TELLER_ID  ;
    private String TXN_SRC_TYPE_CODE  ;
    private String CURRENCY_CODE_ORIG ;
    private String CURRENCY_CODE_BASE ;
    private Date ORIGINATION_DATE  ;
    private Date POSTING_DATE  ;
    private Date VALUE_DATE  ;
    private Timestamp SYSTEM_TIMESTAMP  ;
    private Timestamp LOCAL_TIMESTAMP  ;
    private String TXN_CHANNEL_CODE ;
    private Double TXN_AMOUNT_ORIG  ;
    private Double TXN_AMOUNT_BASE  ;
    private String TRANS_REF_DESC ;
    private String CLIENT_DE_PASSAGE ;
    private String TXN_STATUS_CODE ;
    private String SRC_SYSTEM_CODE ;
    private String ORGUNIT_CODE ;
    private String CARD_SOURCE_REF_ID ;
    private String ORIG_CPARTY_SRC_UNIQUE_ID ;
    private String ORIG_CPARTY_SRC_REF_ID ;
    private String ORIG_SUBCUST_SRC_UNIQUE_ID ;
    private String ORIG_SUBCUST_SRC_REF_ID ;
    private String ORIG_CPARTY_ACCOUNT_ID ;
    private String ORIG_BIC ;
    private String ORIG_ACCOUNT_NUMBER ;
    private String ORIG_NAME ;
    private String ORIG_BANK_NAME ;
    private String ORIG_COUNTRY_CODE ;
    private String ORIG_ADDRESS ;
    private String ORIG_CITY ;
    private String ORIG_ZONE ;
    private String ORIG_POSTAL_CODE ;
    private String ORIG_BANK_COUNTRY_CODE ;
    private String ORIG_BANK_ADDRESS ;
    private String BENE_CPARTY_SRC_UNIQUE_ID ;
    private String BENE_CPARTY_SRC_REF_ID ;
    private String BENE_SUBCUST_SRC_UNIQUE_ID ;
    private String BENE_SUBCUST_SRC_REF_ID ;
    private String BENE_CPARTY_ACCOUNT_ID ;
    private String BENE_BIC ;
    private String BENE_ACCOUNT_NUMBER ;
    private String BENE_NAME ;
    private String BENE_BANK_NAME ;
    private String BENE_COUNTRY_CODE ;
    private String BENE_ADDRESS ;
    private String BENE_CITY ;
    private String BENE_ZONE ;
    private String BENE_POSTAL_CODE ;
    private String BENE_BANK_COUNTRY_CODE ;
    private String BENE_BANK_ADDRESS ;
    private String DB_RESP_CUST_SRC_UNIQUE_ID ;
    private String DB_RESP_CUST_SRC_REF_ID ;
    private String DB_RESP_ACCOUNT_SRC_UNIQUE ;
    private String DB_RESP_ACCOUNT_SRC_REF_ID ;
    private String DB_RESP_BIC ;
    private String DB_RESP_ACCOUNT_NUMBER ;
    private String DB_RESP_ORGUNIT_CODE ;
    private String CR_RESP_CUST_SRC_UNIQUE_ID ;
    private String CR_RESP_CUST_SRC_REF_ID ;
    private String CR_RESP_ACCOUNT_SRC_UNIQUE_ ;
    private String CR_RESP_ACCOUNT_SRC_REF_ID ;
    private String CR_RESP_BIC ;
    private String CR_RESP_ACCOUNT_NUMBER ;
    private String CR_RESP_ORGUNIT_CODE ;
    private String SENDING_BANK_NAME ;
    private String SENDING_BANK_BIC ;
    private String SENDING_BANK_ACCOUNT_NUMBE ;
    private String SENDING_BANK_COUNTRY_CODE ;
    private String SENDING_BANK_ADDRESS ;
    private String RECV_BANK_NAME ;
    private String RECV_BANK_BIC ;
    private String RECV_BANK_ACCOUNT_NUMBER ;
    private String RECV_BANK_COUNTRY_CODE ;
    private String RECV_BANK_ADDRESS ;
    private String INT_BANK_NAME ;
    private String INT_BANK_BIC ;
    private String INT_BANK_ACCOUNT_NUMBER ;
    private String INT_BANK_COUNTRY_CODE ;
    private String INT_BANK_ADDRESS ;
    private String ORDER_INST_BANK_NAME ;
    private String ORDER_INST_BANK_BIC ;
    private String ORDER_INST_ACCOUNT_NUMBER ;
    private String ORDER_INST_COUNTRY_CODE ;
    private String ORDER_INST_ADDRESS ;
    private String SENDER_CORR_BANK_NAME ;
    private String SENDER_CORR_BANK_BIC ;
    private String SENDER_CORR_ACCOUNT_NUMBE ;
    private String SENDER_CORR_COUNTRY_CODE ;
    private String SENDER_CORR_ADDRESS ;
    private String RECV_CORR_BANK_NAME ;
    private String RECV_CORR_BANK_BIC ;
    private String RECV_CORR_ACCOUNT_NUMBER ;
    private String RECV_CORR_COUNTRY_CODE ;
    private String RECV_CORR_ADDRESS ;
    private Integer day_part ;

}
