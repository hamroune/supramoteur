package com.bnpparibas.commons.models.domain.enums;

import java.util.HashMap;
import java.util.Map;

public enum SegmentAML {
    UKNOWN(""),
    NP_RB_INA("NP-RB-INA"),
    NP_RB_CB("NP-RB-CB"),
    NP_RB_NR("NP-RB-NR"),
    NP_RB_VUL("NP-RB-VUL"),
    NP_RB_CC("NP-RB-CC"),
    NP_PIV("NP-PIV"),
    LE_CORP_CA300M("LE-CORP-CA300M"),
    NP_RB_CA("NP-RB-CA"),
    NP_RB_EI("NP-RB-EI"),
    LE_SB_PRO("LE-SB-PRO"),
    ASSO_NPS_SM("ASSO-NPS-SM"),
    NP_WM("NP-WM"),
    LE_CORP_CA0M("LE-CORP-CA0M"),
    ASSO_NPS_L("ASSO-NPS-L"),
    NP_PB("NP-PB"),
    LE_CORP_CA50M("LE-CORP-CA50M");

    private String value;
    private static final Map<String, SegmentAML> lookup = new HashMap();

    static
    {
        for(SegmentAML segmentAML : SegmentAML.values())
        {
            lookup.put(segmentAML.getValue(), segmentAML);
        }
    }
    SegmentAML(String value){
        this.value=value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static SegmentAML lookup(String value){
        try{
            return lookup.get(value);
        }catch (Exception e){
            return SegmentAML.UKNOWN;
        }
    }
}
