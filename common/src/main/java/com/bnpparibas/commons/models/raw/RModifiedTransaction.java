package com.bnpparibas.commons.models.raw;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.sql.Date;
import java.sql.Timestamp;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class RModifiedTransaction {
 private Timestamp  RUN_TIMESTAMP;
 private String  SRC_TXN_UNIQUE_ID;
 private String  SOURCE_TXN_NUM;
 private String  ACCOUNT;
 private String  ACCOUNT_SOURCE_REF_ID;
 private String  CUSTOMER;
 private String  PRIMARY_CUST_SRCE_REF_ID;
 private String  BRANCH_ID;
 private String  TXN_SOURCE_TYPE_CODE;
 private String  OWN_ACCOUNT_TRANSFER;
 private String  CURRENCY_CODE_ORIG;
 private String  CURRENCY_CODE_BASE;
 private Date  ORIGINATION_DATE;
 private Date  POSTING_DATE;
 private Date VALUE_DATE;
 private Timestamp SYSTEM_TIMESTAMP;
 private Timestamp  LOCAL_TIMESTAMP;
 private String  PRODUCT_SOURCE_TYPE_CODE;
 private Integer  TXN_VOLUME;
 private String  DEVICE_ID;
 private Double  TXN_AMOUNT_ORIG;
 private Double  TXN_AMOUNT_BASE;
 private String  CREDIT_DEBIT_CODE;
 private String  TRANS_REF_DESC;
 private String  TRANS_REF_DESC_2;
 private String  TRANS_REF_DESC_3;
 private String  TRANS_REF_DESC_4;
 private String  TRANS_REF_DESC_5;
 private String  TRANS_REF_DESC_6;
 private String  CLIENT_DE_PASSAGE;
 private String  TXN_STATUS_CODE;
 private String  TXN_CHANNEL_CODE;
 private String  SOURCE_SYSTEM_CODE;
 private String  ERROR_CORRECT_FLAG;
 private String  TRANSACTION_LOCATION;
 private String  ORG_UNIT_CODE;
 private String  TXN_USR_DTLS;
 private String  DVC_POS_ENTRY_MODE;
 private String  PIN_VERIFY_CD;
 private String  CARD_ID;
 private String  EMPLOYEE_ID;
 private String  COUNTER_PARTY_NAME;
 private String  COUNTER_PARTY_ADDRESS;
 private String  COUNTER_PARTY_ZONE;
 private String  COUNTER_PARTY_POSTAL_CODE;
 private String  COUNTER_PARTY_CITY;
 private String  COUNTER_PARTY_COUNTRY_C;
 private String  COUNTER_PARTY_ACCOUNT_NUM;
 private String  COUNTER_PARTY_ACCOUNT_NAME;
 private String  COUNTER_PARTY_ACCOUNT_TYPE;
 private String  COUNTER_PARTY_ACCOUNT_IBAN;
 private String  COUNTER_PARTY_ACCOUNT_BIC;
 private String  COUNTER_PARTY_BANK_NAME;
 private String  COUNTER_PARTY_BANK_CODE;
 private String  COUNTER_PARTY_BANK_ADDRESS;
 private String  COUNTER_PARTY_BANK_CITY;
 private String  COUNTER_PARTY_BANK_ZONE;
 private String  COUNTER_PARTY_BANK_POSTAL_CODE;
 private String  COUNTER_PARTY_BNK_CNTRY_CD;
 private String  ORIGINATOR_NAME;
 private String  BENEFICIARY_NAME;
 private String  ORIGINATOR_BANK_NAME;
 private String  BENEFICIARY_BANK_NAME;
 private String  TELLER_ID;
 private String  CARD_SOURCE_REF_ID;
 private String  CHECK_NUMBER;
 private String  CHECK_ACCOUNT_NUMBER;
 private Double  CHECK_AMOUNT;
 private Double  CASHBACK_AMT;
 private Integer  day_part;
}
