package xom;

import java.util.List;

public class InmemoryCollectionOfCounterpartieIds implements CollectionOfStrings {
    List<String> counterparties;

    public InmemoryCollectionOfCounterpartieIds() {
    }

    public List<String> getCounterparties() {
        return counterparties;
    }

    public void setCounterparties(List<String> counterparties) {
        this.counterparties = counterparties;
    }

    @Override
    public String[] take(int n) {
        n = Math.min(n, counterparties.size());
        return counterparties.subList(0, n).toArray(new String[n]);
    }

    @Override
    public long count() {
        return counterparties.size();
    }
}