package xom;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("serial")
public class Report implements Serializable {
    List<Alert> alerts = new ArrayList<>();
    List<String> messages = new ArrayList<>();

    public void addAlert(String scenario, Client client, String[] messages, CollectionOfTransactions collectionOfTransactions,
                         CollectionOfStrings collectionOfCounterparts) {
        Alert alert = new Alert();
        alert.initialize(scenario, client, messages, collectionOfTransactions, collectionOfCounterparts);
        alerts.add(alert);
    }

    public void addAlert(String scenario, Client client, String messages[], CollectionOfTransactions collectionOfTransactions) {
        addAlert(scenario, client, messages, collectionOfTransactions, null);
    }
    public void addMessage(String message) {
        messages.add(message);
    }

    public String[] getNomsScenarios() {
        if (alerts.isEmpty()) {
            return new String[] { "Aucune alerte" };
        }
        return alerts.stream().map(a -> a.getNomScenario()).collect(Collectors.toList()).toArray(new String[0]);
    }

    public List<Alert> getAlerts() {
        return alerts;
    }
    public void setAlerts(List<Alert> alerts) {
        this.alerts = alerts;
    }

    public List<String> getMessages() {
        return messages;
    }
    public void setMessages(List<String> messages) {
        this.messages = messages;
    }
}
