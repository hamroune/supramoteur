package xom;

public interface CollectionOfStrings {
    String[] take(int n);
    long count();
}
