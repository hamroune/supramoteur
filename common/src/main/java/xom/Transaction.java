package xom;

import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;

@SuppressWarnings("serial")
public class Transaction extends AMLEntity implements Serializable, Comparable<Transaction> {

    // IMPORTANT: MAKE SURE YOU UPDATE clone

    String id; // -> Renommer uniqueId ?
    String clientId;
    double amount; // amount devrait toujours être positif
    Day day;

    Set<TransactionFlag> flags = new TreeSet<>();
    // DEBIT, CREDIT,
    // CLIENT_HIGH_RISK_COUNTRY, COUNTERPARTY_HIGH_RISK_COUNTRY,
    // CLIENT_VERY_HIGH_RISK_COUNTRY, COUNTERPARTY_VERY_HIGH_RISK_COUNTRY,
    // CLIENT_PRIVILEGED_TASK_COUNTRY, COUNTERPARTY_PRIVILEGED_TASK_COUNTRY,
    // TR_INTERNATIONAL_TRANSFER, TR_SEPA, TR_SWIFT, TR_CROSSBORDER;

    String counterpartyAccountNumber; // Pour les contreparties inédites et en sortie de l'alerte.

    String clientCountry; // Obtenir si possible les valeurs du domaine pays
    String counterpartyCountry;
    String clientCountryScore; // Obtenir si possible les valeurs du domaine pays
    String counterpartyCountryScore;

    public Transaction clone(String id) {
        Transaction t = new Transaction();
        t.id = id;
        t.clientId = clientId;
        t.amount = this.amount;
        t.day = this.day.clone2();
        t.flags = new TreeSet<TransactionFlag>(this.flags);
        t.counterpartyAccountNumber = this.counterpartyAccountNumber;
        t.clientCountry = this.clientCountry; // Obtenir si possible les valeurs du domaine pays
        t.counterpartyCountry = this.counterpartyCountry;
        t.clientCountryScore = this.clientCountryScore;
        t.counterpartyCountryScore = this.counterpartyCountryScore;

        return t;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public double getAmount() {
        return amount;
    }
    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public Set<TransactionFlag> getFlags() {
        return flags;
    }
    public void setFlags(Set<TransactionFlag> flags) {
        this.flags = flags;
    }
    public boolean isFlags(TransactionFlag flag) {
        return flags.contains(flag);
    }
    public boolean containsAtLeastOneFlagIn(TransactionFlag[] tF) {
        for (TransactionFlag t : tF) {
            if (flags.contains(t)) {
                return true;
            }
        }
        return false;
    }

    public String getCounterpartyAccountNumber() {
        return counterpartyAccountNumber;
    }
    public void setCounterpartyAccountNumber(String counterpartyAccountNumber) {
        this.counterpartyAccountNumber = counterpartyAccountNumber;
    }

    public String getClientCountry() {
        return clientCountry;
    }
    public void setClientCountry(String clientCountry) {
        this.clientCountry = clientCountry;
    }

    public String getCounterpartyCountry() {
        return counterpartyCountry;
    }
    public void setCounterpartyCountry(String counterpartyCountry) {
        this.counterpartyCountry = counterpartyCountry;
    }

    public String getClientCountryScore() {
        return clientCountryScore;
    }
    public void setClientCountryScore(String clientCountryScore) {
        this.clientCountryScore = clientCountryScore;
    }

    public String getCounterpartyCountryScore() {
        return counterpartyCountryScore;
    }
    public void setCounterpartyCountryScore(String counterpartyCountryScore) {
        this.counterpartyCountryScore = counterpartyCountryScore;
    }

    /*
    // Underscore to avoid JAXB looking for setter
    public Country _getCountryOfCounterparty() {
        switch (counterparty.getIban().substring(0, 2)) {
        case "DE":
            return Country.GERMANY;
        case "GR":
            return Country.GREECE;
        case "IE":
            return Country.IRELAND;
        case "IT":
            return Country.ITALY;
        case "LI":
            return Country.LIECHTENSTEIN;
        case "NK":
            return Country.NORTH_KOREA;
        case "IR":
            return Country.IRAN;
        case "FR":
        default:
            return Country.FRANCE;
        }
    }
    */
    @Override
    public boolean equals(Object obj) {
        return obj instanceof Transaction ? ((Transaction) obj).id == id : false;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return String.format("%s; %s; %s; %.2f; %s; %s", id, clientId, day, amount);
    }

    @Override
    public int compareTo(Transaction o) {
        return id.compareTo(o.id);
    }
}