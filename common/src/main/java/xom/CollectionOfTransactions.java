package xom;

import java.util.Arrays;
import java.util.HashSet;

public abstract class CollectionOfTransactions {

    protected abstract CollectionOfTransactions filter(TransactionPredicate predicate);

    public CollectionOfTransactions filterByClientId(String clientId) {
        return filter(transaction -> transaction.getClientId().equals(clientId));
    }

    public CollectionOfTransactions filterByClientIds(String[] clientIds) {
        HashSet<String> hashSet = new HashSet<>(Arrays.asList(clientIds));
        return filter(transaction -> hashSet.contains(transaction.getClientId()));
    }

    public CollectionOfTransactions filterByDay(Day day) {
        return filter(transaction -> transaction.getDay().equals(day));
    }

    public CollectionOfTransactions filterByDay(Day day1, Day day2) {
        return filter(transaction -> transaction.getDay().isBetween(day1, day2));
    }

    public CollectionOfTransactions filterByTransactionFlag(TransactionFlag transactionFlag) {
        return filter(transaction -> transaction.isFlags(transactionFlag));
    }

    public CollectionOfTransactions filterByTransactionContainsAtLeastOneFlagIn(TransactionFlag[] transactionFlags) {
        return filter(transaction -> transaction.containsAtLeastOneFlagIn(transactionFlags));
    }

    public abstract CollectionOfTransactions filterNotDoneWith(CollectionOfStrings counterparties);

    public CollectionOfTransactions filterAmountEurosMoreThan(double amount) {
        return filter(transaction -> transaction.getAmount() > amount);
    }

    public abstract double _getTotalAmount(); // Avoid JAXB looking for setter => underscore

    public abstract CollectionOfTransactions union(CollectionOfTransactions other);

    public abstract CollectionOfStrings _getCounterparties();

    public abstract Transaction[] take(int n);

    public abstract long count();

    public abstract void cache();

    public abstract void unpersist();
}