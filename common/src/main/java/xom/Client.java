package xom;

import java.io.Serializable;
import java.util.Objects;

@SuppressWarnings("serial")
public class Client extends AMLEntity implements Serializable {

    String id;
    boolean isCashIntensive;
    // Pour la profondeur d'historique
    Marche marche; // -> Domaine à fournir par BigApps.

    // Pour la sortie (alertes)
    String codeIBAN;
    String cotationRSF;

    // Pour la table des seuils
    // NB: Les types String peuvent devenir des enums lorsque les domaines seront connus.
    String segmentKYC; // -> Domaine à fournir par BigApps.
    String segmentAML; // -> Domaine à fournir par BigApps.
    String segmentAPE; // -> Domaine à fournir si possible par BigApps.
    String segmentPPE; // -> Domaine à fournir si possible par BigApps.
    String segmentSRA; // -> Domaine à fournir si possible par BigApps.
    String segmentBLA; // -> Domaine à fournir si possible par BigApps.
    String countryRiskScore; // -> Domaine à fournir si possible par BigApps.
    int customerAge;
    String branch; // -> Domaine à fournir si possible par BigApps.
    String organization; // -> Domaine à fournir si possible par BigApps.

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public boolean isCashIntensive() {
        return isCashIntensive;
    }
    public void setCashIntensive(boolean isCashIntensive) {
        this.isCashIntensive = isCashIntensive;
    }

    public String getCodeIBAN() {
        return codeIBAN;
    }
    public void setCodeIBAN(String codeIBAN) {
        this.codeIBAN = codeIBAN;
    }

    public String getCotationRSF() {
        return cotationRSF;
    }
    public void setCotationRSF(String cotationRSF) {
        this.cotationRSF = cotationRSF;
    }

    public Marche getMarche() {
        return marche;
    }
    public void setMarche(Marche marche) {
        this.marche = marche;
    }

    public String getSegmentKYC() {
        return segmentKYC;
    }
    public void setSegmentKYC(String segmentKYC) {
        this.segmentKYC = segmentKYC;
    }

    public String getSegmentAML() {
        return segmentAML;
    }
    public void setSegmentAML(String segmentAML) {
        this.segmentAML = segmentAML;
    }

    public String getSegmentAPE() {
        return segmentAPE;
    }
    public void setSegmentAPE(String segmentAPE) {
        this.segmentAPE = segmentAPE;
    }

    public String getSegmentPPE() {
        return segmentPPE;
    }
    public void setSegmentPPE(String segmentPPE) {
        this.segmentPPE = segmentPPE;
    }

    public String getSegmentSRA() {
        return segmentSRA;
    }
    public void setSegmentSRA(String segmentSRA) {
        this.segmentSRA = segmentSRA;
    }

    public String getSegmentBLA() {
        return segmentBLA;
    }
    public void setSegmentBLA(String segmentBLA) {
        this.segmentBLA = segmentBLA;
    }

    public String getCountryRiskScore() {
        return countryRiskScore;
    }
    public void setCountryRiskScore(String countryRiskScore) {
        this.countryRiskScore = countryRiskScore;
    }

    public int getCustomerAge() {
        return customerAge;
    }
    public void setCustomerAge(int customerAge) {
        this.customerAge = customerAge;
    }

    public String getBranch() {
        return branch;
    }
    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getOrganization() {
        return organization;
    }
    public void setOrganization(String organization) {
        this.organization = organization;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id='" + id + '\'' +
                ", isCashIntensive=" + isCashIntensive +
                ", marche=" + marche +
                ", codeIBAN='" + codeIBAN + '\'' +
                ", cotationRSF='" + cotationRSF + '\'' +
                ", segmentKYC='" + segmentKYC + '\'' +
                ", segmentAML='" + segmentAML + '\'' +
                ", segmentAPE='" + segmentAPE + '\'' +
                ", segmentPPE='" + segmentPPE + '\'' +
                ", segmentSRA='" + segmentSRA + '\'' +
                ", segmentBLA='" + segmentBLA + '\'' +
                ", countryRiskScore='" + countryRiskScore + '\'' +
                ", customerAge=" + customerAge +
                ", branch='" + branch + '\'' +
                ", organization='" + organization + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Client)) return false;
        Client client = (Client) o;
        return isCashIntensive() == client.isCashIntensive() && getCustomerAge() == client.getCustomerAge() && Objects.equals(getId(), client.getId()) && getMarche() == client.getMarche() && Objects.equals(getCodeIBAN(), client.getCodeIBAN()) && Objects.equals(getCotationRSF(), client.getCotationRSF()) && Objects.equals(getSegmentKYC(), client.getSegmentKYC()) && Objects.equals(getSegmentAML(), client.getSegmentAML()) && Objects.equals(getSegmentAPE(), client.getSegmentAPE()) && Objects.equals(getSegmentPPE(), client.getSegmentPPE()) && Objects.equals(getSegmentSRA(), client.getSegmentSRA()) && Objects.equals(getSegmentBLA(), client.getSegmentBLA()) && Objects.equals(getCountryRiskScore(), client.getCountryRiskScore()) && Objects.equals(getBranch(), client.getBranch()) && Objects.equals(getOrganization(), client.getOrganization());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), isCashIntensive(), getMarche(), getCodeIBAN(), getCotationRSF(), getSegmentKYC(), getSegmentAML(), getSegmentAPE(), getSegmentPPE(), getSegmentSRA(), getSegmentBLA(), getCountryRiskScore(), getCustomerAge(), getBranch(), getOrganization());
    }
}