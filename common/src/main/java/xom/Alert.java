package xom;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

@SuppressWarnings("serial")
public class Alert implements Serializable {

    String id;
    String codeScenario;
    String descriptionScenario; // à préciser
    String nomScenario;
    String messages[];
    long countTransactions = 0;
    long countCounterparties;

    // Ignorer - liste tronquée utilisé par l'IHM (démos)
    Transaction[] sampleTransactions = new Transaction[] {};
    String[] sampleCounterpartieIds = new String[] {};

    // Autres données alerte

    // Données client
    String clientId;
    String clientIBAN;
    String clientOrganization;
    String clientSegmentKYC;
    String clientSegmentAML;
    String clientCotationRSF;
    // Données contrepartie/pays
    String counterpartyAccountNumber;
    String counterPartyCountry;
    String countryScore;

    // Données transaction
    List<Feature> transactionCharacteristics;
    // Critères de détection
    List<Feature> detectionCriteria;

    public Alert() {
    }

    public void initialize(String nomScenario, Client client, String[] messages, CollectionOfTransactions collectionOfTransactions,
                           CollectionOfStrings collectionOfCounterpartieIds) {
        this.nomScenario = nomScenario;
        this.messages = messages;
        this.clientId = client.getId();
        this.clientIBAN = client.getCodeIBAN();

        if (collectionOfTransactions != null) {
            sampleTransactions = collectionOfTransactions.take(1000);
            Arrays.sort(sampleTransactions);
            countTransactions = collectionOfTransactions.count();
        }

        if (collectionOfCounterpartieIds != null) {
            sampleCounterpartieIds = collectionOfCounterpartieIds.take(1000);
            Arrays.sort(sampleCounterpartieIds);
            countCounterparties = collectionOfCounterpartieIds.count();
        }
    }

    public String getNomScenario() {
        return nomScenario;
    }
    public void setNomScenario(String nomScenario) {
        this.nomScenario = nomScenario;
    }

    public String[] getMessages() {
        return messages;
    }
    public void setMessages(String[] messages) {
        this.messages = messages;
    }

    public Transaction[] getSampleTransactions() {
        return sampleTransactions;
    }
    public void setSampleTransactions(Transaction[] sampleTransactions) {
        this.sampleTransactions = sampleTransactions;
    }

    public String[] getSampleCounterpartieIds() {
        return sampleCounterpartieIds;
    }
    public void setSampleCounterparties(String[] sampleCounterpartieIds) {
        this.sampleCounterpartieIds = sampleCounterpartieIds;
    }

    public long getCountTransactions() {
        return countTransactions;
    }
    public void setCountTransactions(long countTransactions) {
        this.countTransactions = countTransactions;
    }
    public long getCountCounterparties() {
        return countCounterparties;
    }
    public void setCountCounterparties(long countCounterparties) {
        this.countCounterparties = countCounterparties;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getCodeScenario() {
        return codeScenario;
    }
    public void setCodeScenario(String codeScenario) {
        this.codeScenario = codeScenario;
    }

    public String getDescriptionScenario() {
        return descriptionScenario;
    }
    public void setDescriptionScenario(String descriptionScenario) {
        this.descriptionScenario = descriptionScenario;
    }

    public String getClientId() {
        return clientId;
    }
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientIBAN() {
        return clientIBAN;
    }
    public void setClientIBAN(String clientIBAN) {
        this.clientIBAN = clientIBAN;
    }

    public String getClientOrganization() {
        return clientOrganization;
    }
    public void setClientOrganization(String clientOrganization) {
        this.clientOrganization = clientOrganization;
    }

    public String getClientSegmentKYC() {
        return clientSegmentKYC;
    }
    public void setClientSegmentKYC(String clientSegmentKYC) {
        this.clientSegmentKYC = clientSegmentKYC;
    }

    public String getClientSegmentAML() {
        return clientSegmentAML;
    }
    public void setClientSegmentAML(String clientSegmentAML) {
        this.clientSegmentAML = clientSegmentAML;
    }

    public String getClientCotationRSF() {
        return clientCotationRSF;
    }
    public void setClientCotationRSF(String clientCotationRSF) {
        this.clientCotationRSF = clientCotationRSF;
    }

    public String getCounterpartyAccountNumber() {
        return counterpartyAccountNumber;
    }
    public void setCounterpartyAccountNumber(String counterpartyAccountNumber) {
        this.counterpartyAccountNumber = counterpartyAccountNumber;
    }

    public String getCounterPartyCountry() {
        return counterPartyCountry;
    }
    public void setCounterPartyCountry(String counterPartyCountry) {
        this.counterPartyCountry = counterPartyCountry;
    }

    public String getCountryScore() {
        return countryScore;
    }
    public void setCountryScore(String countryScore) {
        this.countryScore = countryScore;
    }

    public List<Feature> getTransactionCharacteristics() {
        return transactionCharacteristics;
    }
    public void setTransactionCharacteristics(List<Feature> transactionCharacteristics) {
        this.transactionCharacteristics = transactionCharacteristics;
    }

    public List<Feature> getDetectionCriteria() {
        return detectionCriteria;
    }
    public void setDetectionCriteria(List<Feature> detectionCriteria) {
        this.detectionCriteria = detectionCriteria;
    }
}