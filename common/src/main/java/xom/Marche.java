package xom;

import java.util.HashMap;
import java.util.Map;

public enum Marche {
    //dans les fichier on retrouve uniquement :
    // CORP, PROXI, GF, MC
    Default(""),
    BPF_GF("BPF_GF"),
    Corporate_GF("Corporate_GF"),
    Corporate("CORP"),
    EI_Pri("EI_Pri"),
    EI_Pro("EI_Pro"),
    ER_du_CORPORATE("ER_du_CORPORATE"),
    ER_Retail("ER_Retail"),
    ER("ER"),
    Part(""),
    Pri_du_Pro("Part"),
    Pro("PROXI");

    private String value;
    private static final Map<String, Marche> lookup = new HashMap();

    static
    {
        for(Marche marche : Marche.values())
        {
            System.out.println("marche =="+marche.getValue());
            lookup.put(marche.getValue(), marche);
        }
    }
    Marche(String value){
        this.value=value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static Marche lookup(String value){
        try{
            return lookup.get(value);
        }catch (Exception e){
            return Marche.Default;
        }
    }
}
