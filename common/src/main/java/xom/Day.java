package xom;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings({ "deprecation", "serial" })
public class Day implements Serializable {
    int packedValue; // à la COBOL, P; ex 20210113

    public Day() {}

    public Day(String s) {
        String[] split = s.split("/");
        packedValue = Integer.parseInt(split[0])  // day of month
                + 100 * Integer.parseInt(split[1])  // month
                + 10000 * Integer.parseInt(split[2]); // year
    }

    public Day clone2() {
        Day day = new Day();
        day.packedValue = packedValue;
        return day;
    }

    public boolean isBefore(Day day) {
        return packedValue < day.packedValue;
    }

    public boolean isAfter(Day day) {
        return day.isBefore(this);
    }

    public boolean isOrIsBefore(Day day) {
        return packedValue <= day.packedValue;
    }

    public boolean isOrIsAfter(Day day) {
        return day.isOrIsBefore(this);
    }

    public boolean isBetween(Day day1, Day day2) {
        return day1.packedValue <= packedValue && packedValue <= day2.packedValue;
    }

    public Day plusNbDays(int nb) {
        Date date = toDate();
        date.setDate(date.getDate() + nb);
        return fromDate(date);
    }

    public Day minusNbDays(int nb) {
        return plusNbDays(-nb);
    }

    public Day plusNbWeeks(int nb) {
        return plusNbDays(nb * 7);
    }

    public Day minusNbWeeks(int nb) {
        return plusNbWeeks(-nb);
    }

    public Day plusNbMonths(int nb) {
        Date date = toDate();
        date.setMonth(date.getMonth() + nb);
        return fromDate(date);
    }

    public Day minusNbMonths(int nb) {
        return plusNbMonths(-nb);
    }

    public int getDayOfMonth() {
        return packedValue % 100;
    }

    public void setDayOfMonth(int dayOfMonth) {
        packedValue = (packedValue / 100) * 100 + dayOfMonth;
    }

    public int getMonth() {
        return (packedValue / 100) % 100;
    }

    public void setMonth(int month) {
        int day = getDayOfMonth();
        packedValue = (packedValue / 10000) * 10000 + 100 * month + day;
    }

    public int getYear() {
        return packedValue / 10000;
    }

    public void setYear(int year) {
        packedValue = (packedValue % 10000) + year * 10000;
    }

    @Override
    public String toString() {
        return String.format("%02d/%02d/%d", getDayOfMonth(), getMonth(), getYear());
    }

    @Override
    public boolean equals(Object obj) {
        if (!obj.getClass().equals(getClass())) {
            return false;
        }
        Day day = (Day) obj;
        return (day.packedValue == packedValue);
    }

    public static Day fromString(String s) {
        if (s.toLowerCase().equals("today")) {
            return today();
        }
        Day day = new Day();
        String[] split = s.split("/");
        day.setDayOfMonth(Integer.parseInt(split[0]));
        day.setMonth(Integer.parseInt(split[1]));
        day.setYear(Integer.parseInt(split[2]));
        return day;
    }

    public static Day fromDate(Date date) {
        Day day = new Day();
        day.setDayOfMonth(date.getDate());
        day.setMonth(1 + date.getMonth());
        day.setYear(1900 + date.getYear());
        return day;
    }

    public static Day today() {
        return fromDate(new Date());
    }

    Date toDate() {
        Date date = new Date(0);
        date.setDate(getDayOfMonth());
        date.setMonth(getMonth() - 1);
        date.setYear(getYear() - 1900);
        return date;
    }

	/* Quick & dirty validation without JUnit or ...
	public static void main(String[] args) {
		Day day = new Day();
		day.setDayOfMonth(2);
		day.setMonth(3);
		day.setYear(2021);
		System.out.println(day);
		Date date = day.toDate();
		System.out.println(date);
		Day day2 = fromDate(date);
		System.out.println(day2);

		Day day3 = day2.plusNbDays(62);
		System.out.println(day3);
		System.out.println(day3.plusNbMonths(2));

		Day day4 = new Day("19/09/1971");
		System.out.println("day4 = " + day4);
		System.out.println("packedValue = " + day4.packedValue);
		System.out.println("day4.getDayOfMonth() = " + day4.getDayOfMonth());
		System.out.println("day4.getMonth() = " + day4.getMonth());
		System.out.println("day4.getYear() = " + day4.getYear());
		day4.setDayOfMonth(23);
		System.out.println("day4.setDayOfMonth(23) = " + day4);
		day4.setMonth(12);
		System.out.println("day4.setMonth(12) = " + day4);
		day4.setYear(2021);
		System.out.println("day4.setYear(2021) = " + day4);
		System.out.println("day4 = " + day4);
		System.out.println("day3 = " + day3);
		System.out.println("day3.isBefore(day4) = " + day3.isBefore(day4));
		System.out.println("day4.isBefore(day3) = " + day4.isBefore(day3));
		System.out.println("day3.isOrIsBefore(day4) = " + day3.isOrIsBefore(day4));
		System.out.println("day4.isOrIsBefore(day3) = " + day4.isOrIsBefore(day3));
	}
	*/
}