package xom;

import java.io.Serializable;

public interface TransactionPredicate extends Serializable {
    boolean test(Transaction transaction);
}