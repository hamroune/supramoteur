package xom;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings("serial")
public class InmemoryCollectionOfTransactions extends CollectionOfTransactions implements Serializable {
    List<Transaction> transactions = new ArrayList<>();

    public InmemoryCollectionOfTransactions() {
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    @Override
    protected xom.CollectionOfTransactions filter(TransactionPredicate predicate) {
        InmemoryCollectionOfTransactions collection = new InmemoryCollectionOfTransactions();
        collection.setTransactions(transactions.stream().filter(t -> predicate.test(t)).collect(Collectors.toList()));
        return collection;
    }

    @Override
    public CollectionOfTransactions filterNotDoneWith(CollectionOfStrings counterparties) {
        InmemoryCollectionOfCounterpartieIds counterparties2 = (InmemoryCollectionOfCounterpartieIds) counterparties;
        return filter(transaction -> counterparties2.counterparties.contains(transaction.getCounterpartyAccountNumber()) == false);
    }

    @Override
    public double _getTotalAmount() {
        return transactions.stream().map(Transaction::getAmount).reduce((a1, a2) -> a1 + a2).orElse(0.);
    }

    @Override
    public CollectionOfTransactions union(CollectionOfTransactions collectionOfTransactions) {
        InmemoryCollectionOfTransactions collection = new InmemoryCollectionOfTransactions();
        collection.setTransactions(
                Stream.of(transactions, ((InmemoryCollectionOfTransactions) collectionOfTransactions).transactions)
                        // .flatMap(x -> x.stream())
                        .flatMap(List<Transaction>::stream).collect(Collectors.toList()));
        return collection;
    }

    @Override
    public CollectionOfStrings _getCounterparties() {
        InmemoryCollectionOfCounterpartieIds counterparties = new InmemoryCollectionOfCounterpartieIds();
        counterparties.setCounterparties(
                (transactions.stream().map(Transaction::getCounterpartyAccountNumber).distinct().collect(Collectors.toList())));
        return counterparties;
    }

    @Override
    public Transaction[] take(int n) {
        n = Math.min(n, transactions.size());
        return transactions.subList(0, n).toArray(new Transaction[n]);
    }

    @Override
    public long count() {
        return transactions.size();
    }

    @Override
    public void cache() {
        // TODO Auto-generated method stub
    }

    @Override
    public void unpersist() {
        // TODO Auto-generated method stub
    }
}