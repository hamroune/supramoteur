package xom.dynamic;

public class ProprieteBoolean extends ProprieteDynamique {

    private Boolean value;

    public ProprieteBoolean() {}

    public ProprieteBoolean(String key, Boolean value) {
        super.key = key;
        this.value = value;
    }

    public Boolean getValue() {
        return value;
    }

    // Necessary for WSDL proper generation
    public void setValue(Boolean value) {
        this.value = value;
    }
}