package xom.dynamic;

import java.util.Date;

// import com.fasterxml.jackson.annotation.JsonFormat;

public class ProprieteDate extends ProprieteDynamique {

    private Date value;

    public ProprieteDate() {}

    public ProprieteDate(String key, Date value) {
        super.key = key;
        this.value = value;
    }

    // @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    public Date getValue() {
        return value;
    }

    // Necessary for WSDL proper generation
    public void setValue(Date value) {
        this.value = value;
    }
}