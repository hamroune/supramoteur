package xom.dynamic;

public class ProprieteDouble extends ProprieteDynamique {

    private Double value;

    public ProprieteDouble() {}

    public ProprieteDouble(String key, Double value) {
        super.key = key;
        this.value = value;
    }

    public Double getValue() {
        return value;
    }

    // Necessary for WSDL proper generation
    public void setValue(Double value) {
        this.value = value;
    }
}