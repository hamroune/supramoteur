package xom.dynamic;

public class ProprieteLong extends ProprieteDynamique {

    private Long value;

    public ProprieteLong() {}

    public ProprieteLong(String key, Long value) {
        super.key = key;
        this.value = value;
    }

    public Long getValue() {
        return value;
    }

    // Necessary for WSDL proper generation
    public void setValue(Long value) {
        this.value = value;
    }
}
