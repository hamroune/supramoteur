package xom.dynamic;

// import javax.xml.bind.annotation.XmlSeeAlso;

// import com.fasterxml.jackson.annotation.JsonSubTypes;
// import com.fasterxml.jackson.annotation.JsonTypeInfo;
// import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

// @XmlSeeAlso({ ProprieteBoolean.class, ProprieteDate.class, ProprieteDouble.class, ProprieteLong.class, ProprieteString.class })
// @JsonSubTypes({ @JsonSubTypes.Type(value = ProprieteBoolean.class, name = "proprieteBoolean"), @JsonSubTypes.Type(value = ProprieteDate.class, name = "proprieteDate"), @JsonSubTypes.Type(value = ProprieteDouble.class, name = "proprieteDouble"), @JsonSubTypes.Type(value = ProprieteLong.class, name = "proprieteLong"), @JsonSubTypes.Type(value = ProprieteString.class, name = "proprieteString") })
// @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = As.PROPERTY, property = "type")
public class ProprieteDynamique {

    protected String key;

    public ProprieteDynamique() {
    }

    public String getKey() {
        return key;
    }

    // Necessary for WSDL proper generation
    public void setKey(String key) {
        this.key = key;
    }
}