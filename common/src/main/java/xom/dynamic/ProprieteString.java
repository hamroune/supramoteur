package xom.dynamic;

public class ProprieteString extends ProprieteDynamique {

    private String value;

    public ProprieteString() {}

    public ProprieteString(String key, String value) {
        super.key = key;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    // Necessary for WSDL proper generation
    public void setValue(String value) {
        this.value = value;
    }
}