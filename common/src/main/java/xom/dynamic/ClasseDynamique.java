package xom.dynamic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

// import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Classe de base pour hériter des propriétés dynamiques.
 *
 * @author S_AUGIER
 */
public class ClasseDynamique {

    /**
     * Map de stockage des propriétés dynamiques
     */
    protected Map<String, ProprieteDynamique> proprietes = null;

    /**
     * Retourne la liste des propriétés de type <code>Boolean</code>
     *
     * @return la liste des propriétés de type <code>Boolean</code>
     */
    public Collection<ProprieteBoolean> getProprietesBoolean() {

        Collection<ProprieteBoolean> res = new ArrayList<ProprieteBoolean>();
        if (proprietes == null) {
            return res;
        }
        for (ProprieteDynamique p : proprietes.values()) {
            if (p instanceof ProprieteBoolean) {
                res.add((ProprieteBoolean) p);
            }
        }
        return res;
    }

    /**
     * Définit les propriétés <code>Boolean</code> de la classe dynamique Toutes les propriétées sont effacées, puis les propriétés de la collection <code>props</code> sont ajoutées
     *
     * @param props
     *            les propriétés <code>Boolean</code> qui figureront dans la classe dynamique
     */
    public void setProprietesBoolean(Collection<ProprieteBoolean> props) {

        if ((proprietes == null) && ((props == null) || (props.isEmpty()))) {
            return;
        }
        if (proprietes == null) {
            proprietes = new HashMap<String, ProprieteDynamique>();
        } else {
            for (ProprieteDynamique p : proprietes.values()) {
                if (p instanceof ProprieteBoolean) {
                    proprietes.remove(p.getKey());
                }
            }
        }
        for (ProprieteBoolean p : props) {
            proprietes.put(p.getKey(), p);
        }
    }

    /**
     * Retourne la liste des propriétés de type <code>Date</code>
     *
     * @return la liste des propriétés de type <code>Date</code>
     */
    public Collection<ProprieteDate> getProprietesDate() {

        Collection<ProprieteDate> res = new ArrayList<ProprieteDate>();
        if (proprietes == null) {
            return res;
        }
        for (ProprieteDynamique p : proprietes.values()) {
            if (p instanceof ProprieteDate) {
                res.add((ProprieteDate) p);
            }
        }
        return res;
    }

    /**
     * Définit les propriétés <code>Date</code> de la classe dynamique Toutes les propriétées sont effacées, puis les propriétés de la collection <code>props</code> sont ajoutées
     *
     * @param props
     *            les propriétés <code>Date</code> qui figureront dans la classe dynamique
     */
    public void setProprietesDate(Collection<ProprieteDate> props) {

        if ((proprietes == null) && ((props == null) || (props.isEmpty()))) {
            return;
        }
        if (proprietes == null) {
            proprietes = new HashMap<String, ProprieteDynamique>();
        } else {
            for (ProprieteDynamique p : proprietes.values()) {
                if (p instanceof ProprieteDate) {
                    proprietes.remove(p.getKey());
                }
            }
        }
        for (ProprieteDate p : props) {
            proprietes.put(p.getKey(), p);
        }
    }

    /**
     * Retourne la liste des propriétés de type <code>Double</code>
     *
     * @return la liste des propriétés de type <code>Double</code>
     */
    public Collection<ProprieteDouble> getProprietesDouble() {

        Collection<ProprieteDouble> res = new ArrayList<ProprieteDouble>();
        if (proprietes == null) {
            return res;
        }
        for (ProprieteDynamique p : proprietes.values()) {
            if (p instanceof ProprieteDouble) {
                res.add((ProprieteDouble) p);
            }
        }
        return res;
    }

    /**
     * Définit les propriétés <code>Double</code> de la classe dynamique Toutes les propriétées sont effacées, puis les propriétés de la collection <code>props</code> sont ajoutées
     *
     * @param props
     *            les propriétés <code>Double</code> qui figureront dans la classe dynamique
     */
    public void setProprietesDouble(Collection<ProprieteDouble> props) {

        if ((proprietes == null) && ((props == null) || (props.isEmpty()))) {
            return;
        }
        if (proprietes == null) {
            proprietes = new HashMap<String, ProprieteDynamique>();
        } else {
            for (ProprieteDynamique p : proprietes.values()) {
                if (p instanceof ProprieteDouble) {
                    proprietes.remove(p.getKey());
                }
            }
        }
        for (ProprieteDouble p : props) {
            proprietes.put(p.getKey(), p);
        }
    }

    /**
     * Retourne la liste des propriétés de type <code>Long</code>
     *
     * @return la liste des propriétés de type <code>Long</code>
     */
    public Collection<ProprieteLong> getProprietesLong() {

        Collection<ProprieteLong> res = new ArrayList<ProprieteLong>();
        if (proprietes == null) {
            return res;
        }
        for (ProprieteDynamique p : proprietes.values()) {
            if (p instanceof ProprieteLong) {
                res.add((ProprieteLong) p);
            }
        }
        return res;
    }

    /**
     * Définit les propriétés Long de la classe dynamique Toutes les propriétées sont effacées, puis les propriétés de la collection <code>props</code> sont ajoutées
     *
     * @param props
     *            les propriétés <code>Long</code> qui figureront dans la classe dynamique
     */
    public void setProprietesLong(Collection<ProprieteLong> props) {

        if ((proprietes == null) && ((props == null) || (props.isEmpty()))) {
            return;
        }
        if (proprietes == null) {
            proprietes = new HashMap<String, ProprieteDynamique>();
        } else {
            for (ProprieteDynamique p : proprietes.values()) {
                if (p instanceof ProprieteLong) {
                    proprietes.remove(p.getKey());
                }
            }
        }
        for (ProprieteLong p : props) {
            proprietes.put(p.getKey(), p);
        }
    }

    /**
     * Retourne la liste des propriétés de type <code>String</code>
     *
     * @return la liste des propriétés de type <code>String</code>
     */
    public Collection<ProprieteString> getProprietesString() {

        Collection<ProprieteString> res = new ArrayList<ProprieteString>();
        if (proprietes == null) {
            return res;
        }
        for (ProprieteDynamique p : proprietes.values()) {
            if (p instanceof ProprieteString) {
                res.add((ProprieteString) p);
            }
        }
        return res;
    }

    /**
     * Définit les propriétés <code>String</code> de la classe dynamique Toutes les propriétées sont effacées, puis les propriétés dans la collection <code>prop</code> sont ajoutées
     *
     * @param props
     *            les propriétés <code>String</code> qui figureront dans la classe dynamique
     */
    public void setProprietesString(Collection<ProprieteString> props) {

        if ((proprietes == null) && ((props == null) || (props.isEmpty()))) {
            return;
        }
        if (proprietes == null) {
            proprietes = new HashMap<String, ProprieteDynamique>();
        } else {
            for (ProprieteDynamique p : proprietes.values()) {
                if (p instanceof ProprieteString) {
                    proprietes.remove(p.getKey());
                }
            }
        }
        for (ProprieteString p : props) {
            proprietes.put(p.getKey(), p);
        }
    }

    /**
     * Retourne la <code>Map</code> contenant toutes les propriétés
     *
     * @return la <code>Map</code> contenant toutes les propriétés, ou null si aucune propriété n'a été ajoutée
     */
    public Map<String, ProprieteDynamique> getProprietes() {

        return proprietes;
    }

    /**
     * Retourne la valeur d'une propriété de type <code>Boolean</code>.
     *
     * @param key
     *            le nom de la propriété
     * @return la valeur <code>Boolean</code> de la propriété, ou null si la propriété n'existe pas ou n'est pas de type <code>Boolean</code>
     */
    public Boolean getBooleanPropriete(String key) {

        if (proprietes == null) {
            return null;
        }
        ProprieteDynamique p = proprietes.get(key);
        if ((p != null) && (p instanceof ProprieteBoolean)) {
            return ((ProprieteBoolean) p).getValue();
        }
        return null;
    }

    /**
     * Ajoute une propriété de type <code>Boolean</code>
     *
     * @param key
     *            le nom de la propriété
     * @param value
     *            la valeur de la propriété
     */
    public void setBooleanPropriete(String key, Boolean value) {

        ProprieteBoolean p = new ProprieteBoolean(key, value);
        if (proprietes == null) {
            proprietes = new HashMap<String, ProprieteDynamique>();
        }
        proprietes.put(key, p);
    }

    /**
     * Retourne la valeur d'une propriété de type <code>Date</code>.
     *
     * @param key
     *            le nom de la propriété
     * @return la valeur <code>Date</code> de la propriété, ou null si la propriété n'existe pas ou n'est pas de type <code>Date</code>
     */
    // @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    public Date getDatePropriete(String key) {

        if (proprietes == null) {
            return null;
        }
        ProprieteDynamique p = proprietes.get(key);
        if ((p != null) && (p instanceof ProprieteDate)) {
            return ((ProprieteDate) p).getValue();
        }
        return null;
    }

    /**
     * Ajoute une propriété de type <code>Date</code>
     *
     * @param key
     *            le nom de la propriété
     * @param value
     *            la valeur de la propriété
     */
    public void setDatePropriete(String key, Date value) {

        ProprieteDate p = new ProprieteDate(key, value);
        if (proprietes == null) {
            proprietes = new HashMap<String, ProprieteDynamique>();
        }
        proprietes.put(key, p);
    }

    /**
     * Retourne la valeur d'une propriété de type <code>Double</code>.
     *
     * @param key
     *            le nom de la propriété
     * @return la valeur <code>Double</code> de la propriété, ou null si la propriété n'existe pas ou n'est pas de type <code>Double</code>
     */
    public Double getDoublePropriete(String key) {

        if (proprietes == null) {
            return null;
        }
        ProprieteDynamique p = proprietes.get(key);
        if ((p != null) && (p instanceof ProprieteDouble)) {
            return ((ProprieteDouble) p).getValue();
        }
        return null;
    }

    /**
     * Ajoute une propriété de type <code>Double</code>
     *
     * @param key
     *            le nom de la propriété
     * @param value
     *            la valeur de la propriété
     */
    public void setDoublePropriete(String key, Double value) {

        ProprieteDouble p = new ProprieteDouble(key, value);
        if (proprietes == null) {
            proprietes = new HashMap<String, ProprieteDynamique>();
        }
        proprietes.put(key, p);
    }

    /**
     * Retourne la valeur d'une propriété de type <code>Long</code>.
     *
     * @param key
     *            le nom de la propriété
     * @return la valeur <code>Long</code> de la propriété, ou null si la propriété n'existe pas ou n'est pas de type <code>Long</code>
     */
    public Long getLongPropriete(String key) {

        if (proprietes == null) {
            return null;
        }
        ProprieteDynamique p = proprietes.get(key);
        if ((p != null) && (p instanceof ProprieteLong)) {
            return ((ProprieteLong) p).getValue();
        }
        return null;
    }

    /**
     * Ajoute une propriété de type <code>Long</code>
     *
     * @param key
     *            le nom de la propriété
     * @param value
     *            la valeur de la propriété
     */
    public void setLongPropriete(String key, Long value) {

        ProprieteLong p = new ProprieteLong(key, value);
        if (proprietes == null) {
            proprietes = new HashMap<String, ProprieteDynamique>();
        }
        proprietes.put(key, p);
    }

    /**
     * Retourne la valeur d'une propriété de type <code>String</code>
     *
     * @param key
     *            le nom de la propriété
     * @return la valeur de la propriété sous forme de <code>String</code>, ou null si la propriété n'existe pas ou n'est pas de type <code>String</code>
     */
    public String getStringPropriete(String key) {

        if (proprietes == null) {
            return null;
        }
        ProprieteDynamique p = proprietes.get(key);
        if ((p != null) && (p instanceof ProprieteString)) {
            return ((ProprieteString) p).getValue();
        }
        return null;
    }

    /**
     * Ajoute une propriété de type <code>String</code>
     *
     * @param key
     *            le nom de la propriété
     * @param value
     *            la valeur de la propriété
     */
    public void setStringPropriete(String key, String value) {

        ProprieteString p = new ProprieteString(key, value);
        if (proprietes == null) {
            proprietes = new HashMap<String, ProprieteDynamique>();
        }
        proprietes.put(key, p);
    }

    /**
     * Retourne vrai si la propriété existe.
     *
     * @param nom_propriete
     *            le nom de la propriété
     * @return vrai si la propriét est présente, faux sinon
     */
    public boolean hasPropriete(String nom_propriete) {
        if (proprietes == null) {
            return false;
        }
        return proprietes.get(nom_propriete) != null;
    }

    public boolean hasAllProprietes(Collection<String> nom_proprietes) {
        if (proprietes == null) {
            return false;
        }
        for(String prop : nom_proprietes){
            if( proprietes.get(prop) == null) {
                return false;
            }
        }
        return true;
    }

    public boolean hasAnyProprietes(Collection<String> nom_proprietes) {
        if (proprietes == null) {
            return false;
        }
        for(String prop : nom_proprietes){
            if( proprietes.get(prop) != null) {
                return true;
            }
        }
        return false;
    }

    /**
     * Supprime toutes les propriétés.
     */
    public void removeAllProprietes() {

        proprietes = null;
    }

    /**
     * Supprime une propriété.
     *
     * @param nom_propriete
     *            le nom de la propriété à supprimer
     */
    public void removePropriete(String nom_propriete) {

        if (proprietes != null) {
            proprietes.remove(nom_propriete);
        }
    }
}