#!/usr/bin/env python
# coding: utf-8

# In[1]:


from random import randint
import random
import pandas as pd
import datetime
import time


# In[2]:


nb_simu = 100
min_eventDate = "1/1/2019 1:30 PM"
max_eventDate = datetime.datetime.now().strftime("%m/%d/%Y %I:%M %p")

# Pour le fichier attributs clients
choix_csp = ["Agriculteur exploitant", "Chef d entreprise", "Cadre", "Profession intermediaire", "Employe", "Ouvrier", "Sans activite pro"]
choix_projet = ["Simulation pret immobilier", "Automobile", "Travaux", "Voyages", "Obseques"]
choix_situationLogement = ["Location", "Achat", "Demenagement", "Residence principale ou secondaire"]
choix_situationFamiliale = ["Celibataire", "Marie ou pacse", "Divorce/separe", "Veuf", "Enfant(s) a charge", "Rattachement du foyer fiscal", "EER du conjoint"]

# Pour le fichier Evenements
choix_event = ["reclamation", "imprevu","aucun"]
choix_imprevus =["depenses de sante","accident de voiture","declaration de sinistre","cambriolage","travaux toiture","perte d'emploi","achat nouvel ordinateur","travaux plomberie"]
choix_reclamation = ["dépassement plafond carte","erreur de débit","carte débitée deux fois","renégociation de credit","paiement refuse"]
choix_statut = ["en cours", "cloturé"]


# In[3]:


# Pour le fichier Interactions
choix_moteur = ["OAP", "PE"]

choix_interaction = (
    "protection_familiale_beneficiaires___verifier_designation_beneficiaire_s__et_adequation_clause_beneficiaire_a_la_volonte_client|Protection Familiale Bénéficiaires : Vérifier désignation bénéficiaire[s] et adéquation clause bénéficiaire à la volonté client",
    "multidetention_el___proposer_option_couple|Multidétention EL : Proposer option couple",
    "email___renseigner_adresse_e_mail_client|Email : Renseigner adresse e-mail client",
    "assurance_auto_simulation___verifier_detention_vehicule_et_si_oui_proposer_simulation_auto|Assurance Auto Simulation : Vérifier détention véhicule et si oui proposer simulation auto",
    "assurance_vie|Assurance Vie",
    "assurance_scolaire|Assurance Scolaire",
    "option_servissimes__services_a_la_personne_|Option Servissimes [services à la personne]",
    "lep|LEP",
    "releves_en_ligne___proposer_souscription_offre|Relevés En Ligne : Proposer souscription offre",
    "situation|Situation",
    "multirisques_habitation|Multirisques Habitation",
    "comparateur_tarif_el___proposer_simulation_au_client_detenteur_de_carte_sans_el|Comparateur Tarif EL : Proposer simulation au client détenteur de carte sans EL",
    "services_en_ligne|Services en ligne",
    "pva_fin_couverture___rappeler_fin_couverture_protection_vie_active_apres_65_ans_et_proposer_diagnostic_personnalise_via__ma_decouverte_prevoyance_|PVA Fin Couverture : Rappeler fin couverture Protection Vie Active après 65 ans et proposer diagnostic personnalisé via Ma Découverte Prévoyance",
    "assurance_scolaire___mobileo___presenter_offre_si_client_a_foyer_avec_enfant_s_|Assurance Scolaire & Mobileo : Présenter offre si client a foyer avec enfant[s] ",
    "service_simplidons|Service Simplidons",
    "profil_client_retail_validation___preciser__validation_partielle_|Profil Client Retail Validation : Préciser validation partielle",
    "ma_banque___mes_comptes___faire_demo|Ma banque & Mes comptes : Faire démo",
    "facilite_de_caisse_personalisee|Facilité De Caisse Personalisée",
    "regroupement_de_credits|Regroupement de Crédits",
    "el_visa_reference_infinite|EL Visa Référence Infinite",
    "mobileo___determiner_couverture_assurance_sur_foyer_fiscal__si_un_des_parents_est_souscripteur_de_l_offre_|Mobileo : Déterminer couverture assurance sur foyer fiscal [si un des parents est souscripteur de l'offre]",
    "multiplacements_avenir|Multiplacements Avenir",
    "option_cascade|Option Cascade",
    "cle_digitale___presenter_offre_et_proposer_activation_cle|Clé Digitale : Présenter offre et proposer activation-clé",
    "mrh_point___refaire_point_client_sur_formule_choisie_et_ou_capital_mobilier_assure|MRH Point : Refaire point client sur formule choisie et/ou capital mobilier assuré",
    "bnp_protection_familiale|BNP Protection Familiale",
    "el_visa_reference_premier|EL Visa Référence Premier",
    "pel_plafond___echanger_sur_plafond_et_reallocation_epargne_programmee|PEL Plafond : Echanger sur plafond et réallocation épargne programmée",
    "protection_vie_active|Protection Vie Active",
    "pel|PEL",
    "profil_client_retail_recreation___recreer_profil|Profil Client Retail Recréation : Recréer profil",
    "protection_de_l_epargne|Protection de l'Epargne",
    "cel|CEL",
    "livret_jeune|Livret Jeune",
    "epargne_csep___proposer_rencontre_client_csep_pour_determiner_solutions_epargne_et_prevoyance_adaptees|Epargne CSEP : Proposer rencontre client-CSEP pour déterminer solutions épargne et prévoyance adaptées",
    "carte_bnp_net___proposer_formule_bnp_net_sur_carte_visa_detenue|Carte BNP Net : Proposer formule BNP Net sur carte Visa détenue",
    "dat_potentiels_marche|DAT Potentiels Marché",
    "profil_client_retail_renseignement___renseigner_profil|Profil Client Retail Renseignement : Renseigner profil",
    "csl|CSL",
    "bnp_protection_accidents_de_la_vie|BNP Protection Accidents de la Vie",
    "ldd___echanger_sur_plafond_et_reallocation_epargne_programmee|LDD : Echanger sur plafond et réallocation épargne programmée",
    "pep|PEP",
    "carte_debit_credit_visa_amex__|Carte débit/crédit Visa Amex …",
    "protection_familiale_capital___verifier_adequation_capital_assure_aux_besoins_client|Protection Familiale Capital : Vérifier adéquation capital assuré aux besoins client",
    "protection_des_objets_nomades__mobileo_|Protection des Objets Nomades [Mobileo]",
    "credit_renouvelable|Crédit Renouvelable",
    "facilidom___echanger_sur_facilite_a_changer_de_banque_si_relation_secondaire|Facilidom : Echanger sur facilité à changer de banque si Relation Secondaire",
    "ldd|LDD",
    "opt_in___completer_informations_et_preferences_de_contact_client__tel_email_____|Opt'In : Compléter informations et préférences de contact client [tél email ...]",
    "assurance_vie_versement___proposer_versement_periodique|Assurance Vie Versement : Proposer versement périodique",
    "credit_immobilier|Crédit Immobilier",
    "dat_potentiel_1_2_3|DAT Potentiel 1 2 3",
    "cel___echanger_sur_plafond_et_reallocation_epargne_programmee|CEL : Echanger sur plafond et réallocation épargne programmée",
    "npai___renseigner_nouvelle_adresse_client|NPAI : Renseigner nouvelle adresse client",
    "panorama|Panorama",
    "el_decouverte_16_17_ans|EL Découverte 16-17 ans",
    "livret_a|Livret A",
    "bnp_sante|BNP Santé",
    "pret_personnel_amortissable|Prêt Personnel Amortissable",
    "mrh_confort_confort____echanger_sur_surveillance_habitation__formule_confort_confort__|MRH Confort/confort+ : Echanger sur surveillance habitation [formule confort/confort+] ",
    "bnp_paribas_obseques|BNP Paribas Obsèques",
    "opcvm_sur_compte_titres|OPCVM sur compte-titres",
    "debit_immediat___verifier_adequation_du_debit_carte_aux_besoins_client_et_proposer_debit_differe|Débit Immédiat : Vérifier adéquation du débit carte aux besoins client et proposer débit différé",
    "kyc_documents_renseignement___renseigner_donnees_client_manquantes_et_obligatoires|KYC Documents Renseignement : Renseigner données client manquantes et obligatoires",
    "lep___echanger_sur_plafond_et_reallocation_epargne_programmee|LEP : Echanger sur plafond et réallocation épargne programmée",
    "pel_alimentation___rappeler_alimentation_obligatoire_sauf_si_pel_echu_ou_mis_en_maintien|PEL Alimentation : Rappeler alimentation obligatoire sauf si PEL échu ou mis en maintien",
    "carte_myb_s_12_17_ans|Carte MyB's 12-17 ans",
    "perp|PERP",
    "mrh_etudiant___proposer_bilan_sur_adequation_entre_profil_client_bien_et_formule_detenue|MRH Etudiant : Proposer bilan sur adéquation entre profil client/bien et formule détenue",
    "credit_auto|Crédit Auto",
    "agios___ajuster_fc_si_montant_fcp_disponible|Agios : Ajuster FC si montant FCP disponible",
    "bnp_protection_habitat|BNP Protection Habitat",
    "el_initiative_18_24_ans|EL Initiative 18-24 ans",
    "el_reference_visa_classic|EL Référence Visa Classic",
    "client_eligible_bp___proposer_rencontre_client_conseiller_banque_privee|Client Eligible BP : Proposer rencontre client-conseiller Banque Privée",
    "immobilier___echanger_autour_de_la_relation_principale_et_proposer_simulation_de_capacite_d_endettement_ou_de_rachat|Immobilier : Echanger autour de la Relation Principale et proposer simulation de capacité d'endettement ou de rachat",
    "ade_immo_deleguee___proposer_diagnostic_personnalise_via__ma_decouverte_prevoyance_|ADE Immo Déléguée : Proposer diagnostic personnalisé via Ma Découverte Prévoyance",
    "protection_des_moyens_de_paiement|Protection des Moyens de Paiement",
    "prevoyance_diagnostic___proposer_diagnostic_personnalise_via__ma_decouverte_prevoyance__ou_rdv_a_3_avec_csep|Prévoyance Diagnostic : Proposer diagnostic personnalisé via Ma Découverte Prévoyance ou RDV à 3 avec CSEP",
    "pea|PEA",
    "assurance_auto|Assurance Auto",
    "epargne_projet___echanger_sur_projet_lie_au_compte_d_epargne_et_sur_alimentation_reguliere|Epargne Projet : Echanger sur projet lié au compte d'épargne et sur alimentation régulière",
    "livret_a___echanger_sur_plafond_et_reallocation_epargne_programmee|Livret A : Echanger sur plafond et réallocation épargne programmée",
    "livret_jeune___echanger_sur_plafond_et_reallocation_epargne_programmee|Livret Jeune : Echanger sur plafond et réallocation épargne programmée",
    "releves_en_ligne|Relevés en ligne",
    "mrh_simulation___proposer_simulation_si_client_non_detenteur_de_l_assurance|MRH Simulation : Proposer simulation si client non détenteur de l'assurance",
    "assurance_auto_revalidation___revalider_formule_et_aborder_projet_auto|Assurance Auto Revalidation : Revalider formule et aborder projet auto",
    "assurance_vie_beneficiaires___verifier_adequation_clause_beneficiaire_a_la_volonte_client|Assurance Vie Bénéficiaires : Vérifier adéquation clause bénéficiaire à la volonté client",
    "pva_solutions_complementaires___verifier_coherence_entre_protection_vie_active_et_profil_client_et_proposer_solutions_complementaires__protection_familiale_cardif_garantie___|PVA Solutions Complémentaires : Vérifier cohérence entre Protection Vie Active et profil client et proposer solutions complémentaires [Protection Familiale Cardif Garantie …]",
    "kyc_documents_actualisation___actualiser_donnees_client|KYC Documents Actualisation : Actualiser données client"
)


# In[4]:


def timeToString(time):
    time_split = time.split()[0].split("/")
    time_string = (time_split[2])+"-"+(time_split[0])+"-"+(time_split[1])
    return time_string

def strTimeProp(start, end, format, prop):
    """Get a time at a proportion of a range of two formatted times.
    start and end should be strings specifying times formated in the
    given format (strftime-style), giving an interval [start, end].
    prop specifies how a proportion of the interval to be taken after
    start.  The returned time will be in the specified format.
    """

    stime = time.mktime(time.strptime(start, format))
    etime = time.mktime(time.strptime(end, format))

    ptime = stime + prop * (etime - stime)

    return time.strftime(format, time.localtime(ptime))


def randomDate(start, end, prop):
    return strTimeProp(start, end, '%m/%d/%Y %I:%M %p', prop)


# In[5]:


class AttributClient:
    def __init__(self, clientId, name,  dataType, value, label):
        self.clientId = clientId
        self.name = name
        self.dataType = dataType
        self.value = value
        self.label = label

    def to_dict(self):
          return {
              'clientId' : self.clientId,
              'name' : self.name,          
              'dataType' : self.dataType,
              'value' : self.value,
              'label' : self.label
          }


    
class Interaction:
    def __init__(self, client_id, int_id, int_moteur, int_date, label):
        self.client_id = client_id
        self.int_id = int_id
        self.int_moteur = int_moteur
        self.int_date = int_date
        self.label = label

    def to_dict(self):
        return {
            'clientId' : self.client_id,
            'interactionId' : self.int_id,
            'moteurId' : self.int_moteur,
            'interactionDate' : self.int_date,
            'label' : self.label
        }

class Event:
      def __init__(self, clientId, name, subject, startDate, endDate, status, category, label):
        self.client_id = client_id
        self.name = name
        self.subject = subject
        self.startDate = startDate
        self.endDate = endDate
        self.status = status
        self.category = category
        self.label = label
    
    
      def to_dict(self):
            return {
                'clientId': self.client_id,
                'name' : self.name,
                'subject' : self.subject,
                'startDate' : self.startDate,
                'endDate' : self.endDate,
                'status' : self.status,
                'category' : self.category,
                'label' : self.label
            }


# In[6]:


list_client = list()

for i in range(1,nb_simu):
    client_id="client_"+str(i)
    client_age = randint(16, 80)
    
    # Ajout age
    list_client.append(AttributClient(client_id, "age", "java.lang.Integer", client_age, "âge"))
    
    #Attribut csp par défaut pour les jeunes et retraités, aléatoire sinon
    if client_age<22:
        list_client.append(AttributClient(client_id, "CSP", "java.lang.String", "Etudiant", "CSP"))
    elif client_age>65:
        list_client.append(AttributClient(client_id, "CSP", "java.lang.String", "Retraite", "CSP"))
    else :
        list_client.append(AttributClient(client_id, "CSP", "java.lang.String", random.choice(choix_csp), "CSP"))
        
    # Pour les adultes, ajout aléatoire des attributs non obligatoires
    if client_age>=22:
        if (bool(random.getrandbits(1)) == True) :
            list_client.append(AttributClient(client_id, "projet", "java.lang.String", random.choice(choix_projet), "projet"))
        if (bool(random.getrandbits(1)) == True) :
            list_client.append(AttributClient(client_id, "situation_logement", "java.lang.String", random.choice(choix_situationLogement), "situation logement"))            
        if (bool(random.getrandbits(1)) == True) :
            list_client.append(AttributClient(client_id, "situation_familiale", "java.lang.String", random.choice(choix_situationFamiliale), "situation familiale"))                  


# In[7]:


list_interaction = list()

for i in range(1,nb_simu):
    client_id="client_"+str(i)
    nb_interaction = randint(2, 20)
    
    for i in range (1,nb_interaction):
        interaction = random.choice(choix_interaction).split("|")
        interaction_id = interaction[0]
        interaction_label = interaction[1]
        interaction_moteur = random.choice(choix_moteur)
        interaction_date = datetime.datetime.now().strftime("%Y-%m-%d")
        interaction = Interaction(client_id, interaction_id,interaction_moteur,interaction_date, interaction_label)
        list_interaction.append(interaction)


# In[8]:


list_event = list()
for i in range(1,nb_simu):
    client_id="client_"+str(i)
    event = random.choice(choix_event)
    
    start_time = randomDate(min_eventDate, max_eventDate, random.random())
    startDate = timeToString(start_time)
    status = random.choice(choix_statut)

    if status == "en cours" :
        endDate = datetime.datetime.now().strftime("%Y-%m-%d") #remplacer par vide apres MAJ du projet
    else :
        end_time = randomDate(start_time, max_eventDate, random.random())
        endDate = timeToString(end_time)
  
    if event == "reclamation":
        client_event = random.choice(choix_reclamation)
        label = "réclamation"
    else:
        if event == "imprevu":
            client_event = random.choice(choix_imprevus)
            label = "imprévu"
        
    if (event == "reclamation") or (event == "imprevu"):
        add_event = Event(client_id, event, client_event, startDate, endDate, status, "client", label)
        list_event.append(add_event)


# In[9]:


horodatage = datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
interaction_filename = horodatage+"_interactions.csv"
client_filename = horodatage+"_clients.csv"
event_filename = horodatage+"_events.csv"

interaction_df = pd.DataFrame.from_records([s.to_dict() for s in list_interaction])
client_df = pd.DataFrame.from_records([s.to_dict() for s in list_client])
event_df = pd.DataFrame.from_records([s.to_dict() for s in list_event])

interaction_df.to_csv(interaction_filename, index=False, sep=';')
client_df.to_csv(client_filename, index=False, sep=';')
event_df.to_csv(event_filename, index=False, sep=';')


# In[ ]:




